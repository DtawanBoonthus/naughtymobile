﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void MainScript::LogMessage(System.String,System.String)
extern void MainScript_LogMessage_m7953CB447ABFC4D0E286219425CF26684DB6F6B0 (void);
// 0x00000002 System.Void MainScript::Get()
extern void MainScript_Get_m3BD5249433D12EE1562CB113FF95FD302207B2A0 (void);
// 0x00000003 System.Void MainScript::Post()
extern void MainScript_Post_mF7C21819A39C80CBEF1E5471FF7A505A6CF9B687 (void);
// 0x00000004 System.Void MainScript::Put()
extern void MainScript_Put_m2E077AE7D135014788A9F219D4FA32B618BED552 (void);
// 0x00000005 System.Void MainScript::Delete()
extern void MainScript_Delete_m3CEEF47B740A70E766D3CF74E6C1D37E13C0A8C8 (void);
// 0x00000006 System.Void MainScript::AbortRequest()
extern void MainScript_AbortRequest_mDCEC8008FD946FB21C66D8DE04AA652FD091AC5E (void);
// 0x00000007 System.Void MainScript::DownloadFile()
extern void MainScript_DownloadFile_mF54C07D06969AD450838E27A885FB450BDA6971C (void);
// 0x00000008 System.Void MainScript::.ctor()
extern void MainScript__ctor_mDE68DE7EC111F10F50287CE15AC2100BAC26DF23 (void);
// 0x00000009 System.Void MainScript::<Post>b__4_0(Models.Post)
extern void MainScript_U3CPostU3Eb__4_0_m8B0202AA78218D3787871313668C76F260A52D42 (void);
// 0x0000000A System.Void MainScript::<Post>b__4_1(System.Exception)
extern void MainScript_U3CPostU3Eb__4_1_m2C8C662A8A4EE9A219C715C38E88E9D156CC0777 (void);
// 0x0000000B System.Void MainScript::<Put>b__5_0(Proyecto26.RequestException,Proyecto26.ResponseHelper,Models.Post)
extern void MainScript_U3CPutU3Eb__5_0_m556845DFA7BB10FCFF4A3A04B17EF6BC8AA77CAE (void);
// 0x0000000C System.Void MainScript::<Delete>b__6_0(Proyecto26.RequestException,Proyecto26.ResponseHelper)
extern void MainScript_U3CDeleteU3Eb__6_0_m3092FBC0D31C4B83C0C7E537BFD404FD28F0BB8E (void);
// 0x0000000D System.Void MainScript::<DownloadFile>b__8_0(Proyecto26.ResponseHelper)
extern void MainScript_U3CDownloadFileU3Eb__8_0_m8D61A6C39FFBB97884CF0935DA9BF297B5DE8393 (void);
// 0x0000000E System.Void MainScript::<DownloadFile>b__8_1(System.Exception)
extern void MainScript_U3CDownloadFileU3Eb__8_1_m99B4D340A42D8D8B784669EDB99B721CAD853FB6 (void);
// 0x0000000F System.Void MainScript/<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_m46C50E0A5C12FA151CC55E0B928E27838925B4FC (void);
// 0x00000010 RSG.IPromise`1<Models.Todo[]> MainScript/<>c__DisplayClass3_0::<Get>b__0(Models.Post[])
extern void U3CU3Ec__DisplayClass3_0_U3CGetU3Eb__0_m1E95C1F0B2D8E5BC302A5ADCA1ABAE0293AEA82D (void);
// 0x00000011 RSG.IPromise`1<Models.User[]> MainScript/<>c__DisplayClass3_0::<Get>b__1(Models.Todo[])
extern void U3CU3Ec__DisplayClass3_0_U3CGetU3Eb__1_mB5A9CA352F06FB80FDC47E6FE114495BA4687AC1 (void);
// 0x00000012 RSG.IPromise`1<Models.Photo[]> MainScript/<>c__DisplayClass3_0::<Get>b__2(Models.User[])
extern void U3CU3Ec__DisplayClass3_0_U3CGetU3Eb__2_mB93ACE707409C85F0542FD7665B0BF499473D98D (void);
// 0x00000013 System.Void MainScript/<>c__DisplayClass3_0::<Get>b__3(Models.Photo[])
extern void U3CU3Ec__DisplayClass3_0_U3CGetU3Eb__3_m381C9E851E8FB988B02A1076B749138ABC954BDB (void);
// 0x00000014 System.Void MainScript/<>c__DisplayClass3_0::<Get>b__4(System.Exception)
extern void U3CU3Ec__DisplayClass3_0_U3CGetU3Eb__4_m44FBEB10E4BAEB006FC92154DB40AB3CB7BC4DE0 (void);
// 0x00000015 System.Void MainScript/<>c::.cctor()
extern void U3CU3Ec__cctor_m4FA3DF2408BEA55F36632D2A36DDEA9E1475BBA3 (void);
// 0x00000016 System.Void MainScript/<>c::.ctor()
extern void U3CU3Ec__ctor_m6B5FCC705564205B21810FFBA043CB8DA659F8E1 (void);
// 0x00000017 System.Void MainScript/<>c::<Put>b__5_1(Proyecto26.RequestException,System.Int32)
extern void U3CU3Ec_U3CPutU3Eb__5_1_mC12F48C710EDD364D74E0411BCE9CB0F55326D2F (void);
// 0x00000018 System.Void Bullet::Awake()
extern void Bullet_Awake_m2D77C2A3CF11F66E86FF074B8C4397C0E3DE2004 (void);
// 0x00000019 System.Void Bullet::Init(UnityEngine.Transform)
extern void Bullet_Init_m98ADE8968CAB3858821FA1D84A67385DD8A4176A (void);
// 0x0000001A System.Void Bullet::Move(UnityEngine.Transform)
extern void Bullet_Move_m548616709427BF038F8FC6EEF384B129BE90CF3D (void);
// 0x0000001B System.Void Bullet::OnTriggerEnter(UnityEngine.Collider)
extern void Bullet_OnTriggerEnter_m670A42C7BC93AF346496B6599303EF4C10FE690A (void);
// 0x0000001C System.Void Bullet::.ctor()
extern void Bullet__ctor_mC7D931FE508342F413FBA79525A4819D4114B3EC (void);
// 0x0000001D System.Void CharacterInfo::.ctor()
extern void CharacterInfo__ctor_mDB9D9524D840EB8F6461A1EF79322F9A6A10D8C2 (void);
// 0x0000001E System.Void ChooseCharacterInfo::.ctor()
extern void ChooseCharacterInfo__ctor_m4E173EE4F57AE6E8151BBD432CAB13224DF6AC56 (void);
// 0x0000001F System.Void JennieCharacter::Awake()
extern void JennieCharacter_Awake_m2675C76F05D0562E394C95CBA933210A2D4712CD (void);
// 0x00000020 System.Void JennieCharacter::Init(System.Int32,System.Single,Bullet)
extern void JennieCharacter_Init_mFB32E3DFC6DEBE1CC69F0AF2B062974E8565E499 (void);
// 0x00000021 System.Void JennieCharacter::Attack()
extern void JennieCharacter_Attack_m0CF7BB9CDF008051C834D23E435F70E9E3375C99 (void);
// 0x00000022 System.Void JennieCharacter::Die()
extern void JennieCharacter_Die_mAC9D04281CAFC80DA629E25E904E8B0460C0E3C2 (void);
// 0x00000023 System.Void JennieCharacter::Open()
extern void JennieCharacter_Open_m7CD20A1ED71868B739474F449A1B76C7EA653BC5 (void);
// 0x00000024 System.Void JennieCharacter::TakeHit(System.Int32)
extern void JennieCharacter_TakeHit_m460F00CBD168D3684FABBE75E2E414035A506224 (void);
// 0x00000025 System.Void JennieCharacter::TakeCollision()
extern void JennieCharacter_TakeCollision_m9302E68C3D8028FD5CC46B7AE079B957DE88A2FC (void);
// 0x00000026 System.Void JennieCharacter::NotTakeCollision()
extern void JennieCharacter_NotTakeCollision_m393AC75D5B608B7BFEB71B26FE2569A06051A643 (void);
// 0x00000027 System.Void JennieCharacter::OpenShelfZero()
extern void JennieCharacter_OpenShelfZero_m569C780547653E4AACD2954C954B100206C94576 (void);
// 0x00000028 System.Void JennieCharacter::OpenShelfOne()
extern void JennieCharacter_OpenShelfOne_mFF0F138C1B64A94C895CB4B2E65828FE01963EBD (void);
// 0x00000029 System.Void JennieCharacter::OpenShelfTwo()
extern void JennieCharacter_OpenShelfTwo_m9C3421FF2E5A10564740A075B20026A4384E83B2 (void);
// 0x0000002A System.Void JennieCharacter::OpenShelfThree()
extern void JennieCharacter_OpenShelfThree_m4119D14AB62553583CF1599449BD1F5C66E81EB3 (void);
// 0x0000002B System.Void JennieCharacter::OpenShelfFour()
extern void JennieCharacter_OpenShelfFour_m5014598B7E824B499A975C523055CE93F43CF284 (void);
// 0x0000002C System.Void JennieCharacter::OpenShelfFive()
extern void JennieCharacter_OpenShelfFive_m960A4E43F09D5561D3D80C677F2AC71ECFF27E14 (void);
// 0x0000002D System.Void JennieCharacter::OpenShelfSix()
extern void JennieCharacter_OpenShelfSix_m57FBB13CD720B7978589B0AE81CFAB01D819F31C (void);
// 0x0000002E System.Void JennieCharacter::OpenShelfSeven()
extern void JennieCharacter_OpenShelfSeven_mC4DDA3142721E75D92D53254A3AF13C920E82AF6 (void);
// 0x0000002F System.Void JennieCharacter::OpenShelfEight()
extern void JennieCharacter_OpenShelfEight_m2FEACBE5D2CC008D6363F6634FF84CDC881C00FB (void);
// 0x00000030 System.Void JennieCharacter::OpenShelfNine()
extern void JennieCharacter_OpenShelfNine_mE0F2C74ACD37813CF812642450421BB19779AD58 (void);
// 0x00000031 System.Void JennieCharacter::OpenShelfTen()
extern void JennieCharacter_OpenShelfTen_mA95829425480F2FDC27489630134C11BB157E6C4 (void);
// 0x00000032 System.Void JennieCharacter::OpenShelfEleven()
extern void JennieCharacter_OpenShelfEleven_mF3A8E905C6DA84DB82FA09188CDB31BAF7ECB79F (void);
// 0x00000033 System.Void JennieCharacter::OpenShelfTwelve()
extern void JennieCharacter_OpenShelfTwelve_m9F734BCC2B4B54B8337F94BA8D30C6592B9E62D6 (void);
// 0x00000034 System.Void JennieCharacter::OpenShelfThirteen()
extern void JennieCharacter_OpenShelfThirteen_m8445F76D6441558535847821C14BAF5B91CBA14A (void);
// 0x00000035 System.Void JennieCharacter::OpenShelfFourteen()
extern void JennieCharacter_OpenShelfFourteen_mB069AB2D7A071CFDEDF9A776592F5D6022F5DD03 (void);
// 0x00000036 System.Void JennieCharacter::OpenShelfFifteen()
extern void JennieCharacter_OpenShelfFifteen_m884A5A7882B30382361035C1C0EB6DC8D051D00D (void);
// 0x00000037 System.Void JennieCharacter::OpenShelfSixteen()
extern void JennieCharacter_OpenShelfSixteen_mE54EA9B9795EF11A386D05E7523989AA110257ED (void);
// 0x00000038 System.Void JennieCharacter::.ctor()
extern void JennieCharacter__ctor_m8396610D4C601D393F1D587BCF086C349EF00152 (void);
// 0x00000039 System.Void JohnCharacter::Awake()
extern void JohnCharacter_Awake_m0E3DB4E38662CFE030DD41853EB7F2BAC688A38A (void);
// 0x0000003A System.Void JohnCharacter::Init(System.Int32,System.Single,Bullet)
extern void JohnCharacter_Init_m851F47FF610A0867BF5EAA427EFE25331F3624EF (void);
// 0x0000003B System.Void JohnCharacter::Attack()
extern void JohnCharacter_Attack_m747AB55B2423243DE582336AE9A5CE430F410787 (void);
// 0x0000003C System.Void JohnCharacter::Die()
extern void JohnCharacter_Die_mC3F732919409BF51811C4DEA3FCE8FB8CB47BC6F (void);
// 0x0000003D System.Void JohnCharacter::Open()
extern void JohnCharacter_Open_m0E6D33ACD4D4DDB743C3044077D6AFCE483E29BE (void);
// 0x0000003E System.Void JohnCharacter::TakeHit(System.Int32)
extern void JohnCharacter_TakeHit_m2A46A1E14C03BEB127B879D2CA5A570419A848C1 (void);
// 0x0000003F System.Void JohnCharacter::TakeCollision()
extern void JohnCharacter_TakeCollision_m234D021D28175C5DAFE288290CEA09759C6C773B (void);
// 0x00000040 System.Void JohnCharacter::NotTakeCollision()
extern void JohnCharacter_NotTakeCollision_m8D697042F7AF8087C8ED8A8F91FE9F7085B225AA (void);
// 0x00000041 System.Void JohnCharacter::OpenShelfZero()
extern void JohnCharacter_OpenShelfZero_m64B9D62E10E24247D5E687EA2026BADA39699363 (void);
// 0x00000042 System.Void JohnCharacter::OpenShelfOne()
extern void JohnCharacter_OpenShelfOne_mF7925886EB9BC7311361BCF98BBB23414DD35892 (void);
// 0x00000043 System.Void JohnCharacter::OpenShelfTwo()
extern void JohnCharacter_OpenShelfTwo_mCE0218E790E61D0FB1959FC142E7EE29A1639A87 (void);
// 0x00000044 System.Void JohnCharacter::OpenShelfThree()
extern void JohnCharacter_OpenShelfThree_m066B333C120196DA96E363D610855E763AA921A8 (void);
// 0x00000045 System.Void JohnCharacter::OpenShelfFour()
extern void JohnCharacter_OpenShelfFour_mC7ED1C5C9E63C8065F9EBA4B183E8FBC0AC0D724 (void);
// 0x00000046 System.Void JohnCharacter::OpenShelfFive()
extern void JohnCharacter_OpenShelfFive_m2B40148E1D1E842FDC3A9317CD5BDFD6E4B49E96 (void);
// 0x00000047 System.Void JohnCharacter::OpenShelfSix()
extern void JohnCharacter_OpenShelfSix_m3BD56BFAD4AAFBC91EDCC9F4534C3B31C793DA62 (void);
// 0x00000048 System.Void JohnCharacter::OpenShelfSeven()
extern void JohnCharacter_OpenShelfSeven_m203F842ECF8ED058ED227A4605EB2FA4F1A56BD2 (void);
// 0x00000049 System.Void JohnCharacter::OpenShelfEight()
extern void JohnCharacter_OpenShelfEight_mB638ABB9CAA4FD01337A8B3F9EC34E0FAFADE8B0 (void);
// 0x0000004A System.Void JohnCharacter::OpenShelfNine()
extern void JohnCharacter_OpenShelfNine_mF3B06BC041362379235F87B0152A15B3E5949623 (void);
// 0x0000004B System.Void JohnCharacter::OpenShelfTen()
extern void JohnCharacter_OpenShelfTen_m1DC0B152E946CDABAFB1C4FFC24CC4F19BBB0AD8 (void);
// 0x0000004C System.Void JohnCharacter::OpenShelfEleven()
extern void JohnCharacter_OpenShelfEleven_mC64DE528583ED9A890FCF3B43D34B300D302EA24 (void);
// 0x0000004D System.Void JohnCharacter::OpenShelfTwelve()
extern void JohnCharacter_OpenShelfTwelve_mB1F16EE4F1B91D9049471D4FAEDDE3FD5DD5CB6F (void);
// 0x0000004E System.Void JohnCharacter::OpenShelfThirteen()
extern void JohnCharacter_OpenShelfThirteen_m08F5BED5E99DC70772B756427BEF99C52E69E6B2 (void);
// 0x0000004F System.Void JohnCharacter::OpenShelfFourteen()
extern void JohnCharacter_OpenShelfFourteen_mFCA39E749ED72213F3181D79B8F2B7B56AE60C8F (void);
// 0x00000050 System.Void JohnCharacter::OpenShelfFifteen()
extern void JohnCharacter_OpenShelfFifteen_mF2F7B0A17CDB660724FC0A07C8D223BF2A189585 (void);
// 0x00000051 System.Void JohnCharacter::OpenShelfSixteen()
extern void JohnCharacter_OpenShelfSixteen_m213C3064F417AE5E9BC61E1006E1C49B6E15F39F (void);
// 0x00000052 System.Void JohnCharacter::.ctor()
extern void JohnCharacter__ctor_m4A5F777EB34BEA52677E30D069F738B7F6D2BE2A (void);
// 0x00000053 System.Void EnemyController::Update()
extern void EnemyController_Update_mDB0B02F4008FD062F471267A67A06169E1BC1B3C (void);
// 0x00000054 UnityEngine.Vector3 EnemyController::Target()
extern void EnemyController_Target_mA1B724BCFBCE5171EBE0C36484810BDB3B9EE36D (void);
// 0x00000055 System.Void EnemyController::ChasePlayer()
extern void EnemyController_ChasePlayer_m0410883BAD98EBFFD819FC3394F67399E9499C53 (void);
// 0x00000056 System.Void EnemyController::Patrolling()
extern void EnemyController_Patrolling_mBC53FB53167D372361C5C20C21C43D0B96B1C852 (void);
// 0x00000057 System.Void EnemyController::SearchWalkPoint()
extern void EnemyController_SearchWalkPoint_m2ED2B7F18F2A488A704BE52DB89B13950AF43A58 (void);
// 0x00000058 System.Void EnemyController::OnDrawGizmosSelected()
extern void EnemyController_OnDrawGizmosSelected_m8009702CE33352E8EE2ACB15B8D2AB985C1E80AC (void);
// 0x00000059 System.Void EnemyController::TakeHit(System.Int32)
extern void EnemyController_TakeHit_mB4823CC7995CC19860F2BC24E38A8212C32C7F1B (void);
// 0x0000005A System.Void EnemyController::OnTriggerEnter(UnityEngine.Collider)
extern void EnemyController_OnTriggerEnter_mA87502B43AFEDE1A85F3D768F6EE85F0A8930308 (void);
// 0x0000005B System.Collections.IEnumerator EnemyController::Stun()
extern void EnemyController_Stun_m7E3350CD634381655B1C3431288C09BE9AAA4CB5 (void);
// 0x0000005C System.Void EnemyController::.ctor()
extern void EnemyController__ctor_m547F49905D505F962CBC708846F8E8A3B0838F70 (void);
// 0x0000005D System.Void EnemyController/<Stun>d__25::.ctor(System.Int32)
extern void U3CStunU3Ed__25__ctor_mC4CCF189717A66C781EA8571D96032AD5369E710 (void);
// 0x0000005E System.Void EnemyController/<Stun>d__25::System.IDisposable.Dispose()
extern void U3CStunU3Ed__25_System_IDisposable_Dispose_mDB19DC2FBC53BCF5517E19969BE0FE212912BB56 (void);
// 0x0000005F System.Boolean EnemyController/<Stun>d__25::MoveNext()
extern void U3CStunU3Ed__25_MoveNext_m4948DEFF972B007E10F7776E303956B42A002D3B (void);
// 0x00000060 System.Object EnemyController/<Stun>d__25::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStunU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m006EB001AD1FA918E3D8E11C0062B63E3CD414EB (void);
// 0x00000061 System.Void EnemyController/<Stun>d__25::System.Collections.IEnumerator.Reset()
extern void U3CStunU3Ed__25_System_Collections_IEnumerator_Reset_m49E5932E65C8B5AD2FEF0781365707DF05AC38A9 (void);
// 0x00000062 System.Object EnemyController/<Stun>d__25::System.Collections.IEnumerator.get_Current()
extern void U3CStunU3Ed__25_System_Collections_IEnumerator_get_Current_m00D904DCA24B2741C875F23A3E96132EED59067B (void);
// 0x00000063 System.Void ScoreInfo::.ctor()
extern void ScoreInfo__ctor_mA5525978BB93E4E29663142B356B1AA92A0DD6E9 (void);
// 0x00000064 System.Void ItemList::Init(System.Enum,UnityEngine.Sprite,System.Int32)
extern void ItemList_Init_mF4121001AD672541AF238ABA5E8149CADE7B3A42 (void);
// 0x00000065 System.Void ItemList::.ctor()
extern void ItemList__ctor_m1BDD77BEBF23A2851584E6BC874024B1D49B6474 (void);
// 0x00000066 System.Void ItemShelf::Init(System.Enum,UnityEngine.Sprite)
extern void ItemShelf_Init_m9F6E72A84F775ADC7444244EB7A9F3939EF05CB7 (void);
// 0x00000067 System.Void ItemShelf::.ctor()
extern void ItemShelf__ctor_m5D7FFDDA969A6A353034327B77980325EA3764D4 (void);
// 0x00000068 System.Void GameManager::Awake()
extern void GameManager_Awake_m22F42B2A82708B10F652CAD8F2E0A4767110FF30 (void);
// 0x00000069 System.Void GameManager::StartGame()
extern void GameManager_StartGame_m6022C5CDD590728691B22E9B87185BFE3D6A8EC1 (void);
// 0x0000006A System.Collections.IEnumerator GameManager::CountDownTime()
extern void GameManager_CountDownTime_m774E99963E283E6FCFB0ED04887FC3F9E52228B5 (void);
// 0x0000006B System.Void GameManager::EndGame()
extern void GameManager_EndGame_m34CB0E063C72D2D7BA815B5397C5DB865EE60810 (void);
// 0x0000006C System.Void GameManager::.ctor()
extern void GameManager__ctor_mE8666F6D0CA9C31E16B719F79780DC4B0245B64D (void);
// 0x0000006D System.Void GameManager/<CountDownTime>d__3::.ctor(System.Int32)
extern void U3CCountDownTimeU3Ed__3__ctor_m0741F40648A96BA1C1154602DB873E1A95EF7F02 (void);
// 0x0000006E System.Void GameManager/<CountDownTime>d__3::System.IDisposable.Dispose()
extern void U3CCountDownTimeU3Ed__3_System_IDisposable_Dispose_mC98CC4D20ED03E14A44ED0167236364DC6E49B47 (void);
// 0x0000006F System.Boolean GameManager/<CountDownTime>d__3::MoveNext()
extern void U3CCountDownTimeU3Ed__3_MoveNext_m88D608228620A2420188FDE96B4742C593D864E1 (void);
// 0x00000070 System.Object GameManager/<CountDownTime>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCountDownTimeU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m432CEE0DC100C67E519FD20F93567099DFBEC09D (void);
// 0x00000071 System.Void GameManager/<CountDownTime>d__3::System.Collections.IEnumerator.Reset()
extern void U3CCountDownTimeU3Ed__3_System_Collections_IEnumerator_Reset_mB631E70306B4BA94A494F53CCBECD5CCAF89924B (void);
// 0x00000072 System.Object GameManager/<CountDownTime>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CCountDownTimeU3Ed__3_System_Collections_IEnumerator_get_Current_m5295FD4B6544423C8B35758FA7C649609393BC82 (void);
// 0x00000073 System.Int32 ItemManager::get_CountCreateItem()
extern void ItemManager_get_CountCreateItem_m6C552BDEA7914A6DAAFD5DB9BEE1F60B63E18D66 (void);
// 0x00000074 System.Void ItemManager::set_CountCreateItem(System.Int32)
extern void ItemManager_set_CountCreateItem_m087FE1C92277CACFDF2B57127A672495A9165091 (void);
// 0x00000075 System.Int32 ItemManager::get_CountChips()
extern void ItemManager_get_CountChips_m431AD1777282BBE638A20105601FF45F0571E241 (void);
// 0x00000076 System.Void ItemManager::set_CountChips(System.Int32)
extern void ItemManager_set_CountChips_m6C4EB6A2399A762F372E9ECC309C74710EB89E3B (void);
// 0x00000077 System.Int32 ItemManager::get_CountChocolate()
extern void ItemManager_get_CountChocolate_m587B00C587B6BB77CF9959368D4BF2AE628A0841 (void);
// 0x00000078 System.Void ItemManager::set_CountChocolate(System.Int32)
extern void ItemManager_set_CountChocolate_m2E5EDC57FCBB72835BA29532A00681F7D426EAAF (void);
// 0x00000079 System.Int32 ItemManager::get_CountCandy()
extern void ItemManager_get_CountCandy_mD425F962956CBCE080D580B81D6C2336AE062663 (void);
// 0x0000007A System.Void ItemManager::set_CountCandy(System.Int32)
extern void ItemManager_set_CountCandy_mBBAF17D24BA57FA6F26F9AD94E14F7CFC58A5DBC (void);
// 0x0000007B System.Int32 ItemManager::get_CountMilk()
extern void ItemManager_get_CountMilk_m7D0F9D93518F11AB606A0EA5BF212F696E6C7093 (void);
// 0x0000007C System.Void ItemManager::set_CountMilk(System.Int32)
extern void ItemManager_set_CountMilk_m9FD522B8470865B15F1E4EE1E36E1A22BA7AE70B (void);
// 0x0000007D System.Int32 ItemManager::get_CountCola()
extern void ItemManager_get_CountCola_mAD239036425F52D9BC9E9B69DF407ABC8A332078 (void);
// 0x0000007E System.Void ItemManager::set_CountCola(System.Int32)
extern void ItemManager_set_CountCola_m1A7CA9AB8EB7F8B9829295A4D495045C91DFB4BB (void);
// 0x0000007F System.Int32 ItemManager::get_CountMeat()
extern void ItemManager_get_CountMeat_mC4FF877C321FDB09033ADA52F147CF14157010E1 (void);
// 0x00000080 System.Void ItemManager::set_CountMeat(System.Int32)
extern void ItemManager_set_CountMeat_m0D6F51990969E8BDEA8EC4747C4B2F98283FA1B9 (void);
// 0x00000081 System.Int32 ItemManager::get_CountIceCream()
extern void ItemManager_get_CountIceCream_mCE8B43152CF5DBAE44E478594A2ACF97A8530D9D (void);
// 0x00000082 System.Void ItemManager::set_CountIceCream(System.Int32)
extern void ItemManager_set_CountIceCream_mA4205FF84CED9EC7A7377633DDCD9B31875FDD3A (void);
// 0x00000083 System.Int32 ItemManager::get_CountFrozenFood()
extern void ItemManager_get_CountFrozenFood_m6E9A7BF0B2418409E3D3FBDE4BB49A2C2B61FCA3 (void);
// 0x00000084 System.Void ItemManager::set_CountFrozenFood(System.Int32)
extern void ItemManager_set_CountFrozenFood_m9D615D288C7F81ED97985CD72AA197A71839FD42 (void);
// 0x00000085 System.Int32 ItemManager::get_CountVegetables()
extern void ItemManager_get_CountVegetables_m210EF2EB4D68EF632248FFF648B1DB7A008A5F28 (void);
// 0x00000086 System.Void ItemManager::set_CountVegetables(System.Int32)
extern void ItemManager_set_CountVegetables_m0E8C52DE66373314386315C34EDEC1571433656B (void);
// 0x00000087 System.Int32 ItemManager::get_CountBattery()
extern void ItemManager_get_CountBattery_mA3DE2B0E9ADD96E9352B425D4B1D15B7C6E11C45 (void);
// 0x00000088 System.Void ItemManager::set_CountBattery(System.Int32)
extern void ItemManager_set_CountBattery_m2DAA7D323706C85D611C6D2DBC6ACE7C5D277496 (void);
// 0x00000089 System.Int32 ItemManager::get_CountWire()
extern void ItemManager_get_CountWire_mC5C55E17672A312BA901B71D037140A940853AA1 (void);
// 0x0000008A System.Void ItemManager::set_CountWire(System.Int32)
extern void ItemManager_set_CountWire_mEBF3FD4D022794B5BF80CFE94B9CB8687B91D8AC (void);
// 0x0000008B System.Int32 ItemManager::get_CountLightBulb()
extern void ItemManager_get_CountLightBulb_mDC786E62738959A55E84E2DA7EB400EF8E2AA33E (void);
// 0x0000008C System.Void ItemManager::set_CountLightBulb(System.Int32)
extern void ItemManager_set_CountLightBulb_mCACA46B4E35537CFA786D438855F8D2F6842CAAE (void);
// 0x0000008D System.Void ItemManager::Awake()
extern void ItemManager_Awake_m38A9CB7617956210243C27F71C25FE547791586C (void);
// 0x0000008E System.Void ItemManager::SetOnClick()
extern void ItemManager_SetOnClick_mF5BA6710568D6BF2ECC35393452B3CC1D048B8C2 (void);
// 0x0000008F System.Void ItemManager::SetScoreEasyItem(UnityEngine.UI.Button)
extern void ItemManager_SetScoreEasyItem_m416865AD0DE87B15AF1992D191D8257706CAB274 (void);
// 0x00000090 System.Void ItemManager::SetScoreModerateItem(UnityEngine.UI.Button)
extern void ItemManager_SetScoreModerateItem_mEA9B5C6DE280D7DE3C53C69C949F72C4EFE88F2C (void);
// 0x00000091 System.Void ItemManager::SetScoreHardItem(UnityEngine.UI.Button)
extern void ItemManager_SetScoreHardItem_mD8230DD694D8C3A6BB32E806B294F3993FE2DF3B (void);
// 0x00000092 System.Void ItemManager::CheckEndGame(System.Int32)
extern void ItemManager_CheckEndGame_mB2F4C0706F5DF997091B3AFBE6B6BDEFDC0FCC11 (void);
// 0x00000093 System.Void ItemManager::StartGame()
extern void ItemManager_StartGame_m723F4FAEC2D99C000F6E81A4220D7052FE5D413B (void);
// 0x00000094 System.Void ItemManager::StartCreateItemShelf()
extern void ItemManager_StartCreateItemShelf_mA43BB29365DD92D1E17F6D5496361938DAA83EAD (void);
// 0x00000095 System.Void ItemManager::CreateItemShelf(System.Enum,UnityEngine.Sprite,System.Int32,System.Int32,System.Int32)
extern void ItemManager_CreateItemShelf_mF6C5DE07A96B6DAA0A01C57C51A61C5CFD0DBE3E (void);
// 0x00000096 System.Void ItemManager::SetEasyItemList()
extern void ItemManager_SetEasyItemList_m655E0AA5793D6317BF3A4382E60C6461D4E56B09 (void);
// 0x00000097 System.Void ItemManager::SetModerateItemList()
extern void ItemManager_SetModerateItemList_m472A61DF6AA6256676030BF62286335F0315B798 (void);
// 0x00000098 System.Void ItemManager::SetHardItemList()
extern void ItemManager_SetHardItemList_m8B9EEA7AB916BA6C9819C8EE09984184A8A67C93 (void);
// 0x00000099 System.Void ItemManager::RandomEasyItem(System.Int32)
extern void ItemManager_RandomEasyItem_m20A3EDEF413E3744DE4A21503ACC66D26C4CDE60 (void);
// 0x0000009A System.Void ItemManager::RandomHardItem(System.Int32)
extern void ItemManager_RandomHardItem_mF5E40F84ECA2AA4141E43FE5F694D6B22912AB50 (void);
// 0x0000009B System.Void ItemManager::RandomModerateItem(System.Int32)
extern void ItemManager_RandomModerateItem_mB606CB27B956726637233C584EF98994F72C1F3B (void);
// 0x0000009C System.Void ItemManager::CreateItemList()
extern void ItemManager_CreateItemList_m1C073B93036ED928124E029857A2B1C79CECF002 (void);
// 0x0000009D System.Void ItemManager::InstantiateItemList(System.Enum,UnityEngine.Sprite,System.Int32)
extern void ItemManager_InstantiateItemList_mC54CFA14082486311CE5CEB05152CE597C8FCB58 (void);
// 0x0000009E System.Void ItemManager::.ctor()
extern void ItemManager__ctor_mB4E542587417D4CED86516E00930C40F85219460 (void);
// 0x0000009F System.Void ItemManager/<>c__DisplayClass98_0::.ctor()
extern void U3CU3Ec__DisplayClass98_0__ctor_m0022B9B4E8B7B4E0682D50EF314DE224B1118869 (void);
// 0x000000A0 System.Void ItemManager/<>c__DisplayClass98_0::<SetOnClick>b__0()
extern void U3CU3Ec__DisplayClass98_0_U3CSetOnClickU3Eb__0_m3C6E39B7B8703F405A7BA4BA7A4ECE5F135B1A4B (void);
// 0x000000A1 System.Void ItemManager/<>c__DisplayClass98_1::.ctor()
extern void U3CU3Ec__DisplayClass98_1__ctor_m359E547BE5982C9FCAB089F836B0F99ACFA55E17 (void);
// 0x000000A2 System.Void ItemManager/<>c__DisplayClass98_1::<SetOnClick>b__1()
extern void U3CU3Ec__DisplayClass98_1_U3CSetOnClickU3Eb__1_m291760F0355B86DC17F349B27E8EF4E3C34910B3 (void);
// 0x000000A3 System.Void ItemManager/<>c__DisplayClass98_2::.ctor()
extern void U3CU3Ec__DisplayClass98_2__ctor_mB2A461F19C95BDFB00780B3B1996C23C43E38BF2 (void);
// 0x000000A4 System.Void ItemManager/<>c__DisplayClass98_2::<SetOnClick>b__2()
extern void U3CU3Ec__DisplayClass98_2_U3CSetOnClickU3Eb__2_m2EB947AAD4F6A393E800E6BE2A5B91226EA3CFE8 (void);
// 0x000000A5 System.Void ItemManager/<>c__DisplayClass98_3::.ctor()
extern void U3CU3Ec__DisplayClass98_3__ctor_m05ACC0B8E0E8AA5F256E6AB950021838051439FE (void);
// 0x000000A6 System.Void ItemManager/<>c__DisplayClass98_3::<SetOnClick>b__3()
extern void U3CU3Ec__DisplayClass98_3_U3CSetOnClickU3Eb__3_mF69BAE41A31F5D3FFF11DACAD81F500B7DE46182 (void);
// 0x000000A7 System.Void ItemManager/<>c__DisplayClass98_4::.ctor()
extern void U3CU3Ec__DisplayClass98_4__ctor_mADBC978537A18B54E4A98CEE25273564FD3F30D3 (void);
// 0x000000A8 System.Void ItemManager/<>c__DisplayClass98_4::<SetOnClick>b__4()
extern void U3CU3Ec__DisplayClass98_4_U3CSetOnClickU3Eb__4_mCA0C55F6B2A788063D18547A131604C21C876FD6 (void);
// 0x000000A9 System.Void ItemManager/<>c__DisplayClass98_5::.ctor()
extern void U3CU3Ec__DisplayClass98_5__ctor_mCDE572D1F9BB5930F97E5980D4BEE4F025E3D350 (void);
// 0x000000AA System.Void ItemManager/<>c__DisplayClass98_5::<SetOnClick>b__5()
extern void U3CU3Ec__DisplayClass98_5_U3CSetOnClickU3Eb__5_m9B677D36B7F7383886078590171750211288F291 (void);
// 0x000000AB System.Void ItemManager/<>c__DisplayClass98_6::.ctor()
extern void U3CU3Ec__DisplayClass98_6__ctor_m5F480B615C08FB7D46C3EE45064AF8748E5357AD (void);
// 0x000000AC System.Void ItemManager/<>c__DisplayClass98_6::<SetOnClick>b__6()
extern void U3CU3Ec__DisplayClass98_6_U3CSetOnClickU3Eb__6_mF47435C5165F5A0ED519BAA49EEAB2DEF3117D43 (void);
// 0x000000AD System.Void ItemManager/<>c__DisplayClass98_7::.ctor()
extern void U3CU3Ec__DisplayClass98_7__ctor_m6C803F8D1AF4AFA5B1EE6F8EFAB6F181783A1610 (void);
// 0x000000AE System.Void ItemManager/<>c__DisplayClass98_7::<SetOnClick>b__7()
extern void U3CU3Ec__DisplayClass98_7_U3CSetOnClickU3Eb__7_m3A817EB00BA0D0BB47D52076A1B97476D23095D3 (void);
// 0x000000AF System.Void ItemManager/<>c__DisplayClass98_8::.ctor()
extern void U3CU3Ec__DisplayClass98_8__ctor_m21FA20DB4ADEE856FE089F20E91BD8B91B8BDC1E (void);
// 0x000000B0 System.Void ItemManager/<>c__DisplayClass98_8::<SetOnClick>b__8()
extern void U3CU3Ec__DisplayClass98_8_U3CSetOnClickU3Eb__8_m8BFD6F2202D20F29F0A5CBFB7692E33FCE8FCB58 (void);
// 0x000000B1 System.Void ItemManager/<>c__DisplayClass98_9::.ctor()
extern void U3CU3Ec__DisplayClass98_9__ctor_mD86D8431B928711476742ED99CF82037FB2C9A85 (void);
// 0x000000B2 System.Void ItemManager/<>c__DisplayClass98_9::<SetOnClick>b__9()
extern void U3CU3Ec__DisplayClass98_9_U3CSetOnClickU3Eb__9_m8EE539E7CC807233A51F72DAD421AF28CC31EDB2 (void);
// 0x000000B3 System.Void ItemManager/<>c__DisplayClass98_10::.ctor()
extern void U3CU3Ec__DisplayClass98_10__ctor_mE25CFBD6FF076C21F38432A2F81B92B90323ECE5 (void);
// 0x000000B4 System.Void ItemManager/<>c__DisplayClass98_10::<SetOnClick>b__10()
extern void U3CU3Ec__DisplayClass98_10_U3CSetOnClickU3Eb__10_m4B6F28A348EEA3EE165B24303F5525E8A5DF7267 (void);
// 0x000000B5 System.Void ItemManager/<>c__DisplayClass98_11::.ctor()
extern void U3CU3Ec__DisplayClass98_11__ctor_m5CAD8112C745653D5D9AB4E147F5DD5242EDAA44 (void);
// 0x000000B6 System.Void ItemManager/<>c__DisplayClass98_11::<SetOnClick>b__11()
extern void U3CU3Ec__DisplayClass98_11_U3CSetOnClickU3Eb__11_m9AFA6A21DC4236954888EB7573504213AF59FBE7 (void);
// 0x000000B7 System.Void ScoreManager::Awake()
extern void ScoreManager_Awake_mB6C30C958421EED1082D2D5B24532F2548DB4575 (void);
// 0x000000B8 System.Void ScoreManager::StartGame()
extern void ScoreManager_StartGame_mA0E1A60BD40B6F9B1636A574AD66891466FB232F (void);
// 0x000000B9 System.Void ScoreManager::Update()
extern void ScoreManager_Update_m77B6F3EB450BA678FBB01E50C4A6C6A611FB20CD (void);
// 0x000000BA System.Void ScoreManager::GetScore(System.Int32)
extern void ScoreManager_GetScore_m4A0725CB7BD2000328375DCD5E4099B2B8A3E654 (void);
// 0x000000BB System.Void ScoreManager::SetScore(System.Int32)
extern void ScoreManager_SetScore_m86322F173E436E8E067EC97641031911830FD662 (void);
// 0x000000BC System.Void ScoreManager::SetMaxScore(System.Int32)
extern void ScoreManager_SetMaxScore_mFA4CF7AB6743F612C045332691C0704E902B68E3 (void);
// 0x000000BD System.Void ScoreManager::.ctor()
extern void ScoreManager__ctor_m638A240D34643E8AB9D17553622C1C9354348354 (void);
// 0x000000BE System.Void PlayerController::Awake()
extern void PlayerController_Awake_m23059564DCFDD324EA9DB966473251896647CD23 (void);
// 0x000000BF System.Void PlayerController::Start()
extern void PlayerController_Start_m9531F30EC892BDD1758A2EEC724E86EFBDA150A3 (void);
// 0x000000C0 System.Void PlayerController::Update()
extern void PlayerController_Update_mB31159CAD7DD2329859472554BC9154A83D8E794 (void);
// 0x000000C1 System.Void PlayerController::Move()
extern void PlayerController_Move_mB16F20D8A54197A9CB7648A10810E68038A49E5D (void);
// 0x000000C2 System.Void PlayerController::Open()
extern void PlayerController_Open_m1954927782A81DB40FA112A00DA15928774CF006 (void);
// 0x000000C3 System.Collections.IEnumerator PlayerController::OnAttack(UnityEngine.RaycastHit)
extern void PlayerController_OnAttack_m67BDF8BFE7DE03AA8E2925AB95F06099FDFF84D4 (void);
// 0x000000C4 System.Void PlayerController::.ctor()
extern void PlayerController__ctor_mF30385729DAFDFCB895C4939F6051DCE6C0327FB (void);
// 0x000000C5 System.Void PlayerController/<OnAttack>d__19::.ctor(System.Int32)
extern void U3COnAttackU3Ed__19__ctor_m8ED2C6F0F09070E28C887E3B3674DF0BFDA1DD47 (void);
// 0x000000C6 System.Void PlayerController/<OnAttack>d__19::System.IDisposable.Dispose()
extern void U3COnAttackU3Ed__19_System_IDisposable_Dispose_m0ABC51B8B54F7C477F529A12297FED807D68756C (void);
// 0x000000C7 System.Boolean PlayerController/<OnAttack>d__19::MoveNext()
extern void U3COnAttackU3Ed__19_MoveNext_mDF80D118A79DFD563D58515BD0568F7F239397EE (void);
// 0x000000C8 System.Object PlayerController/<OnAttack>d__19::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3COnAttackU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9D07FF142DD9D1EE9D2D8A040A0D46E42E5B0DAE (void);
// 0x000000C9 System.Void PlayerController/<OnAttack>d__19::System.Collections.IEnumerator.Reset()
extern void U3COnAttackU3Ed__19_System_Collections_IEnumerator_Reset_mE9CBC89C7391490A6C0B004C604716171876D216 (void);
// 0x000000CA System.Object PlayerController/<OnAttack>d__19::System.Collections.IEnumerator.get_Current()
extern void U3COnAttackU3Ed__19_System_Collections_IEnumerator_get_Current_mC6D03D6C21954931D3A3E5DF6931C21BCC0BECE3 (void);
// 0x000000CB System.Void ShelfCollision::OnTriggerStay(UnityEngine.Collider)
extern void ShelfCollision_OnTriggerStay_m78A12615A5C69B5DBD1ED87E9AC829476EB56788 (void);
// 0x000000CC System.Void ShelfCollision::OnTriggerExit(UnityEngine.Collider)
extern void ShelfCollision_OnTriggerExit_m55E7459D975F1A85EF5343F4209F2AE5FF2132C6 (void);
// 0x000000CD System.Void ShelfCollision::.ctor()
extern void ShelfCollision__ctor_mCB8533299C1F9109D6917E3214F268781F00EC29 (void);
// 0x000000CE System.Void ShelfEight::OnTriggerStay(UnityEngine.Collider)
extern void ShelfEight_OnTriggerStay_mECE58133E12EB1BC4228D4409056E58BFF451E54 (void);
// 0x000000CF System.Void ShelfEight::.ctor()
extern void ShelfEight__ctor_mAF2A42998B20B1C1C2608E8B457C3A43A398DFA8 (void);
// 0x000000D0 System.Void ShelfEleven::OnTriggerStay(UnityEngine.Collider)
extern void ShelfEleven_OnTriggerStay_mF8876DF0CD0B2B4D49E07347C9186E42B68BDFAC (void);
// 0x000000D1 System.Void ShelfEleven::.ctor()
extern void ShelfEleven__ctor_m939C02922DD535E95EA39A31851A4A93C699E014 (void);
// 0x000000D2 System.Void ShelfFifteen::OnTriggerStay(UnityEngine.Collider)
extern void ShelfFifteen_OnTriggerStay_m24663A23DE88C6F022E5D62DFB7D8BE82EFFC4D1 (void);
// 0x000000D3 System.Void ShelfFifteen::.ctor()
extern void ShelfFifteen__ctor_mB6BC29B810F5421513D9F04949E98D3268A3BB82 (void);
// 0x000000D4 System.Void ShelfFive::OnTriggerStay(UnityEngine.Collider)
extern void ShelfFive_OnTriggerStay_m6C174C8F93C21DBCF376C1D9357A7F656260305B (void);
// 0x000000D5 System.Void ShelfFive::.ctor()
extern void ShelfFive__ctor_m8A353DB3C2C4BC2BA9D169304D7D2B36906ECF43 (void);
// 0x000000D6 System.Void ShelfFour::OnTriggerStay(UnityEngine.Collider)
extern void ShelfFour_OnTriggerStay_m3C6271ED40BD4926D25F9A6376DB061C84E79F7D (void);
// 0x000000D7 System.Void ShelfFour::.ctor()
extern void ShelfFour__ctor_m801C833742C75C73BFA6649B986D65691CE5CA16 (void);
// 0x000000D8 System.Void ShelfFourteen::OnTriggerStay(UnityEngine.Collider)
extern void ShelfFourteen_OnTriggerStay_m746E7A940DAA719A9FBB82D81A946A5BA3EBD6D9 (void);
// 0x000000D9 System.Void ShelfFourteen::.ctor()
extern void ShelfFourteen__ctor_m629092A3BFE51102F20642759D5197EE6BCC00F3 (void);
// 0x000000DA System.Void ShelfNine::OnTriggerStay(UnityEngine.Collider)
extern void ShelfNine_OnTriggerStay_mCD7EF24FBB89A1C9A598BEC69F5B698D7C480B18 (void);
// 0x000000DB System.Void ShelfNine::.ctor()
extern void ShelfNine__ctor_m49FC3343D03C95F2E7B3110790B16A1995214F18 (void);
// 0x000000DC System.Void ShelfOne::OnTriggerStay(UnityEngine.Collider)
extern void ShelfOne_OnTriggerStay_m520B72EB4E8B4D365B5E1FB0C51D2AE30203299B (void);
// 0x000000DD System.Void ShelfOne::.ctor()
extern void ShelfOne__ctor_m3468CBB39EF1EEE8A4D4FE81B9BFE04E607BF5A2 (void);
// 0x000000DE System.Void ShelfSeven::OnTriggerStay(UnityEngine.Collider)
extern void ShelfSeven_OnTriggerStay_m44C3320A171A6532DE7FC3FCB21B9E6C84E9A21C (void);
// 0x000000DF System.Void ShelfSeven::.ctor()
extern void ShelfSeven__ctor_m97C3EE5ECBD14EA8F2BE498F761CD31CDDA85CCE (void);
// 0x000000E0 System.Void ShelfSix::OnTriggerStay(UnityEngine.Collider)
extern void ShelfSix_OnTriggerStay_mCCA0979BB5D75EE98813C838AF7D087D118E4363 (void);
// 0x000000E1 System.Void ShelfSix::.ctor()
extern void ShelfSix__ctor_mA7DE59CA49E75FDD322919660ACD2855C8DD485F (void);
// 0x000000E2 System.Void ShelfSixteen::OnTriggerStay(UnityEngine.Collider)
extern void ShelfSixteen_OnTriggerStay_m4D20310B8B1359B7B9E8F1845A1F6F630E44DE55 (void);
// 0x000000E3 System.Void ShelfSixteen::.ctor()
extern void ShelfSixteen__ctor_mD6439E0C52E3CFEDE0E61C39924BF3B3FA1DE556 (void);
// 0x000000E4 System.Void ShelfTen::OnTriggerStay(UnityEngine.Collider)
extern void ShelfTen_OnTriggerStay_m1CE2DC557CE85F89C18A28A131B7DBCCC27F522C (void);
// 0x000000E5 System.Void ShelfTen::.ctor()
extern void ShelfTen__ctor_mFE5ECE1226FD2522C4A9F5BA64B351F699ABEB7E (void);
// 0x000000E6 System.Void ShelfThirteen::OnTriggerStay(UnityEngine.Collider)
extern void ShelfThirteen_OnTriggerStay_mC3EE6864EA995CD18E58ADA6C3BE1407806D2DCC (void);
// 0x000000E7 System.Void ShelfThirteen::.ctor()
extern void ShelfThirteen__ctor_mD339173C52DDF0162B8DC96A06F1BD45778465A0 (void);
// 0x000000E8 System.Void ShelfThree::OnTriggerStay(UnityEngine.Collider)
extern void ShelfThree_OnTriggerStay_mCBB6E2745A8AFE90DC58AE53EE4BF1A5D2469408 (void);
// 0x000000E9 System.Void ShelfThree::.ctor()
extern void ShelfThree__ctor_mF5A9652CD493CA681F9D36C7EC69B869E16B89CC (void);
// 0x000000EA System.Void ShelfTwelve::OnTriggerStay(UnityEngine.Collider)
extern void ShelfTwelve_OnTriggerStay_m735B4F25F3BBC3085456CD73AB1B77F828C7D9E3 (void);
// 0x000000EB System.Void ShelfTwelve::.ctor()
extern void ShelfTwelve__ctor_mFA5C038A0C9DD7C57B165A18D259E9A8718C63A1 (void);
// 0x000000EC System.Void ShelfTwo::OnTriggerStay(UnityEngine.Collider)
extern void ShelfTwo_OnTriggerStay_mEAB94B74D5BA8FB368B74F40910F64EFA4ADDF0F (void);
// 0x000000ED System.Void ShelfTwo::.ctor()
extern void ShelfTwo__ctor_m310164FAE1A602D696F9B6A1706F738A696F4284 (void);
// 0x000000EE System.Void ShelfZero::OnTriggerStay(UnityEngine.Collider)
extern void ShelfZero_OnTriggerStay_mECAAF4F6F628635E7FECE04E26EB02ED79EAE405 (void);
// 0x000000EF System.Void ShelfZero::.ctor()
extern void ShelfZero__ctor_mD820D97114B157CAA84A2635CF155712AE95FD1F (void);
// 0x000000F0 System.Void HpUI::Init(UnityEngine.Sprite)
extern void HpUI_Init_m08B42491F8B27279FCEA2A636BEA05CD48135067 (void);
// 0x000000F1 System.Void HpUI::.ctor()
extern void HpUI__ctor_m33CD6BA5039F9375343832DCF11553B5525888EF (void);
// 0x000000F2 System.Void UISceneEndGame::.ctor()
extern void UISceneEndGame__ctor_mC3DCFD5EBCF5F3F0165D91F3E926DCBBA37841B9 (void);
// 0x000000F3 System.Void UISceneStartMenu::Awake()
extern void UISceneStartMenu_Awake_m05259D09056D15889E8D668550AEED9314683541 (void);
// 0x000000F4 System.Void UISceneStartMenu::OnClickPlayButton()
extern void UISceneStartMenu_OnClickPlayButton_m6BC46660D332476CF54C8EC190D06D7C69613811 (void);
// 0x000000F5 System.Void UISceneStartMenu::OnClickExitButton()
extern void UISceneStartMenu_OnClickExitButton_m256DB69BB17641DD593B64E1F2BB5D4D3CF04936 (void);
// 0x000000F6 System.Void UISceneStartMenu::OnClickJennieCharacterButton()
extern void UISceneStartMenu_OnClickJennieCharacterButton_m6278D0FA878EDD5375AF916AEFC78F8DDAC46289 (void);
// 0x000000F7 System.Void UISceneStartMenu::OnClickJohnCharacterButton()
extern void UISceneStartMenu_OnClickJohnCharacterButton_m627720D6F19747DF7DEE512DE165A748BC50F487 (void);
// 0x000000F8 System.Void UISceneStartMenu::LoadSceneGamePlay()
extern void UISceneStartMenu_LoadSceneGamePlay_m7FBA42BDC8A6A8CF5294F4B3106560AAAA1FCE41 (void);
// 0x000000F9 System.Void UISceneStartMenu::ShowUI(UnityEngine.RectTransform)
extern void UISceneStartMenu_ShowUI_m035094A6F0F42CB9ED9E721DBBC774B4785CB50D (void);
// 0x000000FA System.Void UISceneStartMenu::HideUI(UnityEngine.RectTransform)
extern void UISceneStartMenu_HideUI_m68681BE6AC613864BC5BFA36CE036137D3D4E769 (void);
// 0x000000FB System.Void UISceneStartMenu::ShowButton(UnityEngine.UI.Button)
extern void UISceneStartMenu_ShowButton_mCF9E727C083262420BD4E2A4C97A18F59CEBAAC4 (void);
// 0x000000FC System.Void UISceneStartMenu::HideButton(UnityEngine.UI.Button)
extern void UISceneStartMenu_HideButton_mF0F0817222D46FCDF1CD4DB9586502F18FE114B6 (void);
// 0x000000FD System.Void UISceneStartMenu::.ctor()
extern void UISceneStartMenu__ctor_m6A6C15088E9B43997B66E1BE5C00D0988478D371 (void);
// 0x000000FE UnityEngine.InputSystem.InputActionAsset InputActions::get_asset()
extern void InputActions_get_asset_m0B72E38B19A55A117FBA6E926016FBB514C749C2 (void);
// 0x000000FF System.Void InputActions::.ctor()
extern void InputActions__ctor_mC7EBC76D7EB1E115FBA01B9CAF581AD999CC6EA7 (void);
// 0x00000100 System.Void InputActions::Dispose()
extern void InputActions_Dispose_mEA7CA175846E55B94546ECF1CCCADB433193E80A (void);
// 0x00000101 System.Nullable`1<UnityEngine.InputSystem.InputBinding> InputActions::get_bindingMask()
extern void InputActions_get_bindingMask_m73324E13801283558A623C2A7F7F02B734CB3F92 (void);
// 0x00000102 System.Void InputActions::set_bindingMask(System.Nullable`1<UnityEngine.InputSystem.InputBinding>)
extern void InputActions_set_bindingMask_mC25EBFCD84B0FC543E72F4F18B64512BB76EA090 (void);
// 0x00000103 System.Nullable`1<UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputDevice>> InputActions::get_devices()
extern void InputActions_get_devices_mD16EE90C2E482B274DCAD86506CF7483BEAAC09A (void);
// 0x00000104 System.Void InputActions::set_devices(System.Nullable`1<UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputDevice>>)
extern void InputActions_set_devices_m8B7F8D4F7165368CEDB339EC95F0641C8505A304 (void);
// 0x00000105 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputControlScheme> InputActions::get_controlSchemes()
extern void InputActions_get_controlSchemes_m41E467EEFEF963C20562A459B0072D0D665BB7E6 (void);
// 0x00000106 System.Boolean InputActions::Contains(UnityEngine.InputSystem.InputAction)
extern void InputActions_Contains_m13DF17674AB027337D6F8DA0EBAC030A6AECCD93 (void);
// 0x00000107 System.Collections.Generic.IEnumerator`1<UnityEngine.InputSystem.InputAction> InputActions::GetEnumerator()
extern void InputActions_GetEnumerator_m270A505B580FCD59C89CB8B136954B18F23AADF6 (void);
// 0x00000108 System.Collections.IEnumerator InputActions::System.Collections.IEnumerable.GetEnumerator()
extern void InputActions_System_Collections_IEnumerable_GetEnumerator_mD6E8ECA793894FE233B2BEB69D4F96E3EACE355B (void);
// 0x00000109 System.Void InputActions::Enable()
extern void InputActions_Enable_mE34A744896308FEB2E057243D5C8BE5696F05A43 (void);
// 0x0000010A System.Void InputActions::Disable()
extern void InputActions_Disable_mB8B2080BAE165B5A9C189F330EB306F19572C01F (void);
// 0x0000010B InputActions/PlayerActions InputActions::get_Player()
extern void InputActions_get_Player_mBC8DE2D352789433F5300F1C9B9F99BFE3903488 (void);
// 0x0000010C System.Void InputActions/PlayerActions::.ctor(InputActions)
extern void PlayerActions__ctor_m930E091485F40060E44C70D57942B72C8846D851 (void);
// 0x0000010D UnityEngine.InputSystem.InputAction InputActions/PlayerActions::get_Move()
extern void PlayerActions_get_Move_mA26DAF3D380CEAA2C8E380BFF6189FC1B9644889 (void);
// 0x0000010E UnityEngine.InputSystem.InputAction InputActions/PlayerActions::get_Attack()
extern void PlayerActions_get_Attack_mD3B982834EECD17F87FF0B3ABD57015297CAF078 (void);
// 0x0000010F UnityEngine.InputSystem.InputAction InputActions/PlayerActions::get_Look()
extern void PlayerActions_get_Look_m37DE82164F8D5796D0EC565A956819BE4B93F707 (void);
// 0x00000110 UnityEngine.InputSystem.InputAction InputActions/PlayerActions::get_Open()
extern void PlayerActions_get_Open_m16D8BFDB470D6DDFC00A68881E15F23B44039D35 (void);
// 0x00000111 UnityEngine.InputSystem.InputActionMap InputActions/PlayerActions::Get()
extern void PlayerActions_Get_mCFD389B69ACE65D5F30FC407BD927CC40428584A (void);
// 0x00000112 System.Void InputActions/PlayerActions::Enable()
extern void PlayerActions_Enable_mA1D8C3F578776B79B01FFFC610FBAF4A1354714D (void);
// 0x00000113 System.Void InputActions/PlayerActions::Disable()
extern void PlayerActions_Disable_m960BE16B364ED50D91824CF19C69C92CB5C71F89 (void);
// 0x00000114 System.Boolean InputActions/PlayerActions::get_enabled()
extern void PlayerActions_get_enabled_m08BF48AFE9EC8BC74F261D2FF1610B5D5B13AA27 (void);
// 0x00000115 UnityEngine.InputSystem.InputActionMap InputActions/PlayerActions::op_Implicit(InputActions/PlayerActions)
extern void PlayerActions_op_Implicit_m6E473469D8A6C3BE74FE2CE5685F8567154D7D2F (void);
// 0x00000116 System.Void InputActions/PlayerActions::SetCallbacks(InputActions/IPlayerActions)
extern void PlayerActions_SetCallbacks_mF731117D76FCFD8E4B59E2C402524B39759E295B (void);
// 0x00000117 System.Void InputActions/IPlayerActions::OnMove(UnityEngine.InputSystem.InputAction/CallbackContext)
// 0x00000118 System.Void InputActions/IPlayerActions::OnAttack(UnityEngine.InputSystem.InputAction/CallbackContext)
// 0x00000119 System.Void InputActions/IPlayerActions::OnLook(UnityEngine.InputSystem.InputAction/CallbackContext)
// 0x0000011A System.Void InputActions/IPlayerActions::OnOpen(UnityEngine.InputSystem.InputAction/CallbackContext)
// 0x0000011B System.Void SafeAreaSetter::Start()
extern void SafeAreaSetter_Start_m83780A11588277507500E26788A2EA69E09FF392 (void);
// 0x0000011C System.Void SafeAreaSetter::ApplySafeArea()
extern void SafeAreaSetter_ApplySafeArea_m4F4F38103E2C3689B92117BF87F0E7559B48CB25 (void);
// 0x0000011D System.Void SafeAreaSetter::Update()
extern void SafeAreaSetter_Update_mF64C75E3889BDD924F5886A3AFF934BC395A5E01 (void);
// 0x0000011E System.Void SafeAreaSetter::.ctor()
extern void SafeAreaSetter__ctor_mA93E55DED3B5E17188E4D3CC093573F5E1BC696B (void);
// 0x0000011F T Singleton`1::get_Instance()
// 0x00000120 System.Void Singleton`1::.ctor()
// 0x00000121 T SingletonPersistent`1::get_Instance()
// 0x00000122 System.Void SingletonPersistent`1::set_Instance(T)
// 0x00000123 System.Void SingletonPersistent`1::Awake()
// 0x00000124 System.Void SingletonPersistent`1::.ctor()
// 0x00000125 System.Void TestScen::Update()
extern void TestScen_Update_m9A37DFD1020B565707A634E6E5E19412746BE78D (void);
// 0x00000126 System.Void TestScen::T()
extern void TestScen_T_m0C9D744C5CCFF74F6A82384E9D8628EDDB796C2C (void);
// 0x00000127 System.Void TestScen::.ctor()
extern void TestScen__ctor_m6B34D87455D8955B1405782313F317332DB5B933 (void);
// 0x00000128 System.Void ChatController::OnEnable()
extern void ChatController_OnEnable_mBC3BFC05A1D47069DFA58A4F0E39CFF8FAD44FF2 (void);
// 0x00000129 System.Void ChatController::OnDisable()
extern void ChatController_OnDisable_m268A7488A3FDEB850FC3BC91A0BBDA767D42876B (void);
// 0x0000012A System.Void ChatController::AddToChatOutput(System.String)
extern void ChatController_AddToChatOutput_m43856EEA133E04C24701A2616E7F10D7FFA68371 (void);
// 0x0000012B System.Void ChatController::.ctor()
extern void ChatController__ctor_m3B66A5F749B457D865E8BDA1DE481C8CF1158026 (void);
// 0x0000012C System.Void DropdownSample::OnButtonClick()
extern void DropdownSample_OnButtonClick_m97641451A2EAC4FC6682897A948A37FDD6BF9EA6 (void);
// 0x0000012D System.Void DropdownSample::.ctor()
extern void DropdownSample__ctor_m87664E3DC75A1A9AA19FEE6ED6B993AB1E50E2B9 (void);
// 0x0000012E System.Void EnvMapAnimator::Awake()
extern void EnvMapAnimator_Awake_mFFC04BC5320F83CA8C45FF36A11BF43AC17C6B93 (void);
// 0x0000012F System.Collections.IEnumerator EnvMapAnimator::Start()
extern void EnvMapAnimator_Start_mC0D348CAB0F0DC920EF3D3A008688B533F66D1BE (void);
// 0x00000130 System.Void EnvMapAnimator::.ctor()
extern void EnvMapAnimator__ctor_mC151D673A394E2E7CEA8774C4004985028C5C3EC (void);
// 0x00000131 System.Void EnvMapAnimator/<Start>d__4::.ctor(System.Int32)
extern void U3CStartU3Ed__4__ctor_m4D65F4FC2207AE4B6BE963AF9B5EDC55C7E29B23 (void);
// 0x00000132 System.Void EnvMapAnimator/<Start>d__4::System.IDisposable.Dispose()
extern void U3CStartU3Ed__4_System_IDisposable_Dispose_mFEE2ACED70A3D825988E28CC61FEF8DCD7660A5B (void);
// 0x00000133 System.Boolean EnvMapAnimator/<Start>d__4::MoveNext()
extern void U3CStartU3Ed__4_MoveNext_m2F1A8053A32AD86DA80F86391EC32EDC1C396AEE (void);
// 0x00000134 System.Object EnvMapAnimator/<Start>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF03E2A57659B8F598AC450183F55D43F903C0A1E (void);
// 0x00000135 System.Void EnvMapAnimator/<Start>d__4::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m268BA538CF6812C56EB281C0CE29D5AA2E9A2CAB (void);
// 0x00000136 System.Object EnvMapAnimator/<Start>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_mFE9CA9F52F9AFA1E4C9DBADA2064F854D6931CFF (void);
// 0x00000137 System.Char TMPro.TMP_DigitValidator::Validate(System.String&,System.Int32&,System.Char)
extern void TMP_DigitValidator_Validate_m5D303EB8CD6E9E7D526D633BA1021884051FE226 (void);
// 0x00000138 System.Void TMPro.TMP_DigitValidator::.ctor()
extern void TMP_DigitValidator__ctor_m1E838567EF38170662F1BAF52A847CC2C258333E (void);
// 0x00000139 System.Char TMPro.TMP_PhoneNumberValidator::Validate(System.String&,System.Int32&,System.Char)
extern void TMP_PhoneNumberValidator_Validate_mF0E90A277E9E91BC213DD02DC60088D03C9436B1 (void);
// 0x0000013A System.Void TMPro.TMP_PhoneNumberValidator::.ctor()
extern void TMP_PhoneNumberValidator__ctor_mB3C36CAAE3B52554C44A1D19194F0176B5A8EED3 (void);
// 0x0000013B TMPro.TMP_TextEventHandler/CharacterSelectionEvent TMPro.TMP_TextEventHandler::get_onCharacterSelection()
extern void TMP_TextEventHandler_get_onCharacterSelection_m90C39320C726E8E542D91F4BBD690697349F5385 (void);
// 0x0000013C System.Void TMPro.TMP_TextEventHandler::set_onCharacterSelection(TMPro.TMP_TextEventHandler/CharacterSelectionEvent)
extern void TMP_TextEventHandler_set_onCharacterSelection_m5ED7658EB101C6740A921FA150DE18C443BDA0C4 (void);
// 0x0000013D TMPro.TMP_TextEventHandler/SpriteSelectionEvent TMPro.TMP_TextEventHandler::get_onSpriteSelection()
extern void TMP_TextEventHandler_get_onSpriteSelection_m0E645AE1DFE19B011A3319474D0CF0DA612C8B7B (void);
// 0x0000013E System.Void TMPro.TMP_TextEventHandler::set_onSpriteSelection(TMPro.TMP_TextEventHandler/SpriteSelectionEvent)
extern void TMP_TextEventHandler_set_onSpriteSelection_m4AFC6772C3357218956A5D33B3CD19F3AAF39788 (void);
// 0x0000013F TMPro.TMP_TextEventHandler/WordSelectionEvent TMPro.TMP_TextEventHandler::get_onWordSelection()
extern void TMP_TextEventHandler_get_onWordSelection_mA42B89A37810FB659FCFA8539339A3BB8037203A (void);
// 0x00000140 System.Void TMPro.TMP_TextEventHandler::set_onWordSelection(TMPro.TMP_TextEventHandler/WordSelectionEvent)
extern void TMP_TextEventHandler_set_onWordSelection_m4A839FAFFABAFECD073B82BA8826E1CD033C0076 (void);
// 0x00000141 TMPro.TMP_TextEventHandler/LineSelectionEvent TMPro.TMP_TextEventHandler::get_onLineSelection()
extern void TMP_TextEventHandler_get_onLineSelection_mB701B6C713AD4EFC61E1B30A564EE54ADE31F58D (void);
// 0x00000142 System.Void TMPro.TMP_TextEventHandler::set_onLineSelection(TMPro.TMP_TextEventHandler/LineSelectionEvent)
extern void TMP_TextEventHandler_set_onLineSelection_m0B2337598350E51D0A17B8FCB3AAA533F312F3DA (void);
// 0x00000143 TMPro.TMP_TextEventHandler/LinkSelectionEvent TMPro.TMP_TextEventHandler::get_onLinkSelection()
extern void TMP_TextEventHandler_get_onLinkSelection_mF5C3875D661F5B1E3712566FE16D332EA37D15BB (void);
// 0x00000144 System.Void TMPro.TMP_TextEventHandler::set_onLinkSelection(TMPro.TMP_TextEventHandler/LinkSelectionEvent)
extern void TMP_TextEventHandler_set_onLinkSelection_m200566EDEB2C9299647F3EEAC588B51818D360A5 (void);
// 0x00000145 System.Void TMPro.TMP_TextEventHandler::Awake()
extern void TMP_TextEventHandler_Awake_m43EB03A4A6776A624F79457EC49E78E7B5BA1C70 (void);
// 0x00000146 System.Void TMPro.TMP_TextEventHandler::LateUpdate()
extern void TMP_TextEventHandler_LateUpdate_mE1D989C40DA8E54E116E3C60217DFCAADD6FDE11 (void);
// 0x00000147 System.Void TMPro.TMP_TextEventHandler::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextEventHandler_OnPointerEnter_m8FC88F25858B24CE68BE80C727A3F0227A8EE5AC (void);
// 0x00000148 System.Void TMPro.TMP_TextEventHandler::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextEventHandler_OnPointerExit_mBB2A74D55741F631A678A5D6997D40247FE75D44 (void);
// 0x00000149 System.Void TMPro.TMP_TextEventHandler::SendOnCharacterSelection(System.Char,System.Int32)
extern void TMP_TextEventHandler_SendOnCharacterSelection_m78983D3590F1B0C242BEAB0A11FDBDABDD1814EC (void);
// 0x0000014A System.Void TMPro.TMP_TextEventHandler::SendOnSpriteSelection(System.Char,System.Int32)
extern void TMP_TextEventHandler_SendOnSpriteSelection_m10C257A74F121B95E7077F7E488FBC52380A6C53 (void);
// 0x0000014B System.Void TMPro.TMP_TextEventHandler::SendOnWordSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventHandler_SendOnWordSelection_m95A4E5A00E339E5B8BA9AA63B98DB3E81C8B8F66 (void);
// 0x0000014C System.Void TMPro.TMP_TextEventHandler::SendOnLineSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventHandler_SendOnLineSelection_mE85BA6ECE1188B666FD36839B8970C3E0130BC43 (void);
// 0x0000014D System.Void TMPro.TMP_TextEventHandler::SendOnLinkSelection(System.String,System.String,System.Int32)
extern void TMP_TextEventHandler_SendOnLinkSelection_m299E6620DFD835C8258A3F48B4EA076C304E9B77 (void);
// 0x0000014E System.Void TMPro.TMP_TextEventHandler::.ctor()
extern void TMP_TextEventHandler__ctor_m6345DC1CEA2E4209E928AD1E61C3ACA4227DD9B8 (void);
// 0x0000014F System.Void TMPro.TMP_TextEventHandler/CharacterSelectionEvent::.ctor()
extern void CharacterSelectionEvent__ctor_m06BC183AF31BA4A2055A44514BC3FF0539DD04C7 (void);
// 0x00000150 System.Void TMPro.TMP_TextEventHandler/SpriteSelectionEvent::.ctor()
extern void SpriteSelectionEvent__ctor_m0F760052E9A5AF44A7AF7AC006CB4B24809590F2 (void);
// 0x00000151 System.Void TMPro.TMP_TextEventHandler/WordSelectionEvent::.ctor()
extern void WordSelectionEvent__ctor_m106CDEB17C520C9D20CE7120DE6BBBDEDB48886C (void);
// 0x00000152 System.Void TMPro.TMP_TextEventHandler/LineSelectionEvent::.ctor()
extern void LineSelectionEvent__ctor_m5D735FDA9B71B9147C6F791B331498F145D75018 (void);
// 0x00000153 System.Void TMPro.TMP_TextEventHandler/LinkSelectionEvent::.ctor()
extern void LinkSelectionEvent__ctor_m7AB7977D0D0F8883C5A98DD6BB2D390BC3CAB8E0 (void);
// 0x00000154 System.Collections.IEnumerator TMPro.Examples.Benchmark01::Start()
extern void Benchmark01_Start_mE7E5146B0D8D926CC410CAA48F3B617A0A7D955C (void);
// 0x00000155 System.Void TMPro.Examples.Benchmark01::.ctor()
extern void Benchmark01__ctor_mB92568AA7A9E13B92315B6270FCA23584A7D0F7F (void);
// 0x00000156 System.Void TMPro.Examples.Benchmark01/<Start>d__10::.ctor(System.Int32)
extern void U3CStartU3Ed__10__ctor_mBE5D8B4B98C372BD6DC3936437999DF3048DE3AB (void);
// 0x00000157 System.Void TMPro.Examples.Benchmark01/<Start>d__10::System.IDisposable.Dispose()
extern void U3CStartU3Ed__10_System_IDisposable_Dispose_m1F4180A7FDAE9AC5F11F30B67F813B3D7CD56D73 (void);
// 0x00000158 System.Boolean TMPro.Examples.Benchmark01/<Start>d__10::MoveNext()
extern void U3CStartU3Ed__10_MoveNext_mF3B1D38CD37FD5187C3192141DB380747C66AD3C (void);
// 0x00000159 System.Object TMPro.Examples.Benchmark01/<Start>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m22B2975D42326E3DE437174BBE4F0E8790CB6591 (void);
// 0x0000015A System.Void TMPro.Examples.Benchmark01/<Start>d__10::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m514DA545ECA10AA62BFC239BB582FF0017B91D2B (void);
// 0x0000015B System.Object TMPro.Examples.Benchmark01/<Start>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_m6904A738F04118BA24E473F1809B3D69E711627E (void);
// 0x0000015C System.Collections.IEnumerator TMPro.Examples.Benchmark01_UGUI::Start()
extern void Benchmark01_UGUI_Start_m2F8F9E1798943DA8086A7C7E73BA9D7C67482BD4 (void);
// 0x0000015D System.Void TMPro.Examples.Benchmark01_UGUI::.ctor()
extern void Benchmark01_UGUI__ctor_mACFE8D997EAB50FDBD7F671C1A25A892D9F78376 (void);
// 0x0000015E System.Void TMPro.Examples.Benchmark01_UGUI/<Start>d__10::.ctor(System.Int32)
extern void U3CStartU3Ed__10__ctor_m653B757B49A674E239C804FFF4EDEF325B5DB651 (void);
// 0x0000015F System.Void TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.IDisposable.Dispose()
extern void U3CStartU3Ed__10_System_IDisposable_Dispose_mAD0BC985E1E01C8369A7A955A60D3E545B0B3561 (void);
// 0x00000160 System.Boolean TMPro.Examples.Benchmark01_UGUI/<Start>d__10::MoveNext()
extern void U3CStartU3Ed__10_MoveNext_mF9B2C8D290035BC9A79C4347772B5B7B9D404DE1 (void);
// 0x00000161 System.Object TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF438D42D7C5A811FF9A283F8780F015FF55826CD (void);
// 0x00000162 System.Void TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m65545EB5B6DDDDAB5ACF92600137C1C754B322BA (void);
// 0x00000163 System.Object TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_mD4523BE1D1014AAF1C0709EB767C0B0F47D739D2 (void);
// 0x00000164 System.Void TMPro.Examples.Benchmark02::Start()
extern void Benchmark02_Start_m315C0DF3A9AC66B4F5802FDAED056E7E5F3D2046 (void);
// 0x00000165 System.Void TMPro.Examples.Benchmark02::.ctor()
extern void Benchmark02__ctor_m526B38D3B7E75905208E4E57B168D8AC1900F4E5 (void);
// 0x00000166 System.Void TMPro.Examples.Benchmark03::Awake()
extern void Benchmark03_Awake_mD23429C1EE428EEF4ED9A753385BF194BAA41A77 (void);
// 0x00000167 System.Void TMPro.Examples.Benchmark03::Start()
extern void Benchmark03_Start_m19E638DED29CB7F96362F3C346DC47217C4AF516 (void);
// 0x00000168 System.Void TMPro.Examples.Benchmark03::.ctor()
extern void Benchmark03__ctor_m89F9102259BE1694EC418E9023DC563F3999B0BE (void);
// 0x00000169 System.Void TMPro.Examples.Benchmark04::Start()
extern void Benchmark04_Start_mEABD467C12547066E33359FDC433E8B5BAD43DB9 (void);
// 0x0000016A System.Void TMPro.Examples.Benchmark04::.ctor()
extern void Benchmark04__ctor_m5015375E0C8D19060CB5DE14CBF4AC02DDD70E7C (void);
// 0x0000016B System.Void TMPro.Examples.CameraController::Awake()
extern void CameraController_Awake_m420393892377B9703EC97764B34E32890FC5283E (void);
// 0x0000016C System.Void TMPro.Examples.CameraController::Start()
extern void CameraController_Start_m32D90EE7232BE6DE08D224F824F8EB9571655610 (void);
// 0x0000016D System.Void TMPro.Examples.CameraController::LateUpdate()
extern void CameraController_LateUpdate_m6D81DEBA4E8B443CF2AD8288F0E76E3A6B3B5373 (void);
// 0x0000016E System.Void TMPro.Examples.CameraController::GetPlayerInput()
extern void CameraController_GetPlayerInput_m6695FE20CFC691585A6AC279EDB338EC9DD13FE3 (void);
// 0x0000016F System.Void TMPro.Examples.CameraController::.ctor()
extern void CameraController__ctor_m09187FB27B590118043D4DC7B89B93164124C124 (void);
// 0x00000170 System.Void TMPro.Examples.ObjectSpin::Awake()
extern void ObjectSpin_Awake_mC1EB9630B6D3BAE645D3DD79C264F71F7B18A1AA (void);
// 0x00000171 System.Void TMPro.Examples.ObjectSpin::Update()
extern void ObjectSpin_Update_mD39DCBA0789DC0116037C442F0BA1EE6752E36D3 (void);
// 0x00000172 System.Void TMPro.Examples.ObjectSpin::.ctor()
extern void ObjectSpin__ctor_m1827B9648659746252026432DFED907AEC6007FC (void);
// 0x00000173 System.Void TMPro.Examples.ShaderPropAnimator::Awake()
extern void ShaderPropAnimator_Awake_mE04C66A41CA53AB73733E7D2CCD21B30A360C5A8 (void);
// 0x00000174 System.Void TMPro.Examples.ShaderPropAnimator::Start()
extern void ShaderPropAnimator_Start_mC5AC59C59AF2F71E0CF65F11ACD787488140EDD2 (void);
// 0x00000175 System.Collections.IEnumerator TMPro.Examples.ShaderPropAnimator::AnimateProperties()
extern void ShaderPropAnimator_AnimateProperties_mAF49CD157AD41377CE00AA10F0C06C8BF5AA0469 (void);
// 0x00000176 System.Void TMPro.Examples.ShaderPropAnimator::.ctor()
extern void ShaderPropAnimator__ctor_mAA626BC8AEEB00C5AE362FE8690D3F2200CE6E64 (void);
// 0x00000177 System.Void TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::.ctor(System.Int32)
extern void U3CAnimatePropertiesU3Ed__6__ctor_mFAC0F8A7368D9D35AD2780C118E13414DA79B56A (void);
// 0x00000178 System.Void TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.IDisposable.Dispose()
extern void U3CAnimatePropertiesU3Ed__6_System_IDisposable_Dispose_m628EECBFCBC49087298185F17BC2AE7A73FC8A7B (void);
// 0x00000179 System.Boolean TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::MoveNext()
extern void U3CAnimatePropertiesU3Ed__6_MoveNext_m8A58CCFDAE59F55AB1BB2C103800886E62C807CF (void);
// 0x0000017A System.Object TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimatePropertiesU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0742A0FA62C518EBD119ED5FEBF849F918E12390 (void);
// 0x0000017B System.Void TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.Collections.IEnumerator.Reset()
extern void U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_Reset_mC076F281DD17668D6CE42EB04EA974DE1FFB3F6B (void);
// 0x0000017C System.Object TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_get_Current_mA585E786DA86D2589EBCA029AD9E64E8B5135362 (void);
// 0x0000017D System.Void TMPro.Examples.SimpleScript::Start()
extern void SimpleScript_Start_m75CD9CEDCAFA9A991753478D7C28407C7329FB8F (void);
// 0x0000017E System.Void TMPro.Examples.SimpleScript::Update()
extern void SimpleScript_Update_mE8C3930DCC1767C2DF59B622FABD188E6FB91BE5 (void);
// 0x0000017F System.Void TMPro.Examples.SimpleScript::.ctor()
extern void SimpleScript__ctor_mEB370A69544227EF04C48C604A28502ADAA73697 (void);
// 0x00000180 System.Void TMPro.Examples.SkewTextExample::Awake()
extern void SkewTextExample_Awake_m6FBA7E7DC9AFDD3099F0D0B9CDE91574551105DB (void);
// 0x00000181 System.Void TMPro.Examples.SkewTextExample::Start()
extern void SkewTextExample_Start_m536F82F3A5D229332694688C59F396E07F88153F (void);
// 0x00000182 UnityEngine.AnimationCurve TMPro.Examples.SkewTextExample::CopyAnimationCurve(UnityEngine.AnimationCurve)
extern void SkewTextExample_CopyAnimationCurve_m555177255F5828DBC7E677ED06F7EFFC052886B3 (void);
// 0x00000183 System.Collections.IEnumerator TMPro.Examples.SkewTextExample::WarpText()
extern void SkewTextExample_WarpText_m0E0C46988600673F0E5DFA3133534DC6CA5950D3 (void);
// 0x00000184 System.Void TMPro.Examples.SkewTextExample::.ctor()
extern void SkewTextExample__ctor_m945059906734DD38CF860CD32B3E07635D0E1F86 (void);
// 0x00000185 System.Void TMPro.Examples.SkewTextExample/<WarpText>d__7::.ctor(System.Int32)
extern void U3CWarpTextU3Ed__7__ctor_mEAD3C39209B75514446A44B6C2FA76F8097EBD6F (void);
// 0x00000186 System.Void TMPro.Examples.SkewTextExample/<WarpText>d__7::System.IDisposable.Dispose()
extern void U3CWarpTextU3Ed__7_System_IDisposable_Dispose_m32AA7120BE15547799BDC515FA3486488952BDCF (void);
// 0x00000187 System.Boolean TMPro.Examples.SkewTextExample/<WarpText>d__7::MoveNext()
extern void U3CWarpTextU3Ed__7_MoveNext_mB832E4A3DFDDFECC460A510BBC664F218B3D7FCF (void);
// 0x00000188 System.Object TMPro.Examples.SkewTextExample/<WarpText>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWarpTextU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3CF506B740C16FEDE58B6E1BE4556CAC2C6AEAD1 (void);
// 0x00000189 System.Void TMPro.Examples.SkewTextExample/<WarpText>d__7::System.Collections.IEnumerator.Reset()
extern void U3CWarpTextU3Ed__7_System_Collections_IEnumerator_Reset_mDDCDBB794F21DF70178A75747D8D8398F38A9C2F (void);
// 0x0000018A System.Object TMPro.Examples.SkewTextExample/<WarpText>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CWarpTextU3Ed__7_System_Collections_IEnumerator_get_Current_m6D89CF1910F76577E23C924B535E93FD1B01CF0A (void);
// 0x0000018B System.Void TMPro.Examples.TMP_ExampleScript_01::Awake()
extern void TMP_ExampleScript_01_Awake_m381EF5B50E1D012B8CA1883DEFF604EEAE6D95F4 (void);
// 0x0000018C System.Void TMPro.Examples.TMP_ExampleScript_01::Update()
extern void TMP_ExampleScript_01_Update_m31611F22A608784F164A24444195B33B714567FB (void);
// 0x0000018D System.Void TMPro.Examples.TMP_ExampleScript_01::.ctor()
extern void TMP_ExampleScript_01__ctor_m3641F2E0D25B6666CE77773A4A493C4C4D3D3A12 (void);
// 0x0000018E System.Void TMPro.Examples.TMP_FrameRateCounter::Awake()
extern void TMP_FrameRateCounter_Awake_m958B668086DB4A38D9C56E3F8C2DCCB6FF11FAA3 (void);
// 0x0000018F System.Void TMPro.Examples.TMP_FrameRateCounter::Start()
extern void TMP_FrameRateCounter_Start_mC642CA714D0FB8482D2AC6192D976DA179EE3720 (void);
// 0x00000190 System.Void TMPro.Examples.TMP_FrameRateCounter::Update()
extern void TMP_FrameRateCounter_Update_m5E41B55573D86C3D345BFE11C489B00229BCEE21 (void);
// 0x00000191 System.Void TMPro.Examples.TMP_FrameRateCounter::Set_FrameCounter_Position(TMPro.Examples.TMP_FrameRateCounter/FpsCounterAnchorPositions)
extern void TMP_FrameRateCounter_Set_FrameCounter_Position_mA72ECDE464E3290E4F533491FC8113B8D4068BE1 (void);
// 0x00000192 System.Void TMPro.Examples.TMP_FrameRateCounter::.ctor()
extern void TMP_FrameRateCounter__ctor_mC79D5BF3FAE2BB7D22D9955A75E3483725BA619C (void);
// 0x00000193 System.Void TMPro.Examples.TMP_TextEventCheck::OnEnable()
extern void TMP_TextEventCheck_OnEnable_mE6D5125EEE0720E6F414978A496066E7CF1592DF (void);
// 0x00000194 System.Void TMPro.Examples.TMP_TextEventCheck::OnDisable()
extern void TMP_TextEventCheck_OnDisable_m94F087C259890C48D4F5464ABA4CD1D0E5863A9A (void);
// 0x00000195 System.Void TMPro.Examples.TMP_TextEventCheck::OnCharacterSelection(System.Char,System.Int32)
extern void TMP_TextEventCheck_OnCharacterSelection_mDA044F0808D61A99CDA374075943CEB1C92C253D (void);
// 0x00000196 System.Void TMPro.Examples.TMP_TextEventCheck::OnSpriteSelection(System.Char,System.Int32)
extern void TMP_TextEventCheck_OnSpriteSelection_mE8FFA550F38F5447CA37205698A30A41ADE901E0 (void);
// 0x00000197 System.Void TMPro.Examples.TMP_TextEventCheck::OnWordSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventCheck_OnWordSelection_m6037166D18A938678A2B06F28A5DCB3E09FAC61B (void);
// 0x00000198 System.Void TMPro.Examples.TMP_TextEventCheck::OnLineSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventCheck_OnLineSelection_mD2BAA6C8ABD61F0442A40C5448FA7774D782C363 (void);
// 0x00000199 System.Void TMPro.Examples.TMP_TextEventCheck::OnLinkSelection(System.String,System.String,System.Int32)
extern void TMP_TextEventCheck_OnLinkSelection_m184B11D5E35AC4EA7CDCE3340AB9FEE3C14BF41C (void);
// 0x0000019A System.Void TMPro.Examples.TMP_TextEventCheck::.ctor()
extern void TMP_TextEventCheck__ctor_mA5E82EE7CE8F8836FC6CF3823CBC7356005B898B (void);
// 0x0000019B System.Void TMPro.Examples.TMP_TextInfoDebugTool::.ctor()
extern void TMP_TextInfoDebugTool__ctor_mF6AA30660FBD4CE708B6147833853498993CB9EE (void);
// 0x0000019C System.Void TMPro.Examples.TMP_TextSelector_A::Awake()
extern void TMP_TextSelector_A_Awake_mD7252A5075E30E3BF9BEF4353F7AA2A9203FC943 (void);
// 0x0000019D System.Void TMPro.Examples.TMP_TextSelector_A::LateUpdate()
extern void TMP_TextSelector_A_LateUpdate_mDBBC09726332EDDEAF7C30AB6C08FB33261F79FD (void);
// 0x0000019E System.Void TMPro.Examples.TMP_TextSelector_A::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_A_OnPointerEnter_mC19B85FB5B4E6EB47832C39F0115A513B85060D0 (void);
// 0x0000019F System.Void TMPro.Examples.TMP_TextSelector_A::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_A_OnPointerExit_mE51C850F54B18A5C96E3BE015DFBB0F079E5AE66 (void);
// 0x000001A0 System.Void TMPro.Examples.TMP_TextSelector_A::.ctor()
extern void TMP_TextSelector_A__ctor_mEB83D3952B32CEE9A871EC60E3AE9B79048302BF (void);
// 0x000001A1 System.Void TMPro.Examples.TMP_TextSelector_B::Awake()
extern void TMP_TextSelector_B_Awake_m7E413A43C54DF9E0FE70C4E69FC682391B47205A (void);
// 0x000001A2 System.Void TMPro.Examples.TMP_TextSelector_B::OnEnable()
extern void TMP_TextSelector_B_OnEnable_m0828D13E2D407B90038442228D54FB0D7B3D29FB (void);
// 0x000001A3 System.Void TMPro.Examples.TMP_TextSelector_B::OnDisable()
extern void TMP_TextSelector_B_OnDisable_m2B559A85B52C8CAFC7350CC7B4F8E5BC773EF781 (void);
// 0x000001A4 System.Void TMPro.Examples.TMP_TextSelector_B::ON_TEXT_CHANGED(UnityEngine.Object)
extern void TMP_TextSelector_B_ON_TEXT_CHANGED_m1597DBE7C7EBE7CFA4DC395897A4779387B59910 (void);
// 0x000001A5 System.Void TMPro.Examples.TMP_TextSelector_B::LateUpdate()
extern void TMP_TextSelector_B_LateUpdate_m8BA10E368C7F3483E9EC05589BBE45D7EFC75691 (void);
// 0x000001A6 System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerEnter_m0A0064632E4C0E0ADCD4358AD5BC168BFB74AC4D (void);
// 0x000001A7 System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerExit_m667659102B5B17FBAF56593B7034E5EC1C48D43F (void);
// 0x000001A8 System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerClick_m8DB2D22EE2F4D3965115791C81F985D82021469F (void);
// 0x000001A9 System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerUp_m303500BE309B56F3ADCE8B461CEC50C8D5ED70BC (void);
// 0x000001AA System.Void TMPro.Examples.TMP_TextSelector_B::RestoreCachedVertexAttributes(System.Int32)
extern void TMP_TextSelector_B_RestoreCachedVertexAttributes_m3D637CF2C6CA663922EBE56FFD672BE582E9F1F2 (void);
// 0x000001AB System.Void TMPro.Examples.TMP_TextSelector_B::.ctor()
extern void TMP_TextSelector_B__ctor_mE654F9F1570570C4BACDA79640B8DB3033D91C33 (void);
// 0x000001AC System.Void TMPro.Examples.TMP_UiFrameRateCounter::Awake()
extern void TMP_UiFrameRateCounter_Awake_m83DBE22B6CC551BE5E385048B92067679716179E (void);
// 0x000001AD System.Void TMPro.Examples.TMP_UiFrameRateCounter::Start()
extern void TMP_UiFrameRateCounter_Start_m59DA342A492C9EF8AD5C4512753211BF3796C944 (void);
// 0x000001AE System.Void TMPro.Examples.TMP_UiFrameRateCounter::Update()
extern void TMP_UiFrameRateCounter_Update_m28FC233C475AA15A3BE399BF9345977089997749 (void);
// 0x000001AF System.Void TMPro.Examples.TMP_UiFrameRateCounter::Set_FrameCounter_Position(TMPro.Examples.TMP_UiFrameRateCounter/FpsCounterAnchorPositions)
extern void TMP_UiFrameRateCounter_Set_FrameCounter_Position_m2025933902F312C0EC4B6A864196A8BA8D545647 (void);
// 0x000001B0 System.Void TMPro.Examples.TMP_UiFrameRateCounter::.ctor()
extern void TMP_UiFrameRateCounter__ctor_m5774BF4B9389770FC34991095557B24417600F84 (void);
// 0x000001B1 System.Void TMPro.Examples.TMPro_InstructionOverlay::Awake()
extern void TMPro_InstructionOverlay_Awake_m2444EA8749D72BCEA4DC30A328E3745AB19062EC (void);
// 0x000001B2 System.Void TMPro.Examples.TMPro_InstructionOverlay::Set_FrameCounter_Position(TMPro.Examples.TMPro_InstructionOverlay/FpsCounterAnchorPositions)
extern void TMPro_InstructionOverlay_Set_FrameCounter_Position_m62B897E4DB2D6B1936833EC54D9C246D68D50EEB (void);
// 0x000001B3 System.Void TMPro.Examples.TMPro_InstructionOverlay::.ctor()
extern void TMPro_InstructionOverlay__ctor_m7AB5B851B4BFB07547E460D6B7B9D969DEB4A7CF (void);
// 0x000001B4 System.Void TMPro.Examples.TeleType::Awake()
extern void TeleType_Awake_m099FCB202F2A8299B133DC56ECB9D01A16C08DFE (void);
// 0x000001B5 System.Collections.IEnumerator TMPro.Examples.TeleType::Start()
extern void TeleType_Start_m72DE9DE597F4FA0B1CA02115CBC1E74EB53F63F7 (void);
// 0x000001B6 System.Void TMPro.Examples.TeleType::.ctor()
extern void TeleType__ctor_m12A79B34F66CBFDCA8F582F0689D1731586B90AF (void);
// 0x000001B7 System.Void TMPro.Examples.TeleType/<Start>d__4::.ctor(System.Int32)
extern void U3CStartU3Ed__4__ctor_mF3575DBEBF4F153F6A899AF3362940298C11B629 (void);
// 0x000001B8 System.Void TMPro.Examples.TeleType/<Start>d__4::System.IDisposable.Dispose()
extern void U3CStartU3Ed__4_System_IDisposable_Dispose_m6DABFDBC2A313BF6DD90AA1C43B4EF9D6249CBE9 (void);
// 0x000001B9 System.Boolean TMPro.Examples.TeleType/<Start>d__4::MoveNext()
extern void U3CStartU3Ed__4_MoveNext_mEF7A3215376BDFB52C3DA9D26FF0459074E89715 (void);
// 0x000001BA System.Object TMPro.Examples.TeleType/<Start>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m66DC06D52865D30DAA78DBD70B6554D13E2EA70B (void);
// 0x000001BB System.Void TMPro.Examples.TeleType/<Start>d__4::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_mE854A2CECA0E01F3C2D90DA4F720CF0F387994F8 (void);
// 0x000001BC System.Object TMPro.Examples.TeleType/<Start>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_m77D3936B94684C817EE9F6D2C903238A457D5261 (void);
// 0x000001BD System.Void TMPro.Examples.TextConsoleSimulator::Awake()
extern void TextConsoleSimulator_Awake_mA51EE182A528CCA5CAEA8DBE8AD30E43FDFAE7B0 (void);
// 0x000001BE System.Void TMPro.Examples.TextConsoleSimulator::Start()
extern void TextConsoleSimulator_Start_m429701BE4C9A52AA6B257172A4D85D58E091E7DA (void);
// 0x000001BF System.Void TMPro.Examples.TextConsoleSimulator::OnEnable()
extern void TextConsoleSimulator_OnEnable_m3397FBCDA8D9DA264D2149288BE4DCF63368AB50 (void);
// 0x000001C0 System.Void TMPro.Examples.TextConsoleSimulator::OnDisable()
extern void TextConsoleSimulator_OnDisable_m43DF869E864789E215873192ECFCDC51ABA79712 (void);
// 0x000001C1 System.Void TMPro.Examples.TextConsoleSimulator::ON_TEXT_CHANGED(UnityEngine.Object)
extern void TextConsoleSimulator_ON_TEXT_CHANGED_m51F06EE5DD9B32FEFB529743F209C6E6C41BE3B8 (void);
// 0x000001C2 System.Collections.IEnumerator TMPro.Examples.TextConsoleSimulator::RevealCharacters(TMPro.TMP_Text)
extern void TextConsoleSimulator_RevealCharacters_mE8E415644F7BD2056D0809F7BB0FA9A2F9FE8534 (void);
// 0x000001C3 System.Collections.IEnumerator TMPro.Examples.TextConsoleSimulator::RevealWords(TMPro.TMP_Text)
extern void TextConsoleSimulator_RevealWords_mFDBC863D30BC63ADCF2860F303AF252E27D0F4F4 (void);
// 0x000001C4 System.Void TMPro.Examples.TextConsoleSimulator::.ctor()
extern void TextConsoleSimulator__ctor_m4719FB9D4D89F37234757D93876DF0193E8E2848 (void);
// 0x000001C5 System.Void TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::.ctor(System.Int32)
extern void U3CRevealCharactersU3Ed__7__ctor_mD45A85F5F50909F70C80AC8CE460F4FD261CDE9D (void);
// 0x000001C6 System.Void TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.IDisposable.Dispose()
extern void U3CRevealCharactersU3Ed__7_System_IDisposable_Dispose_m85F270FDC11A4D79E9CF47AADC9FA1FBF032F86C (void);
// 0x000001C7 System.Boolean TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::MoveNext()
extern void U3CRevealCharactersU3Ed__7_MoveNext_mA9A6555E50889A7AA73F20749F25989165023DE3 (void);
// 0x000001C8 System.Object TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRevealCharactersU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5BBAE6868EB7F1C11BE5DF001641E18C9D625F83 (void);
// 0x000001C9 System.Void TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.Collections.IEnumerator.Reset()
extern void U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_Reset_mA9F4893381AB24E01EAEBFC38A88AF363A9A0691 (void);
// 0x000001CA System.Object TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_get_Current_mFD0B7538B1A650FB389FFE9296B0E51AEA5B6B6F (void);
// 0x000001CB System.Void TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::.ctor(System.Int32)
extern void U3CRevealWordsU3Ed__8__ctor_m22FF9E770988107A928C5D1EA639F60239BFFEF0 (void);
// 0x000001CC System.Void TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.IDisposable.Dispose()
extern void U3CRevealWordsU3Ed__8_System_IDisposable_Dispose_m3EAF6EF4A8C99A71FEED251BF3F28A26BF6AD7F8 (void);
// 0x000001CD System.Boolean TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::MoveNext()
extern void U3CRevealWordsU3Ed__8_MoveNext_m3811753E2384D4CBBAD6BD712EBA4FAF00D73210 (void);
// 0x000001CE System.Object TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRevealWordsU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA3F93E4AA532F15D52D68B7121805C014AB2D7FB (void);
// 0x000001CF System.Void TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.Collections.IEnumerator.Reset()
extern void U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_Reset_mCDC8982948ED5F7743567569CA1D4A354218714F (void);
// 0x000001D0 System.Object TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_get_Current_mF39E492D576810F58DB9031556CFD6806FD32E27 (void);
// 0x000001D1 System.Void TMPro.Examples.TextMeshProFloatingText::Awake()
extern void TextMeshProFloatingText_Awake_m679E597FF18E192C1FBD263D0E5ECEA392412046 (void);
// 0x000001D2 System.Void TMPro.Examples.TextMeshProFloatingText::Start()
extern void TextMeshProFloatingText_Start_m68E511DEEDA883FE0F0999B329451F3A8A7269B1 (void);
// 0x000001D3 System.Collections.IEnumerator TMPro.Examples.TextMeshProFloatingText::DisplayTextMeshProFloatingText()
extern void TextMeshProFloatingText_DisplayTextMeshProFloatingText_m3C381B8A53C58CF001D7A9212B8BDA6F368CC2C7 (void);
// 0x000001D4 System.Collections.IEnumerator TMPro.Examples.TextMeshProFloatingText::DisplayTextMeshFloatingText()
extern void TextMeshProFloatingText_DisplayTextMeshFloatingText_m22691E6EA41B7FCF782B03954865D8E3B890E4DF (void);
// 0x000001D5 System.Void TMPro.Examples.TextMeshProFloatingText::.ctor()
extern void TextMeshProFloatingText__ctor_m968E2691E21A93C010CF8205BC3666ADF712457E (void);
// 0x000001D6 System.Void TMPro.Examples.TextMeshProFloatingText::.cctor()
extern void TextMeshProFloatingText__cctor_m5FF3949FBDB0FD13908FB7349F64DDB3012512B8 (void);
// 0x000001D7 System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::.ctor(System.Int32)
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15__ctor_m3F7F9561735FF8403090A93E708E781B3997B81B (void);
// 0x000001D8 System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::System.IDisposable.Dispose()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_System_IDisposable_Dispose_m6F24E1DC5777D4682375D8742BC9272EE6D4EA2C (void);
// 0x000001D9 System.Boolean TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::MoveNext()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_MoveNext_m04B1BB9074CBA76BA7F63849721F4CAEA1045A28 (void);
// 0x000001DA System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m14A7C9DEBD0FF0C7A6979AFEC78B0BEE9E4C3963 (void);
// 0x000001DB System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::System.Collections.IEnumerator.Reset()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_IEnumerator_Reset_mC0D5548A92F57550F60EE76E17B58125B57B94A8 (void);
// 0x000001DC System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::System.Collections.IEnumerator.get_Current()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_IEnumerator_get_Current_mD3CBFD6F8558EF09F2B4A3D8D1B76BD7FDB3A98B (void);
// 0x000001DD System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::.ctor(System.Int32)
extern void U3CDisplayTextMeshFloatingTextU3Ed__16__ctor_m0466586E44A01852547C6CECCA91C14673B98C20 (void);
// 0x000001DE System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::System.IDisposable.Dispose()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_System_IDisposable_Dispose_m8730A3A2EFBF759A51F0E5330C3BBF8EFC1736A3 (void);
// 0x000001DF System.Boolean TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::MoveNext()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_MoveNext_m93E19CF96C4EBC57CC4E03E2D3D0B56B3E92A36A (void);
// 0x000001E0 System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA4076BE73343A473EFE46E73ABA5DE66CC797598 (void);
// 0x000001E1 System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::System.Collections.IEnumerator.Reset()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_IEnumerator_Reset_mC33B355BB392C5A52DCA0AAF2588E1E78AF6D3A5 (void);
// 0x000001E2 System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::System.Collections.IEnumerator.get_Current()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_IEnumerator_get_Current_m28CCE2E51295AD5F368C504BADDDFF008C2CC0A6 (void);
// 0x000001E3 System.Void TMPro.Examples.TextMeshSpawner::Awake()
extern void TextMeshSpawner_Awake_mDF8CCC9C6B7380D6FA027263CDC3BC00EF75AEFB (void);
// 0x000001E4 System.Void TMPro.Examples.TextMeshSpawner::Start()
extern void TextMeshSpawner_Start_m7CC21883CA786A846A44921D2E37C201C25439BA (void);
// 0x000001E5 System.Void TMPro.Examples.TextMeshSpawner::.ctor()
extern void TextMeshSpawner__ctor_m1255777673BE591F62B4FC55EB999DBD7A7CB5A1 (void);
// 0x000001E6 System.Void TMPro.Examples.VertexColorCycler::Awake()
extern void VertexColorCycler_Awake_m84B4548078500DA811F3ADFF66186373BE8EDABC (void);
// 0x000001E7 System.Void TMPro.Examples.VertexColorCycler::Start()
extern void VertexColorCycler_Start_mC3FF90808EDD6A02F08375E909254778D5268B66 (void);
// 0x000001E8 System.Collections.IEnumerator TMPro.Examples.VertexColorCycler::AnimateVertexColors()
extern void VertexColorCycler_AnimateVertexColors_m6960F777E4876DFCA726BCAE7A8163850D58FA42 (void);
// 0x000001E9 System.Void TMPro.Examples.VertexColorCycler::.ctor()
extern void VertexColorCycler__ctor_m09990A8066C8A957A96CDBEBDA980399283B45E9 (void);
// 0x000001EA System.Void TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__3__ctor_m0038B1054BCC928D35F8C0021ED7D2E1C533E35F (void);
// 0x000001EB System.Void TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__3_System_IDisposable_Dispose_m5BAD394A0B09B3E0FF19E91521E02C2B3ADD6007 (void);
// 0x000001EC System.Boolean TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__3_MoveNext_m2842AF5B12AFC17112D1AE75E46AB1B12776D2A6 (void);
// 0x000001ED System.Object TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAE810D7968957A09C88E61C29DAAEC68E4AF1E51 (void);
// 0x000001EE System.Void TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_Reset_m72ABAC50E9E4D972FB44CAFF387F3E23FEC5D932 (void);
// 0x000001EF System.Object TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_get_Current_m8DE595A1D01F3A507A356F8BCE020D0851412B52 (void);
// 0x000001F0 System.Void TMPro.Examples.VertexJitter::Awake()
extern void VertexJitter_Awake_m9C5586A35BD9C928455D6479C93C1CE095447D8F (void);
// 0x000001F1 System.Void TMPro.Examples.VertexJitter::OnEnable()
extern void VertexJitter_OnEnable_m97ED60DBD350C72D1436ADFB8009A6F33A78C825 (void);
// 0x000001F2 System.Void TMPro.Examples.VertexJitter::OnDisable()
extern void VertexJitter_OnDisable_mF8D32D6E02E41A73C3E958FB6EE5D1D659D2A846 (void);
// 0x000001F3 System.Void TMPro.Examples.VertexJitter::Start()
extern void VertexJitter_Start_m8A0FED7ED16F90DBF287E09BFCBD7B26E07DBF97 (void);
// 0x000001F4 System.Void TMPro.Examples.VertexJitter::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexJitter_ON_TEXT_CHANGED_mC7AB0113F6823D4FF415A096314B55D9C1BE9549 (void);
// 0x000001F5 System.Collections.IEnumerator TMPro.Examples.VertexJitter::AnimateVertexColors()
extern void VertexJitter_AnimateVertexColors_mECAE037FC0CBA52CAC71C0B61E88829FF18BCC16 (void);
// 0x000001F6 System.Void TMPro.Examples.VertexJitter::.ctor()
extern void VertexJitter__ctor_m550C9169D6FCD6F60D6AABCB8B4955DF58A12DCE (void);
// 0x000001F7 System.Void TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__11__ctor_m0222C3457F5ACA497FE3A8EC829DE4AD11A169F8 (void);
// 0x000001F8 System.Void TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m717E79A39A8161ADDA9E62F7CDFB67B8F2D65099 (void);
// 0x000001F9 System.Boolean TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__11_MoveNext_m169A75C7E2147372CB933520B670AF77907C1C6B (void);
// 0x000001FA System.Object TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m920EBA43D59A1A88E27FED92CF0AC0DF90179479 (void);
// 0x000001FB System.Void TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_m6C045A6DD0B60F1512457448E615877EAB86D75D (void);
// 0x000001FC System.Object TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m017345FE58B497EAFC9D0CB1FB733F76EB3449AF (void);
// 0x000001FD System.Void TMPro.Examples.VertexShakeA::Awake()
extern void VertexShakeA_Awake_m005C6F9EE8A8FD816CD3A736D6AF199CD2B615A2 (void);
// 0x000001FE System.Void TMPro.Examples.VertexShakeA::OnEnable()
extern void VertexShakeA_OnEnable_m9F60F3951A7D0DF4201A0409F0ADC05243D324EA (void);
// 0x000001FF System.Void TMPro.Examples.VertexShakeA::OnDisable()
extern void VertexShakeA_OnDisable_m59B86895C03B278B2E634A7C68CC942344A8D2D2 (void);
// 0x00000200 System.Void TMPro.Examples.VertexShakeA::Start()
extern void VertexShakeA_Start_m3E623C28F873F54F4AC7579433C7C50B2A3319DE (void);
// 0x00000201 System.Void TMPro.Examples.VertexShakeA::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexShakeA_ON_TEXT_CHANGED_m7AD31F3239351E0916E4D02148F46A80DC148D7E (void);
// 0x00000202 System.Collections.IEnumerator TMPro.Examples.VertexShakeA::AnimateVertexColors()
extern void VertexShakeA_AnimateVertexColors_mC451E2732A4E3E90E2553811AD75903EEF974599 (void);
// 0x00000203 System.Void TMPro.Examples.VertexShakeA::.ctor()
extern void VertexShakeA__ctor_m7E6FD1700BD08616532AF22B3B489A18B88DFB62 (void);
// 0x00000204 System.Void TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__11__ctor_m92612416BEC0EBF5E9849FB603629C0F2F95FEF2 (void);
// 0x00000205 System.Void TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m77D966994D4717EAFD8EFE169F3E8A4EE8B05B81 (void);
// 0x00000206 System.Boolean TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__11_MoveNext_mCEEDE03667D329386BB4AE7B8252B7A9B54F443F (void);
// 0x00000207 System.Object TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDCCD3645ACF9B18D760B341C863F853996FA9BCE (void);
// 0x00000208 System.Void TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_mB9444B5E58B4E97105C447F547C0F74C51BCFBFA (void);
// 0x00000209 System.Object TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m5CC464185D5C0251C6206E20AFFA681BA0525A7E (void);
// 0x0000020A System.Void TMPro.Examples.VertexShakeB::Awake()
extern void VertexShakeB_Awake_m99C9A0474DBFE462DC88C898F958C1805376F9D5 (void);
// 0x0000020B System.Void TMPro.Examples.VertexShakeB::OnEnable()
extern void VertexShakeB_OnEnable_m64299258797D745CDFE7F3CBEC4708BDBC7C3971 (void);
// 0x0000020C System.Void TMPro.Examples.VertexShakeB::OnDisable()
extern void VertexShakeB_OnDisable_m07D520A8D7BCD8D188CE9F5CC7845F47D5AD6EF4 (void);
// 0x0000020D System.Void TMPro.Examples.VertexShakeB::Start()
extern void VertexShakeB_Start_m9642281210FA5F701A324B78850331E4638B2DD1 (void);
// 0x0000020E System.Void TMPro.Examples.VertexShakeB::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexShakeB_ON_TEXT_CHANGED_m5650D69D258D0EEF35AF390D6D5086B327B308A5 (void);
// 0x0000020F System.Collections.IEnumerator TMPro.Examples.VertexShakeB::AnimateVertexColors()
extern void VertexShakeB_AnimateVertexColors_mDF7A7E7028361D288DF588D9301541E4FA1EFA87 (void);
// 0x00000210 System.Void TMPro.Examples.VertexShakeB::.ctor()
extern void VertexShakeB__ctor_m605A778C36B506B5763A1AE17B01F8DCCBCD51EC (void);
// 0x00000211 System.Void TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__10__ctor_m3E3B1D286DEBED2BC028AD490308568B930C3760 (void);
// 0x00000212 System.Void TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_m0F9D4B2A6ED0500C2120DBA29932CC279E8908DC (void);
// 0x00000213 System.Boolean TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__10_MoveNext_m0534E436514E663BF024364E524EE5716FF15C8E (void);
// 0x00000214 System.Object TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF2A8BAFC0261ACBBE8EEA74DE4B498D30C68AE3D (void);
// 0x00000215 System.Void TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_m5F12D7C6F2523BEB6326FE49AE972116D6157CBB (void);
// 0x00000216 System.Object TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_mB9C12786011A3B78517AEFAE8D5A78B95A4219AD (void);
// 0x00000217 System.Void TMPro.Examples.VertexZoom::Awake()
extern void VertexZoom_Awake_m1B5D386B98CF2EB05A8155B238D6F6E8275D181C (void);
// 0x00000218 System.Void TMPro.Examples.VertexZoom::OnEnable()
extern void VertexZoom_OnEnable_m7F980FC038FC2534C428A5FD33E3E13AEAEB4EEC (void);
// 0x00000219 System.Void TMPro.Examples.VertexZoom::OnDisable()
extern void VertexZoom_OnDisable_m880C38B62B4BB05AF49F12F75B83FFA2F0519884 (void);
// 0x0000021A System.Void TMPro.Examples.VertexZoom::Start()
extern void VertexZoom_Start_m10A182DCEF8D430DADAFBFFA3F04171F8E60C84C (void);
// 0x0000021B System.Void TMPro.Examples.VertexZoom::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexZoom_ON_TEXT_CHANGED_mB696E443C61D2212AC93A7615AA58106DD25250F (void);
// 0x0000021C System.Collections.IEnumerator TMPro.Examples.VertexZoom::AnimateVertexColors()
extern void VertexZoom_AnimateVertexColors_mEFBA6C940DFECB485236C247358A32011A7963ED (void);
// 0x0000021D System.Void TMPro.Examples.VertexZoom::.ctor()
extern void VertexZoom__ctor_mE7B36F2D3EC8EF397AB0AD7A19E988C06927A3B2 (void);
// 0x0000021E System.Void TMPro.Examples.VertexZoom/<>c__DisplayClass10_0::.ctor()
extern void U3CU3Ec__DisplayClass10_0__ctor_m4EC5F8042B5AC645EA7D0913E50FD22144DBD348 (void);
// 0x0000021F System.Int32 TMPro.Examples.VertexZoom/<>c__DisplayClass10_0::<AnimateVertexColors>b__0(System.Int32,System.Int32)
extern void U3CU3Ec__DisplayClass10_0_U3CAnimateVertexColorsU3Eb__0_m959A7E1D6162B5AAF28B953AFB04D7943BCAB107 (void);
// 0x00000220 System.Void TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__10__ctor_m537CF0A5ADF5BBC2DF784BF526E3F32EB528E1B2 (void);
// 0x00000221 System.Void TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_mF0CD944C393771101D2718A660E1DFF22385819F (void);
// 0x00000222 System.Boolean TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__10_MoveNext_m8340EFEB2B2CF6EA1545CFB6967968CA171FEE66 (void);
// 0x00000223 System.Object TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m093E922A5A046910B2A7EE826D804CA3064A7BD9 (void);
// 0x00000224 System.Void TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_mDED63FD52741D58D8D5A3E53415F91B6560F680C (void);
// 0x00000225 System.Object TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_m933EBF7C4300C7C1E3EE60E4741110025877CA5F (void);
// 0x00000226 System.Void TMPro.Examples.WarpTextExample::Awake()
extern void WarpTextExample_Awake_mEDB072A485F3F37270FC736E1A201624D696B4B0 (void);
// 0x00000227 System.Void TMPro.Examples.WarpTextExample::Start()
extern void WarpTextExample_Start_m36448AEA494658D7C129C3B71BF3A64CC4DFEDC4 (void);
// 0x00000228 UnityEngine.AnimationCurve TMPro.Examples.WarpTextExample::CopyAnimationCurve(UnityEngine.AnimationCurve)
extern void WarpTextExample_CopyAnimationCurve_m744026E638662DA48AC81ED21E0589E3E4E05FAB (void);
// 0x00000229 System.Collections.IEnumerator TMPro.Examples.WarpTextExample::WarpText()
extern void WarpTextExample_WarpText_mA29C98CF3B92F253C2DD2BADFC76618F8324BEF2 (void);
// 0x0000022A System.Void TMPro.Examples.WarpTextExample::.ctor()
extern void WarpTextExample__ctor_m62299DFDB704027267321007E16DB87D93D0F099 (void);
// 0x0000022B System.Void TMPro.Examples.WarpTextExample/<WarpText>d__8::.ctor(System.Int32)
extern void U3CWarpTextU3Ed__8__ctor_m9FADE04C27A0034C5A276232FCA187AECDC6BF49 (void);
// 0x0000022C System.Void TMPro.Examples.WarpTextExample/<WarpText>d__8::System.IDisposable.Dispose()
extern void U3CWarpTextU3Ed__8_System_IDisposable_Dispose_m5AB0DCF4B3DE6C290487F3DBBC7BAE931130DE60 (void);
// 0x0000022D System.Boolean TMPro.Examples.WarpTextExample/<WarpText>d__8::MoveNext()
extern void U3CWarpTextU3Ed__8_MoveNext_m9C83C8455FF8ADFBAA8D05958C3048612A96EB84 (void);
// 0x0000022E System.Object TMPro.Examples.WarpTextExample/<WarpText>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWarpTextU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9D5776AF80C4227401ADD7E13D98F2530CB9E7A1 (void);
// 0x0000022F System.Void TMPro.Examples.WarpTextExample/<WarpText>d__8::System.Collections.IEnumerator.Reset()
extern void U3CWarpTextU3Ed__8_System_Collections_IEnumerator_Reset_m4F2FBBE7B375E6FC837736B4275A7B601A01F535 (void);
// 0x00000230 System.Object TMPro.Examples.WarpTextExample/<WarpText>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CWarpTextU3Ed__8_System_Collections_IEnumerator_get_Current_m01FA935D93D737C9DDD88A051F7DFC393DD7BD25 (void);
// 0x00000231 T Utilities.MonoSingleton`1::get_Instance()
// 0x00000232 System.Void Utilities.MonoSingleton`1::Awake()
// 0x00000233 System.Void Utilities.MonoSingleton`1::OnApplicationQuit()
// 0x00000234 System.Void Utilities.MonoSingleton`1::OnDestroy()
// 0x00000235 System.Void Utilities.MonoSingleton`1::.ctor()
// 0x00000236 System.Void Utilities.MonoSingleton`1::.cctor()
// 0x00000237 System.Void Manager.CharacterManager::Awake()
extern void CharacterManager_Awake_m64BD9F18F150A8AF7824482418781BA5BFC05E1B (void);
// 0x00000238 System.Void Manager.CharacterManager::StartGame()
extern void CharacterManager_StartGame_m207BD9A33CBFB94D3453A8B938485ECD96B40AAC (void);
// 0x00000239 System.Void Manager.CharacterManager::SpawnJennieCharacter()
extern void CharacterManager_SpawnJennieCharacter_m8D235ECBF5D7110B7C710CE441803DC66CC27756 (void);
// 0x0000023A System.Void Manager.CharacterManager::SpawnJohnCharacter()
extern void CharacterManager_SpawnJohnCharacter_m4A0F0D0FF185839AA170C9F6AF567837BEF9903B (void);
// 0x0000023B System.Void Manager.CharacterManager::.ctor()
extern void CharacterManager__ctor_m0EE2629BA31A11062677C9269765AED363DC3E86 (void);
// 0x0000023C System.Void Manager.UISceneGameplayManager::Awake()
extern void UISceneGameplayManager_Awake_m08663A590601B8083C7500056FA11FEF0ED62D35 (void);
// 0x0000023D System.Void Manager.UISceneGameplayManager::StartGame()
extern void UISceneGameplayManager_StartGame_mCA6A43C2B79AFFD0FF17FA82E07A07316DAFAFED (void);
// 0x0000023E System.Void Manager.UISceneGameplayManager::StartHpUI()
extern void UISceneGameplayManager_StartHpUI_mEB93FD04C48F01C6F85154B93C3C3B340E176085 (void);
// 0x0000023F System.Void Manager.UISceneGameplayManager::SetHpUI(System.Int32)
extern void UISceneGameplayManager_SetHpUI_m01F9FED0B054D3951D2D51520D92DF91DDC50A93 (void);
// 0x00000240 System.Void Manager.UISceneGameplayManager::SetTimeText(System.Int32)
extern void UISceneGameplayManager_SetTimeText_mD5EF64A13B41119541B94CB5DA690F256E323844 (void);
// 0x00000241 System.Void Manager.UISceneGameplayManager::SetScoreText(System.Int32)
extern void UISceneGameplayManager_SetScoreText_m1366CCE87678404C47A3EB403DC985F3DD994A4E (void);
// 0x00000242 System.Void Manager.UISceneGameplayManager::SetCountBulletText(System.Int32)
extern void UISceneGameplayManager_SetCountBulletText_mACD756A45D7537DDFF7F4F55D9C41AC1B6D482FA (void);
// 0x00000243 System.Void Manager.UISceneGameplayManager::ShowShelfZero()
extern void UISceneGameplayManager_ShowShelfZero_mB4B13060C15515ED626E29F9EFB1651B7934C723 (void);
// 0x00000244 System.Void Manager.UISceneGameplayManager::ShowShelfOne()
extern void UISceneGameplayManager_ShowShelfOne_m3DF518BBA1E2DA4BF8C999A57C1B63C14769C286 (void);
// 0x00000245 System.Void Manager.UISceneGameplayManager::ShowShelfTwo()
extern void UISceneGameplayManager_ShowShelfTwo_mAB82ADA4F3BCCD52F3452AEA727FE2B8437C5BF3 (void);
// 0x00000246 System.Void Manager.UISceneGameplayManager::ShowShelfThree()
extern void UISceneGameplayManager_ShowShelfThree_m15CAD734C7D07428F87BC35CC919C4578D4912F7 (void);
// 0x00000247 System.Void Manager.UISceneGameplayManager::ShowShelfFour()
extern void UISceneGameplayManager_ShowShelfFour_m01A5C18CD4061313DA250E583BB51DC46B4CAA3C (void);
// 0x00000248 System.Void Manager.UISceneGameplayManager::ShowShelfFive()
extern void UISceneGameplayManager_ShowShelfFive_m52268C0639EFAE04C1167AB392B1AE245C3267AB (void);
// 0x00000249 System.Void Manager.UISceneGameplayManager::ShowShelfSix()
extern void UISceneGameplayManager_ShowShelfSix_m35E984986BC0530E82BE5C2617CD6F5CA4D0B0A6 (void);
// 0x0000024A System.Void Manager.UISceneGameplayManager::ShowShelfSeven()
extern void UISceneGameplayManager_ShowShelfSeven_m65767C0894E6CB66DF8F1FB8C0BAC8EA25AEE72E (void);
// 0x0000024B System.Void Manager.UISceneGameplayManager::ShowShelfEight()
extern void UISceneGameplayManager_ShowShelfEight_m148BB7456D3EBBED18387ECB455CC528E68AAFFF (void);
// 0x0000024C System.Void Manager.UISceneGameplayManager::ShowShelfNine()
extern void UISceneGameplayManager_ShowShelfNine_m055AC838B88ED5C7814B459665FC68D510742454 (void);
// 0x0000024D System.Void Manager.UISceneGameplayManager::ShowShelfTen()
extern void UISceneGameplayManager_ShowShelfTen_m823DE8C2DEE44C0341F49089744DAC671F34EF7E (void);
// 0x0000024E System.Void Manager.UISceneGameplayManager::ShowShelfEleven()
extern void UISceneGameplayManager_ShowShelfEleven_m648290A30763B084D6BC4F26A41437BEEDD17EAF (void);
// 0x0000024F System.Void Manager.UISceneGameplayManager::ShowShelfTwelve()
extern void UISceneGameplayManager_ShowShelfTwelve_m04EDDD419A63FE9C807AA893270F4FAE6C4532EB (void);
// 0x00000250 System.Void Manager.UISceneGameplayManager::ShowShelfThirteen()
extern void UISceneGameplayManager_ShowShelfThirteen_m9AB41B40BB4860BCBFA2D4A3A9D3B7C7F8EF772B (void);
// 0x00000251 System.Void Manager.UISceneGameplayManager::ShowShelfFourteen()
extern void UISceneGameplayManager_ShowShelfFourteen_m87610210A8BA39D2E665D96A19E4756466876941 (void);
// 0x00000252 System.Void Manager.UISceneGameplayManager::ShowShelfFifteen()
extern void UISceneGameplayManager_ShowShelfFifteen_mB06A49394038CAB11FE23FD084BE6E69E56DE5FA (void);
// 0x00000253 System.Void Manager.UISceneGameplayManager::ShowShelfSixteen()
extern void UISceneGameplayManager_ShowShelfSixteen_m832B62B6FB087BD4298523FA727F42D7FEC98758 (void);
// 0x00000254 System.Void Manager.UISceneGameplayManager::ShowShelfPanel()
extern void UISceneGameplayManager_ShowShelfPanel_m73EC5F6F86314DF1C1E59602010558C0D5C96DD4 (void);
// 0x00000255 System.Void Manager.UISceneGameplayManager::CloseShelfPanel()
extern void UISceneGameplayManager_CloseShelfPanel_m35AC101B318800D1012FA2A4916247E0AE60C91D (void);
// 0x00000256 System.Void Manager.UISceneGameplayManager::CloseOpenUI()
extern void UISceneGameplayManager_CloseOpenUI_m44F962B5CB3E8555D9A33187B86CBA846FC2DCB5 (void);
// 0x00000257 System.Void Manager.UISceneGameplayManager::ShowOpenUI()
extern void UISceneGameplayManager_ShowOpenUI_mFA2F75EB604003B66C437088E734828D375EC9DA (void);
// 0x00000258 System.Void Manager.UISceneGameplayManager::CloseShelfUI()
extern void UISceneGameplayManager_CloseShelfUI_m611E31A59DAADC8FB3BEA6585D18D499EFA7C4DE (void);
// 0x00000259 System.Void Manager.UISceneGameplayManager::ShowUI(UnityEngine.RectTransform)
extern void UISceneGameplayManager_ShowUI_m99F453028414EB91FB7FB84ED902E73155D7868D (void);
// 0x0000025A System.Void Manager.UISceneGameplayManager::HideUI(UnityEngine.RectTransform)
extern void UISceneGameplayManager_HideUI_m3EE1D5E43CC7D4507A1571FE8BF9AFB4E3B1D630 (void);
// 0x0000025B System.Void Manager.UISceneGameplayManager::ShowButton(UnityEngine.UI.Button)
extern void UISceneGameplayManager_ShowButton_m17FF4242F06641FBA15470DA6D388E44997EF578 (void);
// 0x0000025C System.Void Manager.UISceneGameplayManager::HideButton(UnityEngine.UI.Button)
extern void UISceneGameplayManager_HideButton_m323DF5AD8C78D66761AAF6DCE70E99FDCCA0C6D9 (void);
// 0x0000025D System.Void Manager.UISceneGameplayManager::.ctor()
extern void UISceneGameplayManager__ctor_mF73463E65694FA081C2283F70AA7463C4688C443 (void);
// 0x0000025E System.Void DefaultNamespace.IOpenShelfEight::OpenShelfEight()
// 0x0000025F System.Void DefaultNamespace.IOpenShelfEleven::OpenShelfEleven()
// 0x00000260 System.Void DefaultNamespace.IOpenShelfFifteen::OpenShelfFifteen()
// 0x00000261 System.Void DefaultNamespace.IOpenShelfFive::OpenShelfFive()
// 0x00000262 System.Void DefaultNamespace.IOpenShelfFour::OpenShelfFour()
// 0x00000263 System.Void DefaultNamespace.IOpenShelfFourteen::OpenShelfFourteen()
// 0x00000264 System.Void DefaultNamespace.IOpenShelfNine::OpenShelfNine()
// 0x00000265 System.Void DefaultNamespace.IOpenShelfOne::OpenShelfOne()
// 0x00000266 System.Void DefaultNamespace.IOpenShelfSeven::OpenShelfSeven()
// 0x00000267 System.Void DefaultNamespace.IOpenShelfSix::OpenShelfSix()
// 0x00000268 System.Void DefaultNamespace.IOpenShelfSixteen::OpenShelfSixteen()
// 0x00000269 System.Void DefaultNamespace.IOpenShelfTen::OpenShelfTen()
// 0x0000026A System.Void DefaultNamespace.IOpenShelfThirteen::OpenShelfThirteen()
// 0x0000026B System.Void DefaultNamespace.IOpenShelfThree::OpenShelfThree()
// 0x0000026C System.Void DefaultNamespace.IOpenShelfTwelve::OpenShelfTwelve()
// 0x0000026D System.Void DefaultNamespace.IOpenShelfTwo::OpenShelfTwo()
// 0x0000026E System.Void DefaultNamespace.IOpenShelfZero::OpenShelfZero()
// 0x0000026F System.Void DefaultNamespace.ICollision::TakeCollision()
// 0x00000270 System.Void DefaultNamespace.ICollision::NotTakeCollision()
// 0x00000271 System.Int32 Character.BaseCharacter::get_Hp()
extern void BaseCharacter_get_Hp_m84512611F7685F0534B1446CFB16236CB55BECF1 (void);
// 0x00000272 System.Void Character.BaseCharacter::set_Hp(System.Int32)
extern void BaseCharacter_set_Hp_mB638FB310531F8AC2FF4BC65E145964985C2EC3C (void);
// 0x00000273 System.Single Character.BaseCharacter::get_Speed()
extern void BaseCharacter_get_Speed_m0F6167443D284CE95AFA64907A6ACE9B886C0B2F (void);
// 0x00000274 System.Void Character.BaseCharacter::set_Speed(System.Single)
extern void BaseCharacter_set_Speed_m6AD36AA734E58013D4842F9F14C3DBA5811DC049 (void);
// 0x00000275 Bullet Character.BaseCharacter::get_Bullet()
extern void BaseCharacter_get_Bullet_m4DB1BD77232B3C1DF7F2FC4F29472F68ED015BFA (void);
// 0x00000276 System.Void Character.BaseCharacter::set_Bullet(Bullet)
extern void BaseCharacter_set_Bullet_mB106B7A92BA4CAD0423A2A9601A378256301ADAC (void);
// 0x00000277 System.Void Character.BaseCharacter::Init(System.Int32,System.Single,Bullet,UnityEngine.Transform)
extern void BaseCharacter_Init_mBFEA03793CA90C41299282F9BA02A725FACF83E2 (void);
// 0x00000278 System.Void Character.BaseCharacter::Attack()
// 0x00000279 System.Void Character.BaseCharacter::Die()
// 0x0000027A System.Void Character.BaseCharacter::Open()
// 0x0000027B System.Void Character.BaseCharacter::.ctor()
extern void BaseCharacter__ctor_m578E57F40104D0E260E0D02460B0949B238109E7 (void);
// 0x0000027C System.Void Character.IDamageable::TakeHit(System.Int32)
// 0x0000027D System.Int32 RSG.IPromise`1::get_Id()
// 0x0000027E RSG.IPromise`1<PromisedT> RSG.IPromise`1::WithName(System.String)
// 0x0000027F System.Void RSG.IPromise`1::Done(System.Action`1<PromisedT>,System.Action`1<System.Exception>)
// 0x00000280 System.Void RSG.IPromise`1::Done(System.Action`1<PromisedT>)
// 0x00000281 System.Void RSG.IPromise`1::Done()
// 0x00000282 RSG.IPromise RSG.IPromise`1::Catch(System.Action`1<System.Exception>)
// 0x00000283 RSG.IPromise`1<PromisedT> RSG.IPromise`1::Catch(System.Func`2<System.Exception,PromisedT>)
// 0x00000284 RSG.IPromise`1<ConvertedT> RSG.IPromise`1::Then(System.Func`2<PromisedT,RSG.IPromise`1<ConvertedT>>)
// 0x00000285 RSG.IPromise RSG.IPromise`1::Then(System.Func`2<PromisedT,RSG.IPromise>)
// 0x00000286 RSG.IPromise RSG.IPromise`1::Then(System.Action`1<PromisedT>)
// 0x00000287 RSG.IPromise`1<ConvertedT> RSG.IPromise`1::Then(System.Func`2<PromisedT,RSG.IPromise`1<ConvertedT>>,System.Func`2<System.Exception,RSG.IPromise`1<ConvertedT>>)
// 0x00000288 RSG.IPromise RSG.IPromise`1::Then(System.Func`2<PromisedT,RSG.IPromise>,System.Action`1<System.Exception>)
// 0x00000289 RSG.IPromise RSG.IPromise`1::Then(System.Action`1<PromisedT>,System.Action`1<System.Exception>)
// 0x0000028A RSG.IPromise`1<ConvertedT> RSG.IPromise`1::Then(System.Func`2<PromisedT,RSG.IPromise`1<ConvertedT>>,System.Func`2<System.Exception,RSG.IPromise`1<ConvertedT>>,System.Action`1<System.Single>)
// 0x0000028B RSG.IPromise RSG.IPromise`1::Then(System.Func`2<PromisedT,RSG.IPromise>,System.Action`1<System.Exception>,System.Action`1<System.Single>)
// 0x0000028C RSG.IPromise RSG.IPromise`1::Then(System.Action`1<PromisedT>,System.Action`1<System.Exception>,System.Action`1<System.Single>)
// 0x0000028D RSG.IPromise`1<ConvertedT> RSG.IPromise`1::Then(System.Func`2<PromisedT,ConvertedT>)
// 0x0000028E RSG.IPromise`1<System.Collections.Generic.IEnumerable`1<ConvertedT>> RSG.IPromise`1::ThenAll(System.Func`2<PromisedT,System.Collections.Generic.IEnumerable`1<RSG.IPromise`1<ConvertedT>>>)
// 0x0000028F RSG.IPromise RSG.IPromise`1::ThenAll(System.Func`2<PromisedT,System.Collections.Generic.IEnumerable`1<RSG.IPromise>>)
// 0x00000290 RSG.IPromise`1<ConvertedT> RSG.IPromise`1::ThenRace(System.Func`2<PromisedT,System.Collections.Generic.IEnumerable`1<RSG.IPromise`1<ConvertedT>>>)
// 0x00000291 RSG.IPromise RSG.IPromise`1::ThenRace(System.Func`2<PromisedT,System.Collections.Generic.IEnumerable`1<RSG.IPromise>>)
// 0x00000292 RSG.IPromise`1<PromisedT> RSG.IPromise`1::Finally(System.Action)
// 0x00000293 RSG.IPromise RSG.IPromise`1::ContinueWith(System.Func`1<RSG.IPromise>)
// 0x00000294 RSG.IPromise`1<ConvertedT> RSG.IPromise`1::ContinueWith(System.Func`1<RSG.IPromise`1<ConvertedT>>)
// 0x00000295 RSG.IPromise`1<PromisedT> RSG.IPromise`1::Progress(System.Action`1<System.Single>)
// 0x00000296 System.Void RSG.IRejectable::Reject(System.Exception)
// 0x00000297 System.Int32 RSG.IPendingPromise`1::get_Id()
// 0x00000298 System.Void RSG.IPendingPromise`1::Resolve(PromisedT)
// 0x00000299 System.Void RSG.IPendingPromise`1::ReportProgress(System.Single)
// 0x0000029A System.Int32 RSG.Promise`1::get_Id()
// 0x0000029B System.String RSG.Promise`1::get_Name()
// 0x0000029C System.Void RSG.Promise`1::set_Name(System.String)
// 0x0000029D RSG.PromiseState RSG.Promise`1::get_CurState()
// 0x0000029E System.Void RSG.Promise`1::set_CurState(RSG.PromiseState)
// 0x0000029F System.Void RSG.Promise`1::.ctor()
// 0x000002A0 System.Void RSG.Promise`1::.ctor(System.Action`2<System.Action`1<PromisedT>,System.Action`1<System.Exception>>)
// 0x000002A1 System.Void RSG.Promise`1::AddRejectHandler(System.Action`1<System.Exception>,RSG.IRejectable)
// 0x000002A2 System.Void RSG.Promise`1::AddResolveHandler(System.Action`1<PromisedT>,RSG.IRejectable)
// 0x000002A3 System.Void RSG.Promise`1::AddProgressHandler(System.Action`1<System.Single>,RSG.IRejectable)
// 0x000002A4 System.Void RSG.Promise`1::InvokeHandler(System.Action`1<T>,RSG.IRejectable,T)
// 0x000002A5 System.Void RSG.Promise`1::ClearHandlers()
// 0x000002A6 System.Void RSG.Promise`1::InvokeRejectHandlers(System.Exception)
// 0x000002A7 System.Void RSG.Promise`1::InvokeResolveHandlers(PromisedT)
// 0x000002A8 System.Void RSG.Promise`1::InvokeProgressHandlers(System.Single)
// 0x000002A9 System.Void RSG.Promise`1::Reject(System.Exception)
// 0x000002AA System.Void RSG.Promise`1::Resolve(PromisedT)
// 0x000002AB System.Void RSG.Promise`1::ReportProgress(System.Single)
// 0x000002AC System.Void RSG.Promise`1::Done(System.Action`1<PromisedT>,System.Action`1<System.Exception>)
// 0x000002AD System.Void RSG.Promise`1::Done(System.Action`1<PromisedT>)
// 0x000002AE System.Void RSG.Promise`1::Done()
// 0x000002AF RSG.IPromise`1<PromisedT> RSG.Promise`1::WithName(System.String)
// 0x000002B0 RSG.IPromise RSG.Promise`1::Catch(System.Action`1<System.Exception>)
// 0x000002B1 RSG.IPromise`1<PromisedT> RSG.Promise`1::Catch(System.Func`2<System.Exception,PromisedT>)
// 0x000002B2 RSG.IPromise`1<ConvertedT> RSG.Promise`1::Then(System.Func`2<PromisedT,RSG.IPromise`1<ConvertedT>>)
// 0x000002B3 RSG.IPromise RSG.Promise`1::Then(System.Func`2<PromisedT,RSG.IPromise>)
// 0x000002B4 RSG.IPromise RSG.Promise`1::Then(System.Action`1<PromisedT>)
// 0x000002B5 RSG.IPromise`1<ConvertedT> RSG.Promise`1::Then(System.Func`2<PromisedT,RSG.IPromise`1<ConvertedT>>,System.Func`2<System.Exception,RSG.IPromise`1<ConvertedT>>)
// 0x000002B6 RSG.IPromise RSG.Promise`1::Then(System.Func`2<PromisedT,RSG.IPromise>,System.Action`1<System.Exception>)
// 0x000002B7 RSG.IPromise RSG.Promise`1::Then(System.Action`1<PromisedT>,System.Action`1<System.Exception>)
// 0x000002B8 RSG.IPromise`1<ConvertedT> RSG.Promise`1::Then(System.Func`2<PromisedT,RSG.IPromise`1<ConvertedT>>,System.Func`2<System.Exception,RSG.IPromise`1<ConvertedT>>,System.Action`1<System.Single>)
// 0x000002B9 RSG.IPromise RSG.Promise`1::Then(System.Func`2<PromisedT,RSG.IPromise>,System.Action`1<System.Exception>,System.Action`1<System.Single>)
// 0x000002BA RSG.IPromise RSG.Promise`1::Then(System.Action`1<PromisedT>,System.Action`1<System.Exception>,System.Action`1<System.Single>)
// 0x000002BB RSG.IPromise`1<ConvertedT> RSG.Promise`1::Then(System.Func`2<PromisedT,ConvertedT>)
// 0x000002BC System.Void RSG.Promise`1::ActionHandlers(RSG.IRejectable,System.Action`1<PromisedT>,System.Action`1<System.Exception>)
// 0x000002BD System.Void RSG.Promise`1::ProgressHandlers(RSG.IRejectable,System.Action`1<System.Single>)
// 0x000002BE RSG.IPromise`1<System.Collections.Generic.IEnumerable`1<ConvertedT>> RSG.Promise`1::ThenAll(System.Func`2<PromisedT,System.Collections.Generic.IEnumerable`1<RSG.IPromise`1<ConvertedT>>>)
// 0x000002BF RSG.IPromise RSG.Promise`1::ThenAll(System.Func`2<PromisedT,System.Collections.Generic.IEnumerable`1<RSG.IPromise>>)
// 0x000002C0 RSG.IPromise`1<System.Collections.Generic.IEnumerable`1<PromisedT>> RSG.Promise`1::All(RSG.IPromise`1<PromisedT>[])
// 0x000002C1 RSG.IPromise`1<System.Collections.Generic.IEnumerable`1<PromisedT>> RSG.Promise`1::All(System.Collections.Generic.IEnumerable`1<RSG.IPromise`1<PromisedT>>)
// 0x000002C2 RSG.IPromise`1<ConvertedT> RSG.Promise`1::ThenRace(System.Func`2<PromisedT,System.Collections.Generic.IEnumerable`1<RSG.IPromise`1<ConvertedT>>>)
// 0x000002C3 RSG.IPromise RSG.Promise`1::ThenRace(System.Func`2<PromisedT,System.Collections.Generic.IEnumerable`1<RSG.IPromise>>)
// 0x000002C4 RSG.IPromise`1<PromisedT> RSG.Promise`1::Race(RSG.IPromise`1<PromisedT>[])
// 0x000002C5 RSG.IPromise`1<PromisedT> RSG.Promise`1::Race(System.Collections.Generic.IEnumerable`1<RSG.IPromise`1<PromisedT>>)
// 0x000002C6 RSG.IPromise`1<PromisedT> RSG.Promise`1::Resolved(PromisedT)
// 0x000002C7 RSG.IPromise`1<PromisedT> RSG.Promise`1::Rejected(System.Exception)
// 0x000002C8 RSG.IPromise`1<PromisedT> RSG.Promise`1::Finally(System.Action)
// 0x000002C9 RSG.IPromise RSG.Promise`1::ContinueWith(System.Func`1<RSG.IPromise>)
// 0x000002CA RSG.IPromise`1<ConvertedT> RSG.Promise`1::ContinueWith(System.Func`1<RSG.IPromise`1<ConvertedT>>)
// 0x000002CB RSG.IPromise`1<PromisedT> RSG.Promise`1::Progress(System.Action`1<System.Single>)
// 0x000002CC System.Void RSG.Promise`1::<Done>b__30_0(System.Exception)
// 0x000002CD System.Void RSG.Promise`1::<Done>b__31_0(System.Exception)
// 0x000002CE System.Void RSG.Promise`1::<Done>b__32_0(System.Exception)
// 0x000002CF System.Void RSG.Promise`1/<>c__DisplayClass24_0::.ctor()
// 0x000002D0 System.Void RSG.Promise`1/<>c__DisplayClass24_0::<InvokeRejectHandlers>b__0(RSG.RejectHandler)
// 0x000002D1 System.Void RSG.Promise`1/<>c__DisplayClass26_0::.ctor()
// 0x000002D2 System.Void RSG.Promise`1/<>c__DisplayClass26_0::<InvokeProgressHandlers>b__0(RSG.ProgressHandler)
// 0x000002D3 System.Void RSG.Promise`1/<>c__DisplayClass34_0::.ctor()
// 0x000002D4 System.Void RSG.Promise`1/<>c__DisplayClass34_0::<Catch>b__0(PromisedT)
// 0x000002D5 System.Void RSG.Promise`1/<>c__DisplayClass34_0::<Catch>b__1(System.Exception)
// 0x000002D6 System.Void RSG.Promise`1/<>c__DisplayClass34_0::<Catch>b__2(System.Single)
// 0x000002D7 System.Void RSG.Promise`1/<>c__DisplayClass35_0::.ctor()
// 0x000002D8 System.Void RSG.Promise`1/<>c__DisplayClass35_0::<Catch>b__0(PromisedT)
// 0x000002D9 System.Void RSG.Promise`1/<>c__DisplayClass35_0::<Catch>b__1(System.Exception)
// 0x000002DA System.Void RSG.Promise`1/<>c__DisplayClass35_0::<Catch>b__2(System.Single)
// 0x000002DB System.Void RSG.Promise`1/<>c__DisplayClass42_0`1::.ctor()
// 0x000002DC System.Void RSG.Promise`1/<>c__DisplayClass42_0`1::<Then>b__0(PromisedT)
// 0x000002DD System.Void RSG.Promise`1/<>c__DisplayClass42_0`1::<Then>b__2(System.Single)
// 0x000002DE System.Void RSG.Promise`1/<>c__DisplayClass42_0`1::<Then>b__3(ConvertedT)
// 0x000002DF System.Void RSG.Promise`1/<>c__DisplayClass42_0`1::<Then>b__4(System.Exception)
// 0x000002E0 System.Void RSG.Promise`1/<>c__DisplayClass42_0`1::<Then>b__1(System.Exception)
// 0x000002E1 System.Void RSG.Promise`1/<>c__DisplayClass42_0`1::<Then>b__5(ConvertedT)
// 0x000002E2 System.Void RSG.Promise`1/<>c__DisplayClass42_0`1::<Then>b__6(System.Exception)
// 0x000002E3 System.Void RSG.Promise`1/<>c__DisplayClass43_0::.ctor()
// 0x000002E4 System.Void RSG.Promise`1/<>c__DisplayClass43_0::<Then>b__0(PromisedT)
// 0x000002E5 System.Void RSG.Promise`1/<>c__DisplayClass43_0::<Then>b__2(System.Single)
// 0x000002E6 System.Void RSG.Promise`1/<>c__DisplayClass43_0::<Then>b__3()
// 0x000002E7 System.Void RSG.Promise`1/<>c__DisplayClass43_0::<Then>b__4(System.Exception)
// 0x000002E8 System.Void RSG.Promise`1/<>c__DisplayClass43_0::<Then>b__1(System.Exception)
// 0x000002E9 System.Void RSG.Promise`1/<>c__DisplayClass44_0::.ctor()
// 0x000002EA System.Void RSG.Promise`1/<>c__DisplayClass44_0::<Then>b__0(PromisedT)
// 0x000002EB System.Void RSG.Promise`1/<>c__DisplayClass44_0::<Then>b__1(System.Exception)
// 0x000002EC System.Void RSG.Promise`1/<>c__DisplayClass45_0`1::.ctor()
// 0x000002ED RSG.IPromise`1<ConvertedT> RSG.Promise`1/<>c__DisplayClass45_0`1::<Then>b__0(PromisedT)
// 0x000002EE System.Void RSG.Promise`1/<>c__DisplayClass48_0`1::.ctor()
// 0x000002EF RSG.IPromise`1<System.Collections.Generic.IEnumerable`1<ConvertedT>> RSG.Promise`1/<>c__DisplayClass48_0`1::<ThenAll>b__0(PromisedT)
// 0x000002F0 System.Void RSG.Promise`1/<>c__DisplayClass49_0::.ctor()
// 0x000002F1 RSG.IPromise RSG.Promise`1/<>c__DisplayClass49_0::<ThenAll>b__0(PromisedT)
// 0x000002F2 System.Void RSG.Promise`1/<>c__DisplayClass51_0::.ctor()
// 0x000002F3 System.Void RSG.Promise`1/<>c__DisplayClass51_0::<All>b__0(RSG.IPromise`1<PromisedT>,System.Int32)
// 0x000002F4 System.Void RSG.Promise`1/<>c__DisplayClass51_0::<All>b__3(System.Exception)
// 0x000002F5 System.Void RSG.Promise`1/<>c__DisplayClass51_1::.ctor()
// 0x000002F6 System.Void RSG.Promise`1/<>c__DisplayClass51_1::<All>b__1(System.Single)
// 0x000002F7 System.Void RSG.Promise`1/<>c__DisplayClass51_1::<All>b__2(PromisedT)
// 0x000002F8 System.Void RSG.Promise`1/<>c__DisplayClass52_0`1::.ctor()
// 0x000002F9 RSG.IPromise`1<ConvertedT> RSG.Promise`1/<>c__DisplayClass52_0`1::<ThenRace>b__0(PromisedT)
// 0x000002FA System.Void RSG.Promise`1/<>c__DisplayClass53_0::.ctor()
// 0x000002FB RSG.IPromise RSG.Promise`1/<>c__DisplayClass53_0::<ThenRace>b__0(PromisedT)
// 0x000002FC System.Void RSG.Promise`1/<>c__DisplayClass55_0::.ctor()
// 0x000002FD System.Void RSG.Promise`1/<>c__DisplayClass55_0::<Race>b__0(RSG.IPromise`1<PromisedT>,System.Int32)
// 0x000002FE System.Void RSG.Promise`1/<>c__DisplayClass55_0::<Race>b__2(PromisedT)
// 0x000002FF System.Void RSG.Promise`1/<>c__DisplayClass55_0::<Race>b__3(System.Exception)
// 0x00000300 System.Void RSG.Promise`1/<>c__DisplayClass55_1::.ctor()
// 0x00000301 System.Void RSG.Promise`1/<>c__DisplayClass55_1::<Race>b__1(System.Single)
// 0x00000302 System.Void RSG.Promise`1/<>c__DisplayClass58_0::.ctor()
// 0x00000303 System.Void RSG.Promise`1/<>c__DisplayClass58_0::<Finally>b__0(PromisedT)
// 0x00000304 System.Void RSG.Promise`1/<>c__DisplayClass58_0::<Finally>b__1(System.Exception)
// 0x00000305 PromisedT RSG.Promise`1/<>c__DisplayClass58_0::<Finally>b__2(PromisedT)
// 0x00000306 System.Void RSG.Promise`1/<>c__DisplayClass59_0::.ctor()
// 0x00000307 System.Void RSG.Promise`1/<>c__DisplayClass59_0::<ContinueWith>b__0(PromisedT)
// 0x00000308 System.Void RSG.Promise`1/<>c__DisplayClass59_0::<ContinueWith>b__1(System.Exception)
// 0x00000309 System.Void RSG.Promise`1/<>c__DisplayClass60_0`1::.ctor()
// 0x0000030A System.Void RSG.Promise`1/<>c__DisplayClass60_0`1::<ContinueWith>b__0(PromisedT)
// 0x0000030B System.Void RSG.Promise`1/<>c__DisplayClass60_0`1::<ContinueWith>b__1(System.Exception)
// 0x0000030C RSG.IPromise`1<RSG.Tuple`2<T1,T2>> RSG.PromiseHelpers::All(RSG.IPromise`1<T1>,RSG.IPromise`1<T2>)
// 0x0000030D RSG.IPromise`1<RSG.Tuple`3<T1,T2,T3>> RSG.PromiseHelpers::All(RSG.IPromise`1<T1>,RSG.IPromise`1<T2>,RSG.IPromise`1<T3>)
// 0x0000030E RSG.IPromise`1<RSG.Tuple`4<T1,T2,T3,T4>> RSG.PromiseHelpers::All(RSG.IPromise`1<T1>,RSG.IPromise`1<T2>,RSG.IPromise`1<T3>,RSG.IPromise`1<T4>)
// 0x0000030F System.Void RSG.PromiseHelpers/<>c__DisplayClass0_0`2::.ctor()
// 0x00000310 System.Void RSG.PromiseHelpers/<>c__DisplayClass0_0`2::<All>b__0(T1)
// 0x00000311 System.Void RSG.PromiseHelpers/<>c__DisplayClass0_0`2::<All>b__1(System.Exception)
// 0x00000312 System.Void RSG.PromiseHelpers/<>c__DisplayClass0_0`2::<All>b__2(T2)
// 0x00000313 System.Void RSG.PromiseHelpers/<>c__DisplayClass0_0`2::<All>b__3(System.Exception)
// 0x00000314 System.Void RSG.PromiseHelpers/<>c__1`3::.cctor()
// 0x00000315 System.Void RSG.PromiseHelpers/<>c__1`3::.ctor()
// 0x00000316 RSG.Tuple`3<T1,T2,T3> RSG.PromiseHelpers/<>c__1`3::<All>b__1_0(RSG.Tuple`2<RSG.Tuple`2<T1,T2>,T3>)
// 0x00000317 System.Void RSG.PromiseHelpers/<>c__2`4::.cctor()
// 0x00000318 System.Void RSG.PromiseHelpers/<>c__2`4::.ctor()
// 0x00000319 RSG.Tuple`4<T1,T2,T3,T4> RSG.PromiseHelpers/<>c__2`4::<All>b__2_0(RSG.Tuple`2<RSG.Tuple`2<T1,T2>,RSG.Tuple`2<T3,T4>>)
// 0x0000031A System.Void RSG.PromiseCancelledException::.ctor()
extern void PromiseCancelledException__ctor_mDA55F2F9C6DF87917C0A40BC149A437C750C66FD (void);
// 0x0000031B System.Void RSG.PromiseCancelledException::.ctor(System.String)
extern void PromiseCancelledException__ctor_m0733E2CEAEE71A6A1A22E96FFF62D85627437BF1 (void);
// 0x0000031C System.Void RSG.PredicateWait::.ctor()
extern void PredicateWait__ctor_mCD6942E554ED82AF36B3F07828C4E5BCA7F0D318 (void);
// 0x0000031D RSG.IPromise RSG.IPromiseTimer::WaitFor(System.Single)
// 0x0000031E RSG.IPromise RSG.IPromiseTimer::WaitUntil(System.Func`2<RSG.TimeData,System.Boolean>)
// 0x0000031F RSG.IPromise RSG.IPromiseTimer::WaitWhile(System.Func`2<RSG.TimeData,System.Boolean>)
// 0x00000320 System.Void RSG.IPromiseTimer::Update(System.Single)
// 0x00000321 System.Boolean RSG.IPromiseTimer::Cancel(RSG.IPromise)
// 0x00000322 RSG.IPromise RSG.PromiseTimer::WaitFor(System.Single)
extern void PromiseTimer_WaitFor_m99B243125D5686DC36844FA69D796651E12C9B76 (void);
// 0x00000323 RSG.IPromise RSG.PromiseTimer::WaitWhile(System.Func`2<RSG.TimeData,System.Boolean>)
extern void PromiseTimer_WaitWhile_m8EF7C405289D3A2AE7F7185AE8E4E901BF504EC9 (void);
// 0x00000324 RSG.IPromise RSG.PromiseTimer::WaitUntil(System.Func`2<RSG.TimeData,System.Boolean>)
extern void PromiseTimer_WaitUntil_m7AC7AE2AFC6CB5E43A789DE5A6E683D62D23FBFC (void);
// 0x00000325 System.Boolean RSG.PromiseTimer::Cancel(RSG.IPromise)
extern void PromiseTimer_Cancel_m87F8524626DDC28FB1ECA1968F73A2D18540704E (void);
// 0x00000326 System.Collections.Generic.LinkedListNode`1<RSG.PredicateWait> RSG.PromiseTimer::FindInWaiting(RSG.IPromise)
extern void PromiseTimer_FindInWaiting_mA1660E092E5065B151FB000488B2C87FF4C6C8F1 (void);
// 0x00000327 System.Void RSG.PromiseTimer::Update(System.Single)
extern void PromiseTimer_Update_m9C84E7E7B30B14A7C13510CC22AB6032C7C0874F (void);
// 0x00000328 System.Collections.Generic.LinkedListNode`1<RSG.PredicateWait> RSG.PromiseTimer::RemoveNode(System.Collections.Generic.LinkedListNode`1<RSG.PredicateWait>)
extern void PromiseTimer_RemoveNode_mF32CC3AE04AE1AFBCEF4B8DA7DFD9E2AF02A6E89 (void);
// 0x00000329 System.Void RSG.PromiseTimer::.ctor()
extern void PromiseTimer__ctor_m3F1298B21CD5644C3AE646911AD13B6FCCD19BE6 (void);
// 0x0000032A System.Void RSG.PromiseTimer/<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_mC51BCEA4436030F27897F20585D501ABF744C78E (void);
// 0x0000032B System.Boolean RSG.PromiseTimer/<>c__DisplayClass3_0::<WaitFor>b__0(RSG.TimeData)
extern void U3CU3Ec__DisplayClass3_0_U3CWaitForU3Eb__0_m7EEE8458FA739B7F87A8EF705E05039C9EFB483D (void);
// 0x0000032C System.Void RSG.PromiseTimer/<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_m8884F549B84F64B7E7D4B61E036A398416091313 (void);
// 0x0000032D System.Boolean RSG.PromiseTimer/<>c__DisplayClass4_0::<WaitWhile>b__0(RSG.TimeData)
extern void U3CU3Ec__DisplayClass4_0_U3CWaitWhileU3Eb__0_m8DF4AC7205D6CD8FF4E8AE640F92F6AB4A9B59F7 (void);
// 0x0000032E System.Int32 RSG.IPromise::get_Id()
// 0x0000032F RSG.IPromise RSG.IPromise::WithName(System.String)
// 0x00000330 System.Void RSG.IPromise::Done(System.Action,System.Action`1<System.Exception>)
// 0x00000331 System.Void RSG.IPromise::Done(System.Action)
// 0x00000332 System.Void RSG.IPromise::Done()
// 0x00000333 RSG.IPromise RSG.IPromise::Catch(System.Action`1<System.Exception>)
// 0x00000334 RSG.IPromise`1<ConvertedT> RSG.IPromise::Then(System.Func`1<RSG.IPromise`1<ConvertedT>>)
// 0x00000335 RSG.IPromise RSG.IPromise::Then(System.Func`1<RSG.IPromise>)
// 0x00000336 RSG.IPromise RSG.IPromise::Then(System.Action)
// 0x00000337 RSG.IPromise`1<ConvertedT> RSG.IPromise::Then(System.Func`1<RSG.IPromise`1<ConvertedT>>,System.Func`2<System.Exception,RSG.IPromise`1<ConvertedT>>)
// 0x00000338 RSG.IPromise RSG.IPromise::Then(System.Func`1<RSG.IPromise>,System.Action`1<System.Exception>)
// 0x00000339 RSG.IPromise RSG.IPromise::Then(System.Action,System.Action`1<System.Exception>)
// 0x0000033A RSG.IPromise`1<ConvertedT> RSG.IPromise::Then(System.Func`1<RSG.IPromise`1<ConvertedT>>,System.Func`2<System.Exception,RSG.IPromise`1<ConvertedT>>,System.Action`1<System.Single>)
// 0x0000033B RSG.IPromise RSG.IPromise::Then(System.Func`1<RSG.IPromise>,System.Action`1<System.Exception>,System.Action`1<System.Single>)
// 0x0000033C RSG.IPromise RSG.IPromise::Then(System.Action,System.Action`1<System.Exception>,System.Action`1<System.Single>)
// 0x0000033D RSG.IPromise RSG.IPromise::ThenAll(System.Func`1<System.Collections.Generic.IEnumerable`1<RSG.IPromise>>)
// 0x0000033E RSG.IPromise`1<System.Collections.Generic.IEnumerable`1<ConvertedT>> RSG.IPromise::ThenAll(System.Func`1<System.Collections.Generic.IEnumerable`1<RSG.IPromise`1<ConvertedT>>>)
// 0x0000033F RSG.IPromise RSG.IPromise::ThenSequence(System.Func`1<System.Collections.Generic.IEnumerable`1<System.Func`1<RSG.IPromise>>>)
// 0x00000340 RSG.IPromise RSG.IPromise::ThenRace(System.Func`1<System.Collections.Generic.IEnumerable`1<RSG.IPromise>>)
// 0x00000341 RSG.IPromise`1<ConvertedT> RSG.IPromise::ThenRace(System.Func`1<System.Collections.Generic.IEnumerable`1<RSG.IPromise`1<ConvertedT>>>)
// 0x00000342 RSG.IPromise RSG.IPromise::Finally(System.Action)
// 0x00000343 RSG.IPromise RSG.IPromise::ContinueWith(System.Func`1<RSG.IPromise>)
// 0x00000344 RSG.IPromise`1<ConvertedT> RSG.IPromise::ContinueWith(System.Func`1<RSG.IPromise`1<ConvertedT>>)
// 0x00000345 RSG.IPromise RSG.IPromise::Progress(System.Action`1<System.Single>)
// 0x00000346 System.Int32 RSG.IPendingPromise::get_Id()
// 0x00000347 System.Void RSG.IPendingPromise::Resolve()
// 0x00000348 System.Void RSG.IPendingPromise::ReportProgress(System.Single)
// 0x00000349 System.Int32 RSG.IPromiseInfo::get_Id()
// 0x0000034A System.String RSG.IPromiseInfo::get_Name()
// 0x0000034B System.Void RSG.ExceptionEventArgs::.ctor(System.Exception)
extern void ExceptionEventArgs__ctor_m7023DA6E99C0B23D96D0AAB0500F16B19345A30C (void);
// 0x0000034C System.Exception RSG.ExceptionEventArgs::get_Exception()
extern void ExceptionEventArgs_get_Exception_m79958EAB2CAA0E6C91E10902A3ABCC242012745A (void);
// 0x0000034D System.Void RSG.ExceptionEventArgs::set_Exception(System.Exception)
extern void ExceptionEventArgs_set_Exception_mBEFA8B8A6990A708691413D5E188FD73D6036A88 (void);
// 0x0000034E System.Void RSG.Promise::add_UnhandledException(System.EventHandler`1<RSG.ExceptionEventArgs>)
extern void Promise_add_UnhandledException_m501ED8710D326733D0A4B095516E79AD93771A17 (void);
// 0x0000034F System.Void RSG.Promise::remove_UnhandledException(System.EventHandler`1<RSG.ExceptionEventArgs>)
extern void Promise_remove_UnhandledException_m0A7255398B93C672C3F4AD9AE8CC1260E6BDC493 (void);
// 0x00000350 System.Collections.Generic.IEnumerable`1<RSG.IPromiseInfo> RSG.Promise::GetPendingPromises()
extern void Promise_GetPendingPromises_mCD78AA3FD69C8809D95E0FBBC6DC24DB648CD1E4 (void);
// 0x00000351 System.Int32 RSG.Promise::get_Id()
extern void Promise_get_Id_m2A159B9C83B813983859B9F8497B92A636D26198 (void);
// 0x00000352 System.String RSG.Promise::get_Name()
extern void Promise_get_Name_m2681487A6A6188C86D6B1FEE6F4E42F0817F0AF1 (void);
// 0x00000353 System.Void RSG.Promise::set_Name(System.String)
extern void Promise_set_Name_m04BD79E80089945F37642A501B2E0C621CFAADFF (void);
// 0x00000354 RSG.PromiseState RSG.Promise::get_CurState()
extern void Promise_get_CurState_mBE60E772D62CB2C8449BF67F3A2106C0E0FA720D (void);
// 0x00000355 System.Void RSG.Promise::set_CurState(RSG.PromiseState)
extern void Promise_set_CurState_mC9014B9835DB4BDFE7B3E1AFC0B61A249DB0B148 (void);
// 0x00000356 System.Void RSG.Promise::.ctor()
extern void Promise__ctor_m72891E8C449A8F0285F2BBEE6596EEE91D1B69D0 (void);
// 0x00000357 System.Void RSG.Promise::.ctor(System.Action`2<System.Action,System.Action`1<System.Exception>>)
extern void Promise__ctor_m0F170C315BB66058A0B8DB59D2FB2E269460DB4A (void);
// 0x00000358 System.Int32 RSG.Promise::NextId()
extern void Promise_NextId_m91D574BF971140F5BCA824858A1F23785E625936 (void);
// 0x00000359 System.Void RSG.Promise::AddRejectHandler(System.Action`1<System.Exception>,RSG.IRejectable)
extern void Promise_AddRejectHandler_m8A5C912F1C8D8FC752EAAFCC3397992F84DE2C83 (void);
// 0x0000035A System.Void RSG.Promise::AddResolveHandler(System.Action,RSG.IRejectable)
extern void Promise_AddResolveHandler_m39C28AC656C57C1865250657901C41569B49B87A (void);
// 0x0000035B System.Void RSG.Promise::AddProgressHandler(System.Action`1<System.Single>,RSG.IRejectable)
extern void Promise_AddProgressHandler_mC3147E564182B45C04B4AD18F55200ADE09494A9 (void);
// 0x0000035C System.Void RSG.Promise::InvokeRejectHandler(System.Action`1<System.Exception>,RSG.IRejectable,System.Exception)
extern void Promise_InvokeRejectHandler_mD002929AA239DA2F2D3CBA6B828BCC190E2FA010 (void);
// 0x0000035D System.Void RSG.Promise::InvokeResolveHandler(System.Action,RSG.IRejectable)
extern void Promise_InvokeResolveHandler_m8253411D16B16952735214A8D4840B69CBC19367 (void);
// 0x0000035E System.Void RSG.Promise::InvokeProgressHandler(System.Action`1<System.Single>,RSG.IRejectable,System.Single)
extern void Promise_InvokeProgressHandler_m2767B570D7BE8E457C295823CFD48F4DF079AD1F (void);
// 0x0000035F System.Void RSG.Promise::ClearHandlers()
extern void Promise_ClearHandlers_m77595F54867BFFB97E19A6354940D3C574E8AF30 (void);
// 0x00000360 System.Void RSG.Promise::InvokeRejectHandlers(System.Exception)
extern void Promise_InvokeRejectHandlers_m78C4A77BB7F1728BCF63373111A992940FD73FBA (void);
// 0x00000361 System.Void RSG.Promise::InvokeResolveHandlers()
extern void Promise_InvokeResolveHandlers_m071F1B5884447CEE1F7D19154AB430522BE6C208 (void);
// 0x00000362 System.Void RSG.Promise::InvokeProgressHandlers(System.Single)
extern void Promise_InvokeProgressHandlers_m33BBE047F8B32BDC15B943203CF4D653C10F5F3D (void);
// 0x00000363 System.Void RSG.Promise::Reject(System.Exception)
extern void Promise_Reject_mA334C811DFA609A9294A15B8C4FE3D06CFE59E4B (void);
// 0x00000364 System.Void RSG.Promise::Resolve()
extern void Promise_Resolve_mB395CBF764AD56F64C953F240F5525C4F24AA176 (void);
// 0x00000365 System.Void RSG.Promise::ReportProgress(System.Single)
extern void Promise_ReportProgress_m7496AD24A305E551C317FAF83A339D50555EC2F0 (void);
// 0x00000366 System.Void RSG.Promise::Done(System.Action,System.Action`1<System.Exception>)
extern void Promise_Done_mBF4D40ECA5734EFE1213BD2AD11124841BCA6C63 (void);
// 0x00000367 System.Void RSG.Promise::Done(System.Action)
extern void Promise_Done_mA20B58D299DFC458330AB673679F52760076ED6D (void);
// 0x00000368 System.Void RSG.Promise::Done()
extern void Promise_Done_mB94F090BD0EDC421C98291DF2437788619416322 (void);
// 0x00000369 RSG.IPromise RSG.Promise::WithName(System.String)
extern void Promise_WithName_m9A8610E5066D79F98133FBDE0C427032A9EF04DB (void);
// 0x0000036A RSG.IPromise RSG.Promise::Catch(System.Action`1<System.Exception>)
extern void Promise_Catch_m34A1A6F483209E344CEE20CAECA131319E25A35F (void);
// 0x0000036B RSG.IPromise`1<ConvertedT> RSG.Promise::Then(System.Func`1<RSG.IPromise`1<ConvertedT>>)
// 0x0000036C RSG.IPromise RSG.Promise::Then(System.Func`1<RSG.IPromise>)
extern void Promise_Then_m660D492D7CB977D2658FDC71CA16C5F1C7C80BFB (void);
// 0x0000036D RSG.IPromise RSG.Promise::Then(System.Action)
extern void Promise_Then_mE35145837C7EBB840D64C24A92FFEC013AA4E52F (void);
// 0x0000036E RSG.IPromise`1<ConvertedT> RSG.Promise::Then(System.Func`1<RSG.IPromise`1<ConvertedT>>,System.Func`2<System.Exception,RSG.IPromise`1<ConvertedT>>)
// 0x0000036F RSG.IPromise RSG.Promise::Then(System.Func`1<RSG.IPromise>,System.Action`1<System.Exception>)
extern void Promise_Then_m9C5987E6126BE35855402986BE5878DDDA5DDD2F (void);
// 0x00000370 RSG.IPromise RSG.Promise::Then(System.Action,System.Action`1<System.Exception>)
extern void Promise_Then_m1F2DA120D93DB9D1801A4410BACE242992BFDE5D (void);
// 0x00000371 RSG.IPromise`1<ConvertedT> RSG.Promise::Then(System.Func`1<RSG.IPromise`1<ConvertedT>>,System.Func`2<System.Exception,RSG.IPromise`1<ConvertedT>>,System.Action`1<System.Single>)
// 0x00000372 RSG.IPromise RSG.Promise::Then(System.Func`1<RSG.IPromise>,System.Action`1<System.Exception>,System.Action`1<System.Single>)
extern void Promise_Then_mE03C61503CE58EA2981EF96852E6F252B6F29736 (void);
// 0x00000373 RSG.IPromise RSG.Promise::Then(System.Action,System.Action`1<System.Exception>,System.Action`1<System.Single>)
extern void Promise_Then_m6693CA7F4467B53237CE92F7F44C36CD44FABE0D (void);
// 0x00000374 System.Void RSG.Promise::ActionHandlers(RSG.IRejectable,System.Action,System.Action`1<System.Exception>)
extern void Promise_ActionHandlers_m226D1CB18140648C8EC32E3E2FA7DF0730478F26 (void);
// 0x00000375 System.Void RSG.Promise::ProgressHandlers(RSG.IRejectable,System.Action`1<System.Single>)
extern void Promise_ProgressHandlers_m21C8B94D38E0FBF1F14B24FCFFFEC214D9D80912 (void);
// 0x00000376 RSG.IPromise RSG.Promise::ThenAll(System.Func`1<System.Collections.Generic.IEnumerable`1<RSG.IPromise>>)
extern void Promise_ThenAll_m444DBB4B6489921A44CDCF98AB8D3F2708372D66 (void);
// 0x00000377 RSG.IPromise`1<System.Collections.Generic.IEnumerable`1<ConvertedT>> RSG.Promise::ThenAll(System.Func`1<System.Collections.Generic.IEnumerable`1<RSG.IPromise`1<ConvertedT>>>)
// 0x00000378 RSG.IPromise RSG.Promise::All(RSG.IPromise[])
extern void Promise_All_m4021CBAF737AB75139BE836A21E02B28B238AD21 (void);
// 0x00000379 RSG.IPromise RSG.Promise::All(System.Collections.Generic.IEnumerable`1<RSG.IPromise>)
extern void Promise_All_mD80850322247A01D346893F8962617690B9D2606 (void);
// 0x0000037A RSG.IPromise RSG.Promise::ThenSequence(System.Func`1<System.Collections.Generic.IEnumerable`1<System.Func`1<RSG.IPromise>>>)
extern void Promise_ThenSequence_mE42D6CD848060EB59C10345B5A14B16724D35F9B (void);
// 0x0000037B RSG.IPromise RSG.Promise::Sequence(System.Func`1<RSG.IPromise>[])
extern void Promise_Sequence_m9F9C078AA9DB0404DAB83831A3EF769379E58906 (void);
// 0x0000037C RSG.IPromise RSG.Promise::Sequence(System.Collections.Generic.IEnumerable`1<System.Func`1<RSG.IPromise>>)
extern void Promise_Sequence_m646E63C1583234FE0B9EC2F642D5B1EB7A830E52 (void);
// 0x0000037D RSG.IPromise RSG.Promise::ThenRace(System.Func`1<System.Collections.Generic.IEnumerable`1<RSG.IPromise>>)
extern void Promise_ThenRace_mF04FB2EDFB9FF1589EF714F409994945E123EE7D (void);
// 0x0000037E RSG.IPromise`1<ConvertedT> RSG.Promise::ThenRace(System.Func`1<System.Collections.Generic.IEnumerable`1<RSG.IPromise`1<ConvertedT>>>)
// 0x0000037F RSG.IPromise RSG.Promise::Race(RSG.IPromise[])
extern void Promise_Race_m7A61FE78FC907244940AE23BE119699E657860BB (void);
// 0x00000380 RSG.IPromise RSG.Promise::Race(System.Collections.Generic.IEnumerable`1<RSG.IPromise>)
extern void Promise_Race_m422B8D03C310B70EC818A1A5F7A8E12C836FD877 (void);
// 0x00000381 RSG.IPromise RSG.Promise::Resolved()
extern void Promise_Resolved_m27D7349EFBB518D89093ADB573BD7D7F836B24DE (void);
// 0x00000382 RSG.IPromise RSG.Promise::Rejected(System.Exception)
extern void Promise_Rejected_mF6CA3689BE449406F16A5D145B1DD4FD13C41890 (void);
// 0x00000383 RSG.IPromise RSG.Promise::Finally(System.Action)
extern void Promise_Finally_m762D2CD8F1F48788A1B94FB46CB338D69083230B (void);
// 0x00000384 RSG.IPromise RSG.Promise::ContinueWith(System.Func`1<RSG.IPromise>)
extern void Promise_ContinueWith_m1A53BD142DC421621617AE8E0BE6DA2053D96D8A (void);
// 0x00000385 RSG.IPromise`1<ConvertedT> RSG.Promise::ContinueWith(System.Func`1<RSG.IPromise`1<ConvertedT>>)
// 0x00000386 RSG.IPromise RSG.Promise::Progress(System.Action`1<System.Single>)
extern void Promise_Progress_m53E64BDA8764C5E88057601FAB54BF75FD0B368A (void);
// 0x00000387 System.Void RSG.Promise::PropagateUnhandledException(System.Object,System.Exception)
extern void Promise_PropagateUnhandledException_m30B4923F2598FA6B4F7CD5964E00760B5D9F10B7 (void);
// 0x00000388 System.Void RSG.Promise::.cctor()
extern void Promise__cctor_mB03C9B64D5DF3D552E96C4FCFA942B128173413D (void);
// 0x00000389 System.Void RSG.Promise::<InvokeResolveHandlers>b__35_0(RSG.Promise/ResolveHandler)
extern void Promise_U3CInvokeResolveHandlersU3Eb__35_0_mE4D9BB0CB11B33C2E7F12BFE44103CC546FD2E2A (void);
// 0x0000038A System.Void RSG.Promise::<Done>b__40_0(System.Exception)
extern void Promise_U3CDoneU3Eb__40_0_m03D038B6097EEEF2B736CCE4A1D9154518BE3E01 (void);
// 0x0000038B System.Void RSG.Promise::<Done>b__41_0(System.Exception)
extern void Promise_U3CDoneU3Eb__41_0_mDE5A60CCBCC7F17ADD74A395C92F553EAAA52817 (void);
// 0x0000038C System.Void RSG.Promise::<Done>b__42_0(System.Exception)
extern void Promise_U3CDoneU3Eb__42_0_m10D5F4FC16A8A05494ED431EA28AA288DB13AE3B (void);
// 0x0000038D System.Void RSG.Promise/<>c__DisplayClass34_0::.ctor()
extern void U3CU3Ec__DisplayClass34_0__ctor_mF81EF54B39A864EB5E463DA04D72B5F8A0CF4DFC (void);
// 0x0000038E System.Void RSG.Promise/<>c__DisplayClass34_0::<InvokeRejectHandlers>b__0(RSG.RejectHandler)
extern void U3CU3Ec__DisplayClass34_0_U3CInvokeRejectHandlersU3Eb__0_mC2CB7940F03A8D527C6C9BA7793BA6C6A07E13FC (void);
// 0x0000038F System.Void RSG.Promise/<>c__DisplayClass36_0::.ctor()
extern void U3CU3Ec__DisplayClass36_0__ctor_m18834E008AB1CCA3258C2388F1158B37AB6AFAC1 (void);
// 0x00000390 System.Void RSG.Promise/<>c__DisplayClass36_0::<InvokeProgressHandlers>b__0(RSG.ProgressHandler)
extern void U3CU3Ec__DisplayClass36_0_U3CInvokeProgressHandlersU3Eb__0_mB466D8A8500579A62E0EF36D3766DB1A59E89F41 (void);
// 0x00000391 System.Void RSG.Promise/<>c__DisplayClass44_0::.ctor()
extern void U3CU3Ec__DisplayClass44_0__ctor_mCB6B49A87238E9E939A3FB884973D5B8085A1805 (void);
// 0x00000392 System.Void RSG.Promise/<>c__DisplayClass44_0::<Catch>b__0()
extern void U3CU3Ec__DisplayClass44_0_U3CCatchU3Eb__0_m59D9BB020E9BAA19BF3A55E1A66B4BBF0115A4F3 (void);
// 0x00000393 System.Void RSG.Promise/<>c__DisplayClass44_0::<Catch>b__1(System.Exception)
extern void U3CU3Ec__DisplayClass44_0_U3CCatchU3Eb__1_mC2F86C6B2CC1463D07838F8B6E71CF95A00D6B1C (void);
// 0x00000394 System.Void RSG.Promise/<>c__DisplayClass44_0::<Catch>b__2(System.Single)
extern void U3CU3Ec__DisplayClass44_0_U3CCatchU3Eb__2_m1C82A7D09BAFC515977C21BBAD3E8EE6E6C8B665 (void);
// 0x00000395 System.Void RSG.Promise/<>c__DisplayClass51_0`1::.ctor()
// 0x00000396 System.Void RSG.Promise/<>c__DisplayClass51_0`1::<Then>b__0()
// 0x00000397 System.Void RSG.Promise/<>c__DisplayClass51_0`1::<Then>b__2(System.Single)
// 0x00000398 System.Void RSG.Promise/<>c__DisplayClass51_0`1::<Then>b__3(ConvertedT)
// 0x00000399 System.Void RSG.Promise/<>c__DisplayClass51_0`1::<Then>b__4(System.Exception)
// 0x0000039A System.Void RSG.Promise/<>c__DisplayClass51_0`1::<Then>b__1(System.Exception)
// 0x0000039B System.Void RSG.Promise/<>c__DisplayClass51_0`1::<Then>b__5(ConvertedT)
// 0x0000039C System.Void RSG.Promise/<>c__DisplayClass51_0`1::<Then>b__6(System.Exception)
// 0x0000039D System.Void RSG.Promise/<>c__DisplayClass52_0::.ctor()
extern void U3CU3Ec__DisplayClass52_0__ctor_mB894C314363743106885723C9A014E58D79394DE (void);
// 0x0000039E System.Void RSG.Promise/<>c__DisplayClass52_0::<Then>b__0()
extern void U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__0_mFAB16B8D05F5B6D1B0820FEAECBB318B28D6F95F (void);
// 0x0000039F System.Void RSG.Promise/<>c__DisplayClass52_0::<Then>b__2(System.Single)
extern void U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__2_m68317E06CF2A6CB4E45914984E5E7B1C8E250B3A (void);
// 0x000003A0 System.Void RSG.Promise/<>c__DisplayClass52_0::<Then>b__3()
extern void U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__3_m1DD830DC1CA6AEAB53B32028949E3349E37146EE (void);
// 0x000003A1 System.Void RSG.Promise/<>c__DisplayClass52_0::<Then>b__4(System.Exception)
extern void U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__4_m6F52D5A374717AC944B68D544EF57498A6CF0C12 (void);
// 0x000003A2 System.Void RSG.Promise/<>c__DisplayClass52_0::<Then>b__1(System.Exception)
extern void U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__1_mE79481476F323A2E8C64CF6853F48919B454B238 (void);
// 0x000003A3 System.Void RSG.Promise/<>c__DisplayClass53_0::.ctor()
extern void U3CU3Ec__DisplayClass53_0__ctor_m8AD8D6610D93664501D710EB55F2D75DE59813DC (void);
// 0x000003A4 System.Void RSG.Promise/<>c__DisplayClass53_0::<Then>b__0()
extern void U3CU3Ec__DisplayClass53_0_U3CThenU3Eb__0_m089AF19F35A75DF89A101BC920FA8F3CBF856F03 (void);
// 0x000003A5 System.Void RSG.Promise/<>c__DisplayClass53_0::<Then>b__1(System.Exception)
extern void U3CU3Ec__DisplayClass53_0_U3CThenU3Eb__1_m6BF2C7A1D0A0D3699E5339E92DFF00CA7B60BBE2 (void);
// 0x000003A6 System.Void RSG.Promise/<>c__DisplayClass56_0::.ctor()
extern void U3CU3Ec__DisplayClass56_0__ctor_mA16A09395360FDB88DAEB794DAF2A6BAF4CAC8AE (void);
// 0x000003A7 RSG.IPromise RSG.Promise/<>c__DisplayClass56_0::<ThenAll>b__0()
extern void U3CU3Ec__DisplayClass56_0_U3CThenAllU3Eb__0_m76809680DE80AD5F5596FA06F41BA163AC6D1C30 (void);
// 0x000003A8 System.Void RSG.Promise/<>c__DisplayClass57_0`1::.ctor()
// 0x000003A9 RSG.IPromise`1<System.Collections.Generic.IEnumerable`1<ConvertedT>> RSG.Promise/<>c__DisplayClass57_0`1::<ThenAll>b__0()
// 0x000003AA System.Void RSG.Promise/<>c__DisplayClass59_0::.ctor()
extern void U3CU3Ec__DisplayClass59_0__ctor_mBA66AC86289688F1C468B96C50BE5358F31A082F (void);
// 0x000003AB System.Void RSG.Promise/<>c__DisplayClass59_0::<All>b__0(RSG.IPromise,System.Int32)
extern void U3CU3Ec__DisplayClass59_0_U3CAllU3Eb__0_mA4CF65F285BDA1A6D803D917E9FD265051D6BEB9 (void);
// 0x000003AC System.Void RSG.Promise/<>c__DisplayClass59_0::<All>b__3(System.Exception)
extern void U3CU3Ec__DisplayClass59_0_U3CAllU3Eb__3_m8253EC136EA78D646B69616B15FA235271A992AA (void);
// 0x000003AD System.Void RSG.Promise/<>c__DisplayClass59_1::.ctor()
extern void U3CU3Ec__DisplayClass59_1__ctor_m526DF9DF8BBEBFA28914FB4F54E3A1AF03B414BB (void);
// 0x000003AE System.Void RSG.Promise/<>c__DisplayClass59_1::<All>b__1(System.Single)
extern void U3CU3Ec__DisplayClass59_1_U3CAllU3Eb__1_m593233FA219F817C7E2E48E4B57F1967B608EDAE (void);
// 0x000003AF System.Void RSG.Promise/<>c__DisplayClass59_1::<All>b__2()
extern void U3CU3Ec__DisplayClass59_1_U3CAllU3Eb__2_m5AC61534D5AE0A52E1F555A299AD9A673E2DFD45 (void);
// 0x000003B0 System.Void RSG.Promise/<>c__DisplayClass60_0::.ctor()
extern void U3CU3Ec__DisplayClass60_0__ctor_m99C72D8D0D1E5F3744F901AF2CE1D01B10074CD3 (void);
// 0x000003B1 RSG.IPromise RSG.Promise/<>c__DisplayClass60_0::<ThenSequence>b__0()
extern void U3CU3Ec__DisplayClass60_0_U3CThenSequenceU3Eb__0_m6FC48A06C04839828FBCC417B074F35B1849CBF1 (void);
// 0x000003B2 System.Void RSG.Promise/<>c__DisplayClass62_0::.ctor()
extern void U3CU3Ec__DisplayClass62_0__ctor_m4E600EF746E7384F9FF81A106144CCC5CA295EB6 (void);
// 0x000003B3 RSG.IPromise RSG.Promise/<>c__DisplayClass62_0::<Sequence>b__0(RSG.IPromise,System.Func`1<RSG.IPromise>)
extern void U3CU3Ec__DisplayClass62_0_U3CSequenceU3Eb__0_m3BD1BDD0E7EEF8F00E0C7DE8CD8AA502555164A8 (void);
// 0x000003B4 System.Void RSG.Promise/<>c__DisplayClass62_0::<Sequence>b__1()
extern void U3CU3Ec__DisplayClass62_0_U3CSequenceU3Eb__1_m030831A862679BA67E0AFFF2A793684303FDC197 (void);
// 0x000003B5 System.Void RSG.Promise/<>c__DisplayClass62_1::.ctor()
extern void U3CU3Ec__DisplayClass62_1__ctor_m4E3A09413376A37B8429009826FB5E58FB5918D7 (void);
// 0x000003B6 RSG.IPromise RSG.Promise/<>c__DisplayClass62_1::<Sequence>b__2()
extern void U3CU3Ec__DisplayClass62_1_U3CSequenceU3Eb__2_m38F09C7AB72B5AAAEE2B1F47D8B8E46A8B244D76 (void);
// 0x000003B7 System.Void RSG.Promise/<>c__DisplayClass62_1::<Sequence>b__3(System.Single)
extern void U3CU3Ec__DisplayClass62_1_U3CSequenceU3Eb__3_m45E00529CA4F236640E37AD28227E06C03260F75 (void);
// 0x000003B8 System.Void RSG.Promise/<>c__DisplayClass63_0::.ctor()
extern void U3CU3Ec__DisplayClass63_0__ctor_mA3AE9325599E7C269CF28464DFD338E2FB36D87B (void);
// 0x000003B9 RSG.IPromise RSG.Promise/<>c__DisplayClass63_0::<ThenRace>b__0()
extern void U3CU3Ec__DisplayClass63_0_U3CThenRaceU3Eb__0_m5B8C751B1FAD053AE58DE02DF3FEEA12AE43EC44 (void);
// 0x000003BA System.Void RSG.Promise/<>c__DisplayClass64_0`1::.ctor()
// 0x000003BB RSG.IPromise`1<ConvertedT> RSG.Promise/<>c__DisplayClass64_0`1::<ThenRace>b__0()
// 0x000003BC System.Void RSG.Promise/<>c__DisplayClass66_0::.ctor()
extern void U3CU3Ec__DisplayClass66_0__ctor_m20042739F9915BDA4DCECD38A6FAEC9A32FC9FD4 (void);
// 0x000003BD System.Void RSG.Promise/<>c__DisplayClass66_0::<Race>b__0(RSG.IPromise,System.Int32)
extern void U3CU3Ec__DisplayClass66_0_U3CRaceU3Eb__0_m3C1812F10C134DFFDE20266483FB8CEE43E00D28 (void);
// 0x000003BE System.Void RSG.Promise/<>c__DisplayClass66_0::<Race>b__2(System.Exception)
extern void U3CU3Ec__DisplayClass66_0_U3CRaceU3Eb__2_mD8A7C646663CD58D24E0B9E5AD99A223042BCC7B (void);
// 0x000003BF System.Void RSG.Promise/<>c__DisplayClass66_0::<Race>b__3()
extern void U3CU3Ec__DisplayClass66_0_U3CRaceU3Eb__3_m5925431199B068785646FFD433C27ACF0DA6C7D2 (void);
// 0x000003C0 System.Void RSG.Promise/<>c__DisplayClass66_1::.ctor()
extern void U3CU3Ec__DisplayClass66_1__ctor_m864691E480DAEE7AEEAD46F8FB882828DB249E77 (void);
// 0x000003C1 System.Void RSG.Promise/<>c__DisplayClass66_1::<Race>b__1(System.Single)
extern void U3CU3Ec__DisplayClass66_1_U3CRaceU3Eb__1_mAA09AC32AAC0FB42ED3E102028E9F62480131D48 (void);
// 0x000003C2 System.Void RSG.Promise/<>c__DisplayClass69_0::.ctor()
extern void U3CU3Ec__DisplayClass69_0__ctor_m1076FE9DB847E9C246A4B77B4A9AD68DA316DAFC (void);
// 0x000003C3 System.Void RSG.Promise/<>c__DisplayClass69_0::<Finally>b__0()
extern void U3CU3Ec__DisplayClass69_0_U3CFinallyU3Eb__0_m05B68401734C79A8332FD12FBF32C5C1DB2FCF61 (void);
// 0x000003C4 System.Void RSG.Promise/<>c__DisplayClass69_0::<Finally>b__1(System.Exception)
extern void U3CU3Ec__DisplayClass69_0_U3CFinallyU3Eb__1_m805874267E687B76A8017DEFD8A11A96DA9FA8AD (void);
// 0x000003C5 System.Void RSG.Promise/<>c__DisplayClass70_0::.ctor()
extern void U3CU3Ec__DisplayClass70_0__ctor_mD191E17C3CB17FB74E6636B739DFFE3332CF62FD (void);
// 0x000003C6 System.Void RSG.Promise/<>c__DisplayClass70_0::<ContinueWith>b__0()
extern void U3CU3Ec__DisplayClass70_0_U3CContinueWithU3Eb__0_m251D75318366BEDFA9CC05A71A3B108CBEBDE4D6 (void);
// 0x000003C7 System.Void RSG.Promise/<>c__DisplayClass70_0::<ContinueWith>b__1(System.Exception)
extern void U3CU3Ec__DisplayClass70_0_U3CContinueWithU3Eb__1_m8EF480C7A95C97BAE71BB581C32C4B1A14B98178 (void);
// 0x000003C8 System.Void RSG.Promise/<>c__DisplayClass71_0`1::.ctor()
// 0x000003C9 System.Void RSG.Promise/<>c__DisplayClass71_0`1::<ContinueWith>b__0()
// 0x000003CA System.Void RSG.Promise/<>c__DisplayClass71_0`1::<ContinueWith>b__1(System.Exception)
// 0x000003CB RSG.Tuple`2<T1,T2> RSG.Tuple::Create(T1,T2)
// 0x000003CC RSG.Tuple`3<T1,T2,T3> RSG.Tuple::Create(T1,T2,T3)
// 0x000003CD RSG.Tuple`4<T1,T2,T3,T4> RSG.Tuple::Create(T1,T2,T3,T4)
// 0x000003CE System.Void RSG.Tuple::.ctor()
extern void Tuple__ctor_mB90BC7455CD38661E7B174497948846C3717B84A (void);
// 0x000003CF System.Void RSG.Tuple`2::.ctor(T1,T2)
// 0x000003D0 T1 RSG.Tuple`2::get_Item1()
// 0x000003D1 System.Void RSG.Tuple`2::set_Item1(T1)
// 0x000003D2 T2 RSG.Tuple`2::get_Item2()
// 0x000003D3 System.Void RSG.Tuple`2::set_Item2(T2)
// 0x000003D4 System.Void RSG.Tuple`3::.ctor(T1,T2,T3)
// 0x000003D5 T1 RSG.Tuple`3::get_Item1()
// 0x000003D6 System.Void RSG.Tuple`3::set_Item1(T1)
// 0x000003D7 T2 RSG.Tuple`3::get_Item2()
// 0x000003D8 System.Void RSG.Tuple`3::set_Item2(T2)
// 0x000003D9 T3 RSG.Tuple`3::get_Item3()
// 0x000003DA System.Void RSG.Tuple`3::set_Item3(T3)
// 0x000003DB System.Void RSG.Tuple`4::.ctor(T1,T2,T3,T4)
// 0x000003DC T1 RSG.Tuple`4::get_Item1()
// 0x000003DD System.Void RSG.Tuple`4::set_Item1(T1)
// 0x000003DE T2 RSG.Tuple`4::get_Item2()
// 0x000003DF System.Void RSG.Tuple`4::set_Item2(T2)
// 0x000003E0 T3 RSG.Tuple`4::get_Item3()
// 0x000003E1 System.Void RSG.Tuple`4::set_Item3(T3)
// 0x000003E2 T4 RSG.Tuple`4::get_Item4()
// 0x000003E3 System.Void RSG.Tuple`4::set_Item4(T4)
// 0x000003E4 System.Void RSG.Exceptions.PromiseException::.ctor()
extern void PromiseException__ctor_mC1C353F7C09B485B8EFCF3B176412385745A37B2 (void);
// 0x000003E5 System.Void RSG.Exceptions.PromiseException::.ctor(System.String)
extern void PromiseException__ctor_m25FEF871F4EADD0813603091A36151F62E072F34 (void);
// 0x000003E6 System.Void RSG.Exceptions.PromiseException::.ctor(System.String,System.Exception)
extern void PromiseException__ctor_m378D7284D56C1662B14AFD057C60ACBBABCF8895 (void);
// 0x000003E7 System.Void RSG.Exceptions.PromiseStateException::.ctor()
extern void PromiseStateException__ctor_m0841D9ED95CB98A821651EB934050C7F99325539 (void);
// 0x000003E8 System.Void RSG.Exceptions.PromiseStateException::.ctor(System.String)
extern void PromiseStateException__ctor_mB10D86B4B0D696B724F6A344C9DA8BA582785E8A (void);
// 0x000003E9 System.Void RSG.Exceptions.PromiseStateException::.ctor(System.String,System.Exception)
extern void PromiseStateException__ctor_m09D7E625AC5B60FBAA33F53C52F2C5B9784C3AA9 (void);
// 0x000003EA System.Void RSG.Promises.EnumerableExt::Each(System.Collections.Generic.IEnumerable`1<T>,System.Action`1<T>)
// 0x000003EB System.Void RSG.Promises.EnumerableExt::Each(System.Collections.Generic.IEnumerable`1<T>,System.Action`2<T,System.Int32>)
// 0x000003EC System.Collections.Generic.IEnumerable`1<T> RSG.Promises.EnumerableExt::FromItems(T[])
// 0x000003ED System.Void RSG.Promises.EnumerableExt/<FromItems>d__2`1::.ctor(System.Int32)
// 0x000003EE System.Void RSG.Promises.EnumerableExt/<FromItems>d__2`1::System.IDisposable.Dispose()
// 0x000003EF System.Boolean RSG.Promises.EnumerableExt/<FromItems>d__2`1::MoveNext()
// 0x000003F0 T RSG.Promises.EnumerableExt/<FromItems>d__2`1::System.Collections.Generic.IEnumerator<T>.get_Current()
// 0x000003F1 System.Void RSG.Promises.EnumerableExt/<FromItems>d__2`1::System.Collections.IEnumerator.Reset()
// 0x000003F2 System.Object RSG.Promises.EnumerableExt/<FromItems>d__2`1::System.Collections.IEnumerator.get_Current()
// 0x000003F3 System.Collections.Generic.IEnumerator`1<T> RSG.Promises.EnumerableExt/<FromItems>d__2`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x000003F4 System.Collections.IEnumerator RSG.Promises.EnumerableExt/<FromItems>d__2`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000003F5 System.Collections.IEnumerator Proyecto26.HttpBase::CreateRequestAndRetry(Proyecto26.RequestHelper,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void HttpBase_CreateRequestAndRetry_m6DC921EB7E83F0FB058D9C9889B15D489092D059 (void);
// 0x000003F6 UnityEngine.Networking.UnityWebRequest Proyecto26.HttpBase::CreateRequest(Proyecto26.RequestHelper)
extern void HttpBase_CreateRequest_m395D82BBC11CA3C9D8FA5DCA55BC4E5AFE6AC451 (void);
// 0x000003F7 Proyecto26.RequestException Proyecto26.HttpBase::CreateException(Proyecto26.RequestHelper,UnityEngine.Networking.UnityWebRequest)
extern void HttpBase_CreateException_m4F00611C1B083E175A53008F96FCDAEED39E094E (void);
// 0x000003F8 System.Void Proyecto26.HttpBase::DebugLog(System.Boolean,System.Object,System.Boolean)
extern void HttpBase_DebugLog_mB403310A43B2C0F73923BE8135DBA43E5FECF8C3 (void);
// 0x000003F9 System.Collections.IEnumerator Proyecto26.HttpBase::DefaultUnityWebRequest(Proyecto26.RequestHelper,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void HttpBase_DefaultUnityWebRequest_mA2CA028D057F027E7D8283E63643F0AF2974B069 (void);
// 0x000003FA System.Collections.IEnumerator Proyecto26.HttpBase::DefaultUnityWebRequest(Proyecto26.RequestHelper,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,TResponse>)
// 0x000003FB System.Collections.IEnumerator Proyecto26.HttpBase::DefaultUnityWebRequest(Proyecto26.RequestHelper,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,TResponse[]>)
// 0x000003FC System.Void Proyecto26.HttpBase/<CreateRequestAndRetry>d__0::.ctor(System.Int32)
extern void U3CCreateRequestAndRetryU3Ed__0__ctor_mA038A5FC9BE11F7D821C2EB89C903BD81DD6673F (void);
// 0x000003FD System.Void Proyecto26.HttpBase/<CreateRequestAndRetry>d__0::System.IDisposable.Dispose()
extern void U3CCreateRequestAndRetryU3Ed__0_System_IDisposable_Dispose_m0F0654EB7C2275B8DDFC1B070E8B145B2C5FBCAC (void);
// 0x000003FE System.Boolean Proyecto26.HttpBase/<CreateRequestAndRetry>d__0::MoveNext()
extern void U3CCreateRequestAndRetryU3Ed__0_MoveNext_mC05783571C265F2A6348BF44253BDF379AD516DD (void);
// 0x000003FF System.Void Proyecto26.HttpBase/<CreateRequestAndRetry>d__0::<>m__Finally1()
extern void U3CCreateRequestAndRetryU3Ed__0_U3CU3Em__Finally1_m65EAC36497FC4D9964288E99CD0E428ABC806C46 (void);
// 0x00000400 System.Object Proyecto26.HttpBase/<CreateRequestAndRetry>d__0::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCreateRequestAndRetryU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m585D7434321501EABFADD21A314DCBB231EC3252 (void);
// 0x00000401 System.Void Proyecto26.HttpBase/<CreateRequestAndRetry>d__0::System.Collections.IEnumerator.Reset()
extern void U3CCreateRequestAndRetryU3Ed__0_System_Collections_IEnumerator_Reset_m09C357E4FCCD4430591B46B1960B5DC7D4BFE376 (void);
// 0x00000402 System.Object Proyecto26.HttpBase/<CreateRequestAndRetry>d__0::System.Collections.IEnumerator.get_Current()
extern void U3CCreateRequestAndRetryU3Ed__0_System_Collections_IEnumerator_get_Current_m4D5CD62D3F82FDDE9C67A9FD2E2894857D7746A6 (void);
// 0x00000403 System.Void Proyecto26.HttpBase/<>c__DisplayClass5_0`1::.ctor()
// 0x00000404 System.Void Proyecto26.HttpBase/<>c__DisplayClass5_0`1::<DefaultUnityWebRequest>b__0(Proyecto26.RequestException,Proyecto26.ResponseHelper)
// 0x00000405 System.Void Proyecto26.HttpBase/<>c__DisplayClass6_0`1::.ctor()
// 0x00000406 System.Void Proyecto26.HttpBase/<>c__DisplayClass6_0`1::<DefaultUnityWebRequest>b__0(Proyecto26.RequestException,Proyecto26.ResponseHelper)
// 0x00000407 T[] Proyecto26.JsonHelper::ArrayFromJson(System.String)
// 0x00000408 T[] Proyecto26.JsonHelper::FromJsonString(System.String)
// 0x00000409 System.String Proyecto26.JsonHelper::ArrayToJsonString(T[])
// 0x0000040A System.String Proyecto26.JsonHelper::ArrayToJsonString(T[],System.Boolean)
// 0x0000040B System.Void Proyecto26.JsonHelper/Wrapper`1::.ctor()
// 0x0000040C System.Boolean Proyecto26.RequestException::get_IsHttpError()
extern void RequestException_get_IsHttpError_mF1D87818843F7F4BFA1BB1B1F1EAD6925497CF30 (void);
// 0x0000040D System.Void Proyecto26.RequestException::set_IsHttpError(System.Boolean)
extern void RequestException_set_IsHttpError_mF311DBD704CCC61C23A270F8BCD1E7E8E5F17469 (void);
// 0x0000040E System.Boolean Proyecto26.RequestException::get_IsNetworkError()
extern void RequestException_get_IsNetworkError_mF6C31F24DCF7B5F649A4CABF455EBB03CB53434C (void);
// 0x0000040F System.Void Proyecto26.RequestException::set_IsNetworkError(System.Boolean)
extern void RequestException_set_IsNetworkError_mB4D9148437DD8A59B2969764E067E35E085ABB15 (void);
// 0x00000410 System.Int64 Proyecto26.RequestException::get_StatusCode()
extern void RequestException_get_StatusCode_m786C170A63F36EF40CF6F4347F5EACC85925E1F1 (void);
// 0x00000411 System.Void Proyecto26.RequestException::set_StatusCode(System.Int64)
extern void RequestException_set_StatusCode_mFD7034224DB7D4C27984CE0DA365D02173432309 (void);
// 0x00000412 System.String Proyecto26.RequestException::get_ServerMessage()
extern void RequestException_get_ServerMessage_m14FEBDED7437643B057376759428572897C03597 (void);
// 0x00000413 System.Void Proyecto26.RequestException::set_ServerMessage(System.String)
extern void RequestException_set_ServerMessage_m3DE596FD331A4F694B415FA3C8886FDC5F861DE9 (void);
// 0x00000414 System.String Proyecto26.RequestException::get_Response()
extern void RequestException_get_Response_m4FE8480DACD2F2347FDE3BC8F0843885E10912E5 (void);
// 0x00000415 System.Void Proyecto26.RequestException::set_Response(System.String)
extern void RequestException_set_Response_mEF4EB83001EB93F345CD9FB6214237F3888EDA25 (void);
// 0x00000416 System.Void Proyecto26.RequestException::.ctor()
extern void RequestException__ctor_m4255A573E554424A1B5C9F6E2D58590C50877C85 (void);
// 0x00000417 System.Void Proyecto26.RequestException::.ctor(System.String)
extern void RequestException__ctor_m2A12EC5D24756FA04C4D080FFD4DB9264B01753B (void);
// 0x00000418 System.Void Proyecto26.RequestException::.ctor(System.String,System.Object[])
extern void RequestException__ctor_m230AC0532C60E770A87DB74A57630F5464269D23 (void);
// 0x00000419 System.Void Proyecto26.RequestException::.ctor(System.String,System.Boolean,System.Boolean,System.Int64,System.String)
extern void RequestException__ctor_mD2293F4AA83F9A3A3EA0E48219894E91DF485FCC (void);
// 0x0000041A System.String Proyecto26.RequestHelper::get_Uri()
extern void RequestHelper_get_Uri_m579870F497E54040498F01E0F03D8E385B75682B (void);
// 0x0000041B System.Void Proyecto26.RequestHelper::set_Uri(System.String)
extern void RequestHelper_set_Uri_mED832E7E6B02422B23573A3BE154D41312A32BE3 (void);
// 0x0000041C System.String Proyecto26.RequestHelper::get_Method()
extern void RequestHelper_get_Method_m3CAF08A8E99D378C7693940ED9BBC576130C5558 (void);
// 0x0000041D System.Void Proyecto26.RequestHelper::set_Method(System.String)
extern void RequestHelper_set_Method_m93E8A5F66E37E3459E8F1689E272C9EA2303EEA2 (void);
// 0x0000041E System.Object Proyecto26.RequestHelper::get_Body()
extern void RequestHelper_get_Body_mE96291B55EA0C31805FDF261E1A6E0E0A04B9B4D (void);
// 0x0000041F System.Void Proyecto26.RequestHelper::set_Body(System.Object)
extern void RequestHelper_set_Body_m6C34BB52EE69E8A7D5B2FC14CE8478A4CCCB223A (void);
// 0x00000420 System.String Proyecto26.RequestHelper::get_BodyString()
extern void RequestHelper_get_BodyString_m583C6B16055049B23992E90B9BE9240D2494C872 (void);
// 0x00000421 System.Void Proyecto26.RequestHelper::set_BodyString(System.String)
extern void RequestHelper_set_BodyString_m264B491596B1E5D59DEBEDFA6D2C07EC37F6AF25 (void);
// 0x00000422 System.Byte[] Proyecto26.RequestHelper::get_BodyRaw()
extern void RequestHelper_get_BodyRaw_m0567B940B2FD769D548A63A3937EDAB8AF8BEB07 (void);
// 0x00000423 System.Void Proyecto26.RequestHelper::set_BodyRaw(System.Byte[])
extern void RequestHelper_set_BodyRaw_m4BB1B19C3109ED8297827657F5855DCAF7EF315E (void);
// 0x00000424 System.Nullable`1<System.Int32> Proyecto26.RequestHelper::get_Timeout()
extern void RequestHelper_get_Timeout_m5EB987EDD3D72C0AFC56600A506DED4393B31D03 (void);
// 0x00000425 System.Void Proyecto26.RequestHelper::set_Timeout(System.Nullable`1<System.Int32>)
extern void RequestHelper_set_Timeout_m8769C12B49481D0B8BC11B3227F802FDF049707F (void);
// 0x00000426 System.String Proyecto26.RequestHelper::get_ContentType()
extern void RequestHelper_get_ContentType_mF1D15920E2C975B8788B61B57B6A0EC713C5EB97 (void);
// 0x00000427 System.Void Proyecto26.RequestHelper::set_ContentType(System.String)
extern void RequestHelper_set_ContentType_m53EBFC2111ABCD757D3B613E943FED0F2A916B1F (void);
// 0x00000428 System.Int32 Proyecto26.RequestHelper::get_Retries()
extern void RequestHelper_get_Retries_mFE6F51F6EE991FEE0982128A8305F013668B7D5B (void);
// 0x00000429 System.Void Proyecto26.RequestHelper::set_Retries(System.Int32)
extern void RequestHelper_set_Retries_m8E91AB08F2D651BE88B57973D472A87CFE403AE1 (void);
// 0x0000042A System.Single Proyecto26.RequestHelper::get_RetrySecondsDelay()
extern void RequestHelper_get_RetrySecondsDelay_mF461806AF11B3F89D745861D3EE818A9548705F5 (void);
// 0x0000042B System.Void Proyecto26.RequestHelper::set_RetrySecondsDelay(System.Single)
extern void RequestHelper_set_RetrySecondsDelay_mF554D9A11BAD76DF31F07A5FA92B67E34FF7D6F8 (void);
// 0x0000042C System.Action`2<Proyecto26.RequestException,System.Int32> Proyecto26.RequestHelper::get_RetryCallback()
extern void RequestHelper_get_RetryCallback_m8487D96D1889FAC85CC99D59CE9AAD81AF975858 (void);
// 0x0000042D System.Void Proyecto26.RequestHelper::set_RetryCallback(System.Action`2<Proyecto26.RequestException,System.Int32>)
extern void RequestHelper_set_RetryCallback_m8B02AFF7AB55609093C48182D79501B34C2F2646 (void);
// 0x0000042E System.Boolean Proyecto26.RequestHelper::get_EnableDebug()
extern void RequestHelper_get_EnableDebug_m031DA45E2494D29141E793FBFE2BEF047DCF7472 (void);
// 0x0000042F System.Void Proyecto26.RequestHelper::set_EnableDebug(System.Boolean)
extern void RequestHelper_set_EnableDebug_m7DEAE27887D92A686E341EB14010DE5A4B3E4DF9 (void);
// 0x00000430 System.Nullable`1<System.Boolean> Proyecto26.RequestHelper::get_UseHttpContinue()
extern void RequestHelper_get_UseHttpContinue_m64713A0BA2260B82D1587517ADF0671DB60B8D66 (void);
// 0x00000431 System.Void Proyecto26.RequestHelper::set_UseHttpContinue(System.Nullable`1<System.Boolean>)
extern void RequestHelper_set_UseHttpContinue_m454427FB941A608ABD370EC1E64F0C5166183E48 (void);
// 0x00000432 System.Nullable`1<System.Int32> Proyecto26.RequestHelper::get_RedirectLimit()
extern void RequestHelper_get_RedirectLimit_m7628CFD271A1A06DBF264C289268CE52DA66DB82 (void);
// 0x00000433 System.Void Proyecto26.RequestHelper::set_RedirectLimit(System.Nullable`1<System.Int32>)
extern void RequestHelper_set_RedirectLimit_m32E15A1AE611D405855B008D58576C8839929DC3 (void);
// 0x00000434 System.Boolean Proyecto26.RequestHelper::get_IgnoreHttpException()
extern void RequestHelper_get_IgnoreHttpException_m0BA6B17AACCF9C74FEFD730806A64B67E4505FEA (void);
// 0x00000435 System.Void Proyecto26.RequestHelper::set_IgnoreHttpException(System.Boolean)
extern void RequestHelper_set_IgnoreHttpException_m6653B079C6C70E478895D1EF4BFAC9FE5D5D2209 (void);
// 0x00000436 UnityEngine.WWWForm Proyecto26.RequestHelper::get_FormData()
extern void RequestHelper_get_FormData_m1CE699C5BC879C7AEFBB1084CF0A1928CD02B931 (void);
// 0x00000437 System.Void Proyecto26.RequestHelper::set_FormData(UnityEngine.WWWForm)
extern void RequestHelper_set_FormData_mD33B2EEF5749F7898DB81BD2ACEB0F7EA9E97EC4 (void);
// 0x00000438 System.Collections.Generic.Dictionary`2<System.String,System.String> Proyecto26.RequestHelper::get_SimpleForm()
extern void RequestHelper_get_SimpleForm_mE158581E8CAC46C434A072324A6290CBE4878C78 (void);
// 0x00000439 System.Void Proyecto26.RequestHelper::set_SimpleForm(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void RequestHelper_set_SimpleForm_m885368EFE7D8FF37C0E9CBC36A8B551562042165 (void);
// 0x0000043A System.Collections.Generic.List`1<UnityEngine.Networking.IMultipartFormSection> Proyecto26.RequestHelper::get_FormSections()
extern void RequestHelper_get_FormSections_m4157D4255B827B864F882669136AB30BA93316F0 (void);
// 0x0000043B System.Void Proyecto26.RequestHelper::set_FormSections(System.Collections.Generic.List`1<UnityEngine.Networking.IMultipartFormSection>)
extern void RequestHelper_set_FormSections_m955F901BFE0A3D2FFE42F223BB2005BA2028337E (void);
// 0x0000043C UnityEngine.Networking.CertificateHandler Proyecto26.RequestHelper::get_CertificateHandler()
extern void RequestHelper_get_CertificateHandler_mBF95069255782425F21F491C5A51F3BBCEE956B1 (void);
// 0x0000043D System.Void Proyecto26.RequestHelper::set_CertificateHandler(UnityEngine.Networking.CertificateHandler)
extern void RequestHelper_set_CertificateHandler_m1B5296D19D58F16EF358706D8AC3817F0ED0B1C5 (void);
// 0x0000043E UnityEngine.Networking.UploadHandler Proyecto26.RequestHelper::get_UploadHandler()
extern void RequestHelper_get_UploadHandler_m5C72A7EF49F678C8CB02FA0620C0B41D1CF8AE78 (void);
// 0x0000043F System.Void Proyecto26.RequestHelper::set_UploadHandler(UnityEngine.Networking.UploadHandler)
extern void RequestHelper_set_UploadHandler_mDA3C9BCAE240C2A5185B88FE2FE7BF198B6467ED (void);
// 0x00000440 UnityEngine.Networking.DownloadHandler Proyecto26.RequestHelper::get_DownloadHandler()
extern void RequestHelper_get_DownloadHandler_m3CC95DFC6C81ABD8508947C1808BC8FB7095BDA3 (void);
// 0x00000441 System.Void Proyecto26.RequestHelper::set_DownloadHandler(UnityEngine.Networking.DownloadHandler)
extern void RequestHelper_set_DownloadHandler_mE7FC2C6C38C5418A915D4007DCFF2A9CF5C6DC74 (void);
// 0x00000442 System.Collections.Generic.Dictionary`2<System.String,System.String> Proyecto26.RequestHelper::get_Headers()
extern void RequestHelper_get_Headers_m09842B9228E6B35E7254DFAC41A5D2C98DBF0BE9 (void);
// 0x00000443 System.Void Proyecto26.RequestHelper::set_Headers(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void RequestHelper_set_Headers_mE4B41FA1E24D02AD057068E2D0A697C129921A29 (void);
// 0x00000444 System.Collections.Generic.Dictionary`2<System.String,System.String> Proyecto26.RequestHelper::get_Params()
extern void RequestHelper_get_Params_mDB495A9CB92B43BB1BD3526C99880EFBDC40E033 (void);
// 0x00000445 System.Void Proyecto26.RequestHelper::set_Params(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void RequestHelper_set_Params_m887AAEE89701BE57B2875DD482A584C58EF7C400 (void);
// 0x00000446 System.Boolean Proyecto26.RequestHelper::get_ParseResponseBody()
extern void RequestHelper_get_ParseResponseBody_m8352D39E0EBACC6FB532FBE05B5D3EC709EA43BD (void);
// 0x00000447 System.Void Proyecto26.RequestHelper::set_ParseResponseBody(System.Boolean)
extern void RequestHelper_set_ParseResponseBody_m1149B5FC0628F6BCDAFA95D21516A9CAE3F4031D (void);
// 0x00000448 UnityEngine.Networking.UnityWebRequest Proyecto26.RequestHelper::get_Request()
extern void RequestHelper_get_Request_mB3DD0BDA5D264948DE92BA5E060545D8D3CCBD63 (void);
// 0x00000449 System.Void Proyecto26.RequestHelper::set_Request(UnityEngine.Networking.UnityWebRequest)
extern void RequestHelper_set_Request_m4C49567052095FC092A24AD821EF7957CCB6F4BF (void);
// 0x0000044A System.Single Proyecto26.RequestHelper::get_UploadProgress()
extern void RequestHelper_get_UploadProgress_m4F7D8BCA66AE1D13F1C59D91EE10E656D48DE91B (void);
// 0x0000044B System.UInt64 Proyecto26.RequestHelper::get_UploadedBytes()
extern void RequestHelper_get_UploadedBytes_m7D3445EE67E794E91D82A28171224EED03460533 (void);
// 0x0000044C System.Single Proyecto26.RequestHelper::get_DownloadProgress()
extern void RequestHelper_get_DownloadProgress_m0ADF466E7C45A9D043506B099C6B3E7A08C5BE33 (void);
// 0x0000044D System.UInt64 Proyecto26.RequestHelper::get_DownloadedBytes()
extern void RequestHelper_get_DownloadedBytes_m5201AEFCAF699F4C054CBA3C63004950C1E32ED7 (void);
// 0x0000044E System.String Proyecto26.RequestHelper::GetHeader(System.String)
extern void RequestHelper_GetHeader_m0EAA6AEED4B885553B4CD97B2C4CBF81A32EF468 (void);
// 0x0000044F System.Boolean Proyecto26.RequestHelper::get_IsAborted()
extern void RequestHelper_get_IsAborted_mED2B0C2E2FA7DA65574ED4141818116D5407825B (void);
// 0x00000450 System.Void Proyecto26.RequestHelper::set_IsAborted(System.Boolean)
extern void RequestHelper_set_IsAborted_mF37D6A6B93B70D7B4AC2B579FDA59345D0D51DDF (void);
// 0x00000451 System.Boolean Proyecto26.RequestHelper::get_DefaultContentType()
extern void RequestHelper_get_DefaultContentType_mF52E8AD322DC2F3A0A0444FA1A60D2C62B42113F (void);
// 0x00000452 System.Void Proyecto26.RequestHelper::set_DefaultContentType(System.Boolean)
extern void RequestHelper_set_DefaultContentType_m0098EE480F8E776673979AFB751C76CD61AB5365 (void);
// 0x00000453 System.Void Proyecto26.RequestHelper::Abort()
extern void RequestHelper_Abort_m7CE069D799170B7D0336E7DB358C78B1B00DA8F3 (void);
// 0x00000454 System.Void Proyecto26.RequestHelper::.ctor()
extern void RequestHelper__ctor_m76D5CE4B07BAD0EFE344E9A8C4D1BFE895922C13 (void);
// 0x00000455 UnityEngine.Networking.UnityWebRequest Proyecto26.ResponseHelper::get_Request()
extern void ResponseHelper_get_Request_mE2DF5B066E83E4216167EEE1A28A79A09A372843 (void);
// 0x00000456 System.Void Proyecto26.ResponseHelper::set_Request(UnityEngine.Networking.UnityWebRequest)
extern void ResponseHelper_set_Request_m9743967B9D789F748E8205E910F339114CB7D522 (void);
// 0x00000457 System.Void Proyecto26.ResponseHelper::.ctor(UnityEngine.Networking.UnityWebRequest)
extern void ResponseHelper__ctor_m0F45F428ADF231F62EC7C6E5621058DA806A1850 (void);
// 0x00000458 System.Int64 Proyecto26.ResponseHelper::get_StatusCode()
extern void ResponseHelper_get_StatusCode_mAC67167172472DC6804430CA8FDEDDBF1FED2D4F (void);
// 0x00000459 System.Byte[] Proyecto26.ResponseHelper::get_Data()
extern void ResponseHelper_get_Data_mDFB63D2691666575C126F59C928580F359646E44 (void);
// 0x0000045A System.String Proyecto26.ResponseHelper::get_Text()
extern void ResponseHelper_get_Text_m4C6CE813DC4FA5B0659BF20EC4B5EE2504C618AF (void);
// 0x0000045B System.String Proyecto26.ResponseHelper::get_Error()
extern void ResponseHelper_get_Error_mAF99FD6F00AF225E4D2EF620266C6A428199F6E7 (void);
// 0x0000045C System.Collections.Generic.Dictionary`2<System.String,System.String> Proyecto26.ResponseHelper::get_Headers()
extern void ResponseHelper_get_Headers_m2990A8F894F71B2503103388796B0717EEF9D9CB (void);
// 0x0000045D System.String Proyecto26.ResponseHelper::GetHeader(System.String)
extern void ResponseHelper_GetHeader_m9E39EF8000022EA04DF66B43FCCB31AA01AB0D52 (void);
// 0x0000045E System.String Proyecto26.ResponseHelper::ToString()
extern void ResponseHelper_ToString_m3DD8554605B5BDE7A6412218369E317C804B0572 (void);
// 0x0000045F Proyecto26.StaticCoroutine/CoroutineHolder Proyecto26.StaticCoroutine::get_Runner()
extern void StaticCoroutine_get_Runner_mC13BCB3C4C6F548812E35E5555D3455AEB2700DF (void);
// 0x00000460 UnityEngine.Coroutine Proyecto26.StaticCoroutine::StartCoroutine(System.Collections.IEnumerator)
extern void StaticCoroutine_StartCoroutine_m46187D9942A6387EAD6EEE4771C5C54027547C58 (void);
// 0x00000461 System.Void Proyecto26.StaticCoroutine/CoroutineHolder::.ctor()
extern void CoroutineHolder__ctor_mC1424305AB5B1F2E631B7121BBB184B1B8BBBCF6 (void);
// 0x00000462 System.Version Proyecto26.RestClient::get_Version()
extern void RestClient_get_Version_m5701043CF8E7F883ACF65A25CADCE6AB4AFC8EB0 (void);
// 0x00000463 System.Collections.Generic.Dictionary`2<System.String,System.String> Proyecto26.RestClient::get_DefaultRequestParams()
extern void RestClient_get_DefaultRequestParams_mE23F55E6B8003CFEE7FF60C7483232FE7E247A3D (void);
// 0x00000464 System.Void Proyecto26.RestClient::set_DefaultRequestParams(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void RestClient_set_DefaultRequestParams_mA826F5ADE53A384196CC73D37D26E72E5B7CAA9B (void);
// 0x00000465 System.Void Proyecto26.RestClient::ClearDefaultParams()
extern void RestClient_ClearDefaultParams_mCBFF0C9C7DA720E8F0F9D54D0BDDE3D1C421CA71 (void);
// 0x00000466 System.Collections.Generic.Dictionary`2<System.String,System.String> Proyecto26.RestClient::get_DefaultRequestHeaders()
extern void RestClient_get_DefaultRequestHeaders_m7CFC695866D2FFFAE6C2C79B38564917EFFE37AA (void);
// 0x00000467 System.Void Proyecto26.RestClient::set_DefaultRequestHeaders(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void RestClient_set_DefaultRequestHeaders_m8ED47C4D90B6F05DB8ED523A5A49C2B0B1B7C2B5 (void);
// 0x00000468 System.Void Proyecto26.RestClient::ClearDefaultHeaders()
extern void RestClient_ClearDefaultHeaders_mFADDD96FA3D43A3CFC44B9A6A641B666F000C09F (void);
// 0x00000469 System.Void Proyecto26.RestClient::Request(Proyecto26.RequestHelper,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void RestClient_Request_m6D0EEC74B2073F8006977A737FEB5A26FF8B2BE4 (void);
// 0x0000046A System.Void Proyecto26.RestClient::Request(Proyecto26.RequestHelper,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,T>)
// 0x0000046B System.Void Proyecto26.RestClient::Get(System.String,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void RestClient_Get_mD3CD57C7B7809173BB0841F05883D4EAB667C4E4 (void);
// 0x0000046C System.Void Proyecto26.RestClient::Get(Proyecto26.RequestHelper,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void RestClient_Get_m62DDD6AA1F3E2D93DC5AF9378090C97D67EE7D54 (void);
// 0x0000046D System.Void Proyecto26.RestClient::Get(System.String,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,T>)
// 0x0000046E System.Void Proyecto26.RestClient::Get(Proyecto26.RequestHelper,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,T>)
// 0x0000046F System.Void Proyecto26.RestClient::GetArray(System.String,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,T[]>)
// 0x00000470 System.Void Proyecto26.RestClient::GetArray(Proyecto26.RequestHelper,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,T[]>)
// 0x00000471 System.Void Proyecto26.RestClient::Post(System.String,System.Object,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void RestClient_Post_m4B07264A4B85D23B2F3013C227AB599B04F18E7A (void);
// 0x00000472 System.Void Proyecto26.RestClient::Post(System.String,System.String,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void RestClient_Post_mAD4114F33CCE7E913400D3AA42D2494C1C71D27A (void);
// 0x00000473 System.Void Proyecto26.RestClient::Post(Proyecto26.RequestHelper,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void RestClient_Post_mB43880A95D1AD7E76C1878CB288DC4E6B3453651 (void);
// 0x00000474 System.Void Proyecto26.RestClient::Post(System.String,System.Object,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,T>)
// 0x00000475 System.Void Proyecto26.RestClient::Post(System.String,System.String,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,T>)
// 0x00000476 System.Void Proyecto26.RestClient::Post(Proyecto26.RequestHelper,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,T>)
// 0x00000477 System.Void Proyecto26.RestClient::PostArray(System.String,System.Object,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,T[]>)
// 0x00000478 System.Void Proyecto26.RestClient::PostArray(System.String,System.String,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,T[]>)
// 0x00000479 System.Void Proyecto26.RestClient::PostArray(Proyecto26.RequestHelper,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,T[]>)
// 0x0000047A System.Void Proyecto26.RestClient::Put(System.String,System.Object,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void RestClient_Put_m955E298F1437E5DDB4391BE3BF2A58B02232AA4E (void);
// 0x0000047B System.Void Proyecto26.RestClient::Put(System.String,System.String,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void RestClient_Put_m8AF7486C0FD795F8D4CCEBE663A56980E9397A02 (void);
// 0x0000047C System.Void Proyecto26.RestClient::Put(Proyecto26.RequestHelper,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void RestClient_Put_m08D2C2C9C8B6F73A7531BF8BD2AC15C9C5E13680 (void);
// 0x0000047D System.Void Proyecto26.RestClient::Put(System.String,System.Object,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,T>)
// 0x0000047E System.Void Proyecto26.RestClient::Put(System.String,System.String,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,T>)
// 0x0000047F System.Void Proyecto26.RestClient::Put(Proyecto26.RequestHelper,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,T>)
// 0x00000480 System.Void Proyecto26.RestClient::Delete(System.String,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void RestClient_Delete_m7701A6040B30FF21656087D1FFBE7BCAFA782E16 (void);
// 0x00000481 System.Void Proyecto26.RestClient::Delete(Proyecto26.RequestHelper,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void RestClient_Delete_m1C6AA27C4494A101CED7B1B6BFE9A3B38247FB60 (void);
// 0x00000482 System.Void Proyecto26.RestClient::Head(System.String,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void RestClient_Head_m01F7D5AEF77C54D5AE2948C1F579C1FD856AB0A8 (void);
// 0x00000483 System.Void Proyecto26.RestClient::Head(Proyecto26.RequestHelper,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void RestClient_Head_mFEFB7B87CFC87642E95B09D1A4AB76F63ABA3E14 (void);
// 0x00000484 RSG.IPromise`1<Proyecto26.ResponseHelper> Proyecto26.RestClient::Request(Proyecto26.RequestHelper)
extern void RestClient_Request_mA130CF629F30CB634AA89655F61C5BD37F729409 (void);
// 0x00000485 RSG.IPromise`1<T> Proyecto26.RestClient::Request(Proyecto26.RequestHelper)
// 0x00000486 RSG.IPromise`1<Proyecto26.ResponseHelper> Proyecto26.RestClient::Get(System.String)
extern void RestClient_Get_m7E5DEB235C456E5FAFCA828BF688ECFD630E86F7 (void);
// 0x00000487 RSG.IPromise`1<Proyecto26.ResponseHelper> Proyecto26.RestClient::Get(Proyecto26.RequestHelper)
extern void RestClient_Get_mDEA2483857BD9E64941E12CB42063D74EEF50FE6 (void);
// 0x00000488 RSG.IPromise`1<T> Proyecto26.RestClient::Get(System.String)
// 0x00000489 RSG.IPromise`1<T> Proyecto26.RestClient::Get(Proyecto26.RequestHelper)
// 0x0000048A RSG.IPromise`1<T[]> Proyecto26.RestClient::GetArray(System.String)
// 0x0000048B RSG.IPromise`1<T[]> Proyecto26.RestClient::GetArray(Proyecto26.RequestHelper)
// 0x0000048C RSG.IPromise`1<Proyecto26.ResponseHelper> Proyecto26.RestClient::Post(System.String,System.Object)
extern void RestClient_Post_m4763A3817990302191F3A5B069A64B02419523BC (void);
// 0x0000048D RSG.IPromise`1<Proyecto26.ResponseHelper> Proyecto26.RestClient::Post(System.String,System.String)
extern void RestClient_Post_mD59FE057DB71BF372315AB100BD5E4339FFFFE07 (void);
// 0x0000048E RSG.IPromise`1<Proyecto26.ResponseHelper> Proyecto26.RestClient::Post(Proyecto26.RequestHelper)
extern void RestClient_Post_m27A12B1AAD73F990A53E62B578D01BF4EA933509 (void);
// 0x0000048F RSG.IPromise`1<T> Proyecto26.RestClient::Post(System.String,System.Object)
// 0x00000490 RSG.IPromise`1<T> Proyecto26.RestClient::Post(System.String,System.String)
// 0x00000491 RSG.IPromise`1<T> Proyecto26.RestClient::Post(Proyecto26.RequestHelper)
// 0x00000492 RSG.IPromise`1<T[]> Proyecto26.RestClient::PostArray(System.String,System.Object)
// 0x00000493 RSG.IPromise`1<T[]> Proyecto26.RestClient::PostArray(System.String,System.String)
// 0x00000494 RSG.IPromise`1<T[]> Proyecto26.RestClient::PostArray(Proyecto26.RequestHelper)
// 0x00000495 RSG.IPromise`1<Proyecto26.ResponseHelper> Proyecto26.RestClient::Put(System.String,System.Object)
extern void RestClient_Put_mB84F593B9E21B27814D3EBAA2920B67D1699FD26 (void);
// 0x00000496 RSG.IPromise`1<Proyecto26.ResponseHelper> Proyecto26.RestClient::Put(System.String,System.String)
extern void RestClient_Put_m3E06B35E9CFAE4AF81DCF12A8F30A178E1CC5ABA (void);
// 0x00000497 RSG.IPromise`1<Proyecto26.ResponseHelper> Proyecto26.RestClient::Put(Proyecto26.RequestHelper)
extern void RestClient_Put_m5E7F007136807CD127CA4FD4D6A986B3979391F4 (void);
// 0x00000498 RSG.IPromise`1<T> Proyecto26.RestClient::Put(System.String,System.Object)
// 0x00000499 RSG.IPromise`1<T> Proyecto26.RestClient::Put(System.String,System.String)
// 0x0000049A RSG.IPromise`1<T> Proyecto26.RestClient::Put(Proyecto26.RequestHelper)
// 0x0000049B RSG.IPromise`1<Proyecto26.ResponseHelper> Proyecto26.RestClient::Delete(System.String)
extern void RestClient_Delete_m7CACD86724C07540430FDA0E5D759F6E57BBF75C (void);
// 0x0000049C RSG.IPromise`1<Proyecto26.ResponseHelper> Proyecto26.RestClient::Delete(Proyecto26.RequestHelper)
extern void RestClient_Delete_m33B84121232197C62DB7D3BAFB4E5F39CEF4C474 (void);
// 0x0000049D RSG.IPromise`1<Proyecto26.ResponseHelper> Proyecto26.RestClient::Head(System.String)
extern void RestClient_Head_m6CA11D53CD3CD3724C345EA2DCEBCF665E31D101 (void);
// 0x0000049E RSG.IPromise`1<Proyecto26.ResponseHelper> Proyecto26.RestClient::Head(Proyecto26.RequestHelper)
extern void RestClient_Head_mFE1399A7241D6D868C0AB0B65C774812167D9D43 (void);
// 0x0000049F System.Void Proyecto26.RestClient::Promisify(RSG.Promise`1<T>,Proyecto26.RequestException,T)
// 0x000004A0 System.Void Proyecto26.RestClient::Promisify(RSG.Promise`1<T>,Proyecto26.RequestException,Proyecto26.ResponseHelper,T)
// 0x000004A1 System.String Proyecto26.Common.Common::GetFormSectionsContentType(System.Byte[]&,Proyecto26.RequestHelper)
extern void Common_GetFormSectionsContentType_m19E34A4A4613B617EB59187AE573639031CD6B93 (void);
// 0x000004A2 System.Void Proyecto26.Common.Common::ConfigureWebRequestWithOptions(UnityEngine.Networking.UnityWebRequest,System.Byte[],System.String,Proyecto26.RequestHelper)
extern void Common_ConfigureWebRequestWithOptions_mF5668E71E221566BFD09C871A2B0667206901F78 (void);
// 0x000004A3 System.Collections.IEnumerator Proyecto26.Common.Common::SendWebRequestWithOptions(UnityEngine.Networking.UnityWebRequest,Proyecto26.RequestHelper)
extern void Common_SendWebRequestWithOptions_mA6DA8F8469D1F337E3CB043E57F2191D8D95EE1B (void);
// 0x000004A4 System.Void Proyecto26.Common.Common/<SendWebRequestWithOptions>d__4::.ctor(System.Int32)
extern void U3CSendWebRequestWithOptionsU3Ed__4__ctor_m47FB1CC913CF34CE244286B13D6600F97F2C7006 (void);
// 0x000004A5 System.Void Proyecto26.Common.Common/<SendWebRequestWithOptions>d__4::System.IDisposable.Dispose()
extern void U3CSendWebRequestWithOptionsU3Ed__4_System_IDisposable_Dispose_m5F26553216BF51BE2953AFD9118938F28D511A34 (void);
// 0x000004A6 System.Boolean Proyecto26.Common.Common/<SendWebRequestWithOptions>d__4::MoveNext()
extern void U3CSendWebRequestWithOptionsU3Ed__4_MoveNext_mB5E4CFE2AAE6A9BCE0D76C72C378282AFCB56C68 (void);
// 0x000004A7 System.Object Proyecto26.Common.Common/<SendWebRequestWithOptions>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSendWebRequestWithOptionsU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF49002192678DCE4CE3D8D78E1E922C730FED67C (void);
// 0x000004A8 System.Void Proyecto26.Common.Common/<SendWebRequestWithOptions>d__4::System.Collections.IEnumerator.Reset()
extern void U3CSendWebRequestWithOptionsU3Ed__4_System_Collections_IEnumerator_Reset_m82497AA2724FEA6918E2C0900364DA889677B45E (void);
// 0x000004A9 System.Object Proyecto26.Common.Common/<SendWebRequestWithOptions>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CSendWebRequestWithOptionsU3Ed__4_System_Collections_IEnumerator_get_Current_mD15322133CA601A9884E3FCBDE8BD76FFFB1558D (void);
// 0x000004AA Proyecto26.ResponseHelper Proyecto26.Common.Extensions::CreateWebResponse(UnityEngine.Networking.UnityWebRequest)
extern void Extensions_CreateWebResponse_m85A8BFBB1916DE1247943658A0758B372807A7F1 (void);
// 0x000004AB System.Boolean Proyecto26.Common.Extensions::IsValidRequest(UnityEngine.Networking.UnityWebRequest,Proyecto26.RequestHelper)
extern void Extensions_IsValidRequest_m95295E8977C94FB722EE97C5047C3975DE5EA65A (void);
// 0x000004AC System.String Proyecto26.Common.Extensions::EscapeURL(System.String)
extern void Extensions_EscapeURL_mAAD51E7CBC83CB8CE79715D8A6086CB153919CF7 (void);
// 0x000004AD System.String Proyecto26.Common.Extensions::BuildUrl(System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void Extensions_BuildUrl_mE2D5E2B005BCD677EED820AE36D5F28F01E57CA0 (void);
// 0x000004AE System.Void Proyecto26.Common.Extensions/<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_mDEE443CFFB4553720586AE36F8109D07F7E08B87 (void);
// 0x000004AF System.Boolean Proyecto26.Common.Extensions/<>c__DisplayClass3_0::<BuildUrl>b__0(System.Collections.Generic.KeyValuePair`2<System.String,System.String>)
extern void U3CU3Ec__DisplayClass3_0_U3CBuildUrlU3Eb__0_mDFB7EBC5B80A07C2899A8B10EE18130CC2AE4157 (void);
// 0x000004B0 System.Void Proyecto26.Common.Extensions/<>c::.cctor()
extern void U3CU3Ec__cctor_m51C966458A7B400E929B365351E9FA572508174F (void);
// 0x000004B1 System.Void Proyecto26.Common.Extensions/<>c::.ctor()
extern void U3CU3Ec__ctor_m3AF7EC6F7B8017905D17E0078A8E8219711B52B7 (void);
// 0x000004B2 System.String Proyecto26.Common.Extensions/<>c::<BuildUrl>b__3_1(System.Collections.Generic.KeyValuePair`2<System.String,System.String>)
extern void U3CU3Ec_U3CBuildUrlU3Eb__3_1_m3F16B42B55E86F33694E97ABD64E3ADB7B6EDD03 (void);
// 0x000004B3 System.String Models.Photo::ToString()
extern void Photo_ToString_mC7549402494D8D3A4192988B7A75C3F43BB81C7D (void);
// 0x000004B4 System.Void Models.Photo::.ctor()
extern void Photo__ctor_mFB7B355DA04B8883B308CC03347EF777898F6846 (void);
// 0x000004B5 System.String Models.Post::ToString()
extern void Post_ToString_m4AD8D8586CF507C7C435254089148F3AD32A855A (void);
// 0x000004B6 System.Void Models.Post::.ctor()
extern void Post__ctor_mAD9D421E1716FEB8D8B297565CE8F10FFB42CC79 (void);
// 0x000004B7 System.String Models.Todo::ToString()
extern void Todo_ToString_mAD1AD0938A238807A2D0EA2888893C11CBE105CF (void);
// 0x000004B8 System.Void Models.Todo::.ctor()
extern void Todo__ctor_m3DBCDF59C636313C811C93C83ED5ABC292A19AA4 (void);
// 0x000004B9 System.String Models.User::ToString()
extern void User_ToString_mD57533E879F6F6B77BD1A38883A0DAED53D2FAF2 (void);
// 0x000004BA System.Void Models.User::.ctor()
extern void User__ctor_m179E4458F95BA48DD9D0F7E1E4DF8AC26DB05C0F (void);
static Il2CppMethodPointer s_methodPointers[1210] = 
{
	MainScript_LogMessage_m7953CB447ABFC4D0E286219425CF26684DB6F6B0,
	MainScript_Get_m3BD5249433D12EE1562CB113FF95FD302207B2A0,
	MainScript_Post_mF7C21819A39C80CBEF1E5471FF7A505A6CF9B687,
	MainScript_Put_m2E077AE7D135014788A9F219D4FA32B618BED552,
	MainScript_Delete_m3CEEF47B740A70E766D3CF74E6C1D37E13C0A8C8,
	MainScript_AbortRequest_mDCEC8008FD946FB21C66D8DE04AA652FD091AC5E,
	MainScript_DownloadFile_mF54C07D06969AD450838E27A885FB450BDA6971C,
	MainScript__ctor_mDE68DE7EC111F10F50287CE15AC2100BAC26DF23,
	MainScript_U3CPostU3Eb__4_0_m8B0202AA78218D3787871313668C76F260A52D42,
	MainScript_U3CPostU3Eb__4_1_m2C8C662A8A4EE9A219C715C38E88E9D156CC0777,
	MainScript_U3CPutU3Eb__5_0_m556845DFA7BB10FCFF4A3A04B17EF6BC8AA77CAE,
	MainScript_U3CDeleteU3Eb__6_0_m3092FBC0D31C4B83C0C7E537BFD404FD28F0BB8E,
	MainScript_U3CDownloadFileU3Eb__8_0_m8D61A6C39FFBB97884CF0935DA9BF297B5DE8393,
	MainScript_U3CDownloadFileU3Eb__8_1_m99B4D340A42D8D8B784669EDB99B721CAD853FB6,
	U3CU3Ec__DisplayClass3_0__ctor_m46C50E0A5C12FA151CC55E0B928E27838925B4FC,
	U3CU3Ec__DisplayClass3_0_U3CGetU3Eb__0_m1E95C1F0B2D8E5BC302A5ADCA1ABAE0293AEA82D,
	U3CU3Ec__DisplayClass3_0_U3CGetU3Eb__1_mB5A9CA352F06FB80FDC47E6FE114495BA4687AC1,
	U3CU3Ec__DisplayClass3_0_U3CGetU3Eb__2_mB93ACE707409C85F0542FD7665B0BF499473D98D,
	U3CU3Ec__DisplayClass3_0_U3CGetU3Eb__3_m381C9E851E8FB988B02A1076B749138ABC954BDB,
	U3CU3Ec__DisplayClass3_0_U3CGetU3Eb__4_m44FBEB10E4BAEB006FC92154DB40AB3CB7BC4DE0,
	U3CU3Ec__cctor_m4FA3DF2408BEA55F36632D2A36DDEA9E1475BBA3,
	U3CU3Ec__ctor_m6B5FCC705564205B21810FFBA043CB8DA659F8E1,
	U3CU3Ec_U3CPutU3Eb__5_1_mC12F48C710EDD364D74E0411BCE9CB0F55326D2F,
	Bullet_Awake_m2D77C2A3CF11F66E86FF074B8C4397C0E3DE2004,
	Bullet_Init_m98ADE8968CAB3858821FA1D84A67385DD8A4176A,
	Bullet_Move_m548616709427BF038F8FC6EEF384B129BE90CF3D,
	Bullet_OnTriggerEnter_m670A42C7BC93AF346496B6599303EF4C10FE690A,
	Bullet__ctor_mC7D931FE508342F413FBA79525A4819D4114B3EC,
	CharacterInfo__ctor_mDB9D9524D840EB8F6461A1EF79322F9A6A10D8C2,
	ChooseCharacterInfo__ctor_m4E173EE4F57AE6E8151BBD432CAB13224DF6AC56,
	JennieCharacter_Awake_m2675C76F05D0562E394C95CBA933210A2D4712CD,
	JennieCharacter_Init_mFB32E3DFC6DEBE1CC69F0AF2B062974E8565E499,
	JennieCharacter_Attack_m0CF7BB9CDF008051C834D23E435F70E9E3375C99,
	JennieCharacter_Die_mAC9D04281CAFC80DA629E25E904E8B0460C0E3C2,
	JennieCharacter_Open_m7CD20A1ED71868B739474F449A1B76C7EA653BC5,
	JennieCharacter_TakeHit_m460F00CBD168D3684FABBE75E2E414035A506224,
	JennieCharacter_TakeCollision_m9302E68C3D8028FD5CC46B7AE079B957DE88A2FC,
	JennieCharacter_NotTakeCollision_m393AC75D5B608B7BFEB71B26FE2569A06051A643,
	JennieCharacter_OpenShelfZero_m569C780547653E4AACD2954C954B100206C94576,
	JennieCharacter_OpenShelfOne_mFF0F138C1B64A94C895CB4B2E65828FE01963EBD,
	JennieCharacter_OpenShelfTwo_m9C3421FF2E5A10564740A075B20026A4384E83B2,
	JennieCharacter_OpenShelfThree_m4119D14AB62553583CF1599449BD1F5C66E81EB3,
	JennieCharacter_OpenShelfFour_m5014598B7E824B499A975C523055CE93F43CF284,
	JennieCharacter_OpenShelfFive_m960A4E43F09D5561D3D80C677F2AC71ECFF27E14,
	JennieCharacter_OpenShelfSix_m57FBB13CD720B7978589B0AE81CFAB01D819F31C,
	JennieCharacter_OpenShelfSeven_mC4DDA3142721E75D92D53254A3AF13C920E82AF6,
	JennieCharacter_OpenShelfEight_m2FEACBE5D2CC008D6363F6634FF84CDC881C00FB,
	JennieCharacter_OpenShelfNine_mE0F2C74ACD37813CF812642450421BB19779AD58,
	JennieCharacter_OpenShelfTen_mA95829425480F2FDC27489630134C11BB157E6C4,
	JennieCharacter_OpenShelfEleven_mF3A8E905C6DA84DB82FA09188CDB31BAF7ECB79F,
	JennieCharacter_OpenShelfTwelve_m9F734BCC2B4B54B8337F94BA8D30C6592B9E62D6,
	JennieCharacter_OpenShelfThirteen_m8445F76D6441558535847821C14BAF5B91CBA14A,
	JennieCharacter_OpenShelfFourteen_mB069AB2D7A071CFDEDF9A776592F5D6022F5DD03,
	JennieCharacter_OpenShelfFifteen_m884A5A7882B30382361035C1C0EB6DC8D051D00D,
	JennieCharacter_OpenShelfSixteen_mE54EA9B9795EF11A386D05E7523989AA110257ED,
	JennieCharacter__ctor_m8396610D4C601D393F1D587BCF086C349EF00152,
	JohnCharacter_Awake_m0E3DB4E38662CFE030DD41853EB7F2BAC688A38A,
	JohnCharacter_Init_m851F47FF610A0867BF5EAA427EFE25331F3624EF,
	JohnCharacter_Attack_m747AB55B2423243DE582336AE9A5CE430F410787,
	JohnCharacter_Die_mC3F732919409BF51811C4DEA3FCE8FB8CB47BC6F,
	JohnCharacter_Open_m0E6D33ACD4D4DDB743C3044077D6AFCE483E29BE,
	JohnCharacter_TakeHit_m2A46A1E14C03BEB127B879D2CA5A570419A848C1,
	JohnCharacter_TakeCollision_m234D021D28175C5DAFE288290CEA09759C6C773B,
	JohnCharacter_NotTakeCollision_m8D697042F7AF8087C8ED8A8F91FE9F7085B225AA,
	JohnCharacter_OpenShelfZero_m64B9D62E10E24247D5E687EA2026BADA39699363,
	JohnCharacter_OpenShelfOne_mF7925886EB9BC7311361BCF98BBB23414DD35892,
	JohnCharacter_OpenShelfTwo_mCE0218E790E61D0FB1959FC142E7EE29A1639A87,
	JohnCharacter_OpenShelfThree_m066B333C120196DA96E363D610855E763AA921A8,
	JohnCharacter_OpenShelfFour_mC7ED1C5C9E63C8065F9EBA4B183E8FBC0AC0D724,
	JohnCharacter_OpenShelfFive_m2B40148E1D1E842FDC3A9317CD5BDFD6E4B49E96,
	JohnCharacter_OpenShelfSix_m3BD56BFAD4AAFBC91EDCC9F4534C3B31C793DA62,
	JohnCharacter_OpenShelfSeven_m203F842ECF8ED058ED227A4605EB2FA4F1A56BD2,
	JohnCharacter_OpenShelfEight_mB638ABB9CAA4FD01337A8B3F9EC34E0FAFADE8B0,
	JohnCharacter_OpenShelfNine_mF3B06BC041362379235F87B0152A15B3E5949623,
	JohnCharacter_OpenShelfTen_m1DC0B152E946CDABAFB1C4FFC24CC4F19BBB0AD8,
	JohnCharacter_OpenShelfEleven_mC64DE528583ED9A890FCF3B43D34B300D302EA24,
	JohnCharacter_OpenShelfTwelve_mB1F16EE4F1B91D9049471D4FAEDDE3FD5DD5CB6F,
	JohnCharacter_OpenShelfThirteen_m08F5BED5E99DC70772B756427BEF99C52E69E6B2,
	JohnCharacter_OpenShelfFourteen_mFCA39E749ED72213F3181D79B8F2B7B56AE60C8F,
	JohnCharacter_OpenShelfFifteen_mF2F7B0A17CDB660724FC0A07C8D223BF2A189585,
	JohnCharacter_OpenShelfSixteen_m213C3064F417AE5E9BC61E1006E1C49B6E15F39F,
	JohnCharacter__ctor_m4A5F777EB34BEA52677E30D069F738B7F6D2BE2A,
	EnemyController_Update_mDB0B02F4008FD062F471267A67A06169E1BC1B3C,
	EnemyController_Target_mA1B724BCFBCE5171EBE0C36484810BDB3B9EE36D,
	EnemyController_ChasePlayer_m0410883BAD98EBFFD819FC3394F67399E9499C53,
	EnemyController_Patrolling_mBC53FB53167D372361C5C20C21C43D0B96B1C852,
	EnemyController_SearchWalkPoint_m2ED2B7F18F2A488A704BE52DB89B13950AF43A58,
	EnemyController_OnDrawGizmosSelected_m8009702CE33352E8EE2ACB15B8D2AB985C1E80AC,
	EnemyController_TakeHit_mB4823CC7995CC19860F2BC24E38A8212C32C7F1B,
	EnemyController_OnTriggerEnter_mA87502B43AFEDE1A85F3D768F6EE85F0A8930308,
	EnemyController_Stun_m7E3350CD634381655B1C3431288C09BE9AAA4CB5,
	EnemyController__ctor_m547F49905D505F962CBC708846F8E8A3B0838F70,
	U3CStunU3Ed__25__ctor_mC4CCF189717A66C781EA8571D96032AD5369E710,
	U3CStunU3Ed__25_System_IDisposable_Dispose_mDB19DC2FBC53BCF5517E19969BE0FE212912BB56,
	U3CStunU3Ed__25_MoveNext_m4948DEFF972B007E10F7776E303956B42A002D3B,
	U3CStunU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m006EB001AD1FA918E3D8E11C0062B63E3CD414EB,
	U3CStunU3Ed__25_System_Collections_IEnumerator_Reset_m49E5932E65C8B5AD2FEF0781365707DF05AC38A9,
	U3CStunU3Ed__25_System_Collections_IEnumerator_get_Current_m00D904DCA24B2741C875F23A3E96132EED59067B,
	ScoreInfo__ctor_mA5525978BB93E4E29663142B356B1AA92A0DD6E9,
	ItemList_Init_mF4121001AD672541AF238ABA5E8149CADE7B3A42,
	ItemList__ctor_m1BDD77BEBF23A2851584E6BC874024B1D49B6474,
	ItemShelf_Init_m9F6E72A84F775ADC7444244EB7A9F3939EF05CB7,
	ItemShelf__ctor_m5D7FFDDA969A6A353034327B77980325EA3764D4,
	GameManager_Awake_m22F42B2A82708B10F652CAD8F2E0A4767110FF30,
	GameManager_StartGame_m6022C5CDD590728691B22E9B87185BFE3D6A8EC1,
	GameManager_CountDownTime_m774E99963E283E6FCFB0ED04887FC3F9E52228B5,
	GameManager_EndGame_m34CB0E063C72D2D7BA815B5397C5DB865EE60810,
	GameManager__ctor_mE8666F6D0CA9C31E16B719F79780DC4B0245B64D,
	U3CCountDownTimeU3Ed__3__ctor_m0741F40648A96BA1C1154602DB873E1A95EF7F02,
	U3CCountDownTimeU3Ed__3_System_IDisposable_Dispose_mC98CC4D20ED03E14A44ED0167236364DC6E49B47,
	U3CCountDownTimeU3Ed__3_MoveNext_m88D608228620A2420188FDE96B4742C593D864E1,
	U3CCountDownTimeU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m432CEE0DC100C67E519FD20F93567099DFBEC09D,
	U3CCountDownTimeU3Ed__3_System_Collections_IEnumerator_Reset_mB631E70306B4BA94A494F53CCBECD5CCAF89924B,
	U3CCountDownTimeU3Ed__3_System_Collections_IEnumerator_get_Current_m5295FD4B6544423C8B35758FA7C649609393BC82,
	ItemManager_get_CountCreateItem_m6C552BDEA7914A6DAAFD5DB9BEE1F60B63E18D66,
	ItemManager_set_CountCreateItem_m087FE1C92277CACFDF2B57127A672495A9165091,
	ItemManager_get_CountChips_m431AD1777282BBE638A20105601FF45F0571E241,
	ItemManager_set_CountChips_m6C4EB6A2399A762F372E9ECC309C74710EB89E3B,
	ItemManager_get_CountChocolate_m587B00C587B6BB77CF9959368D4BF2AE628A0841,
	ItemManager_set_CountChocolate_m2E5EDC57FCBB72835BA29532A00681F7D426EAAF,
	ItemManager_get_CountCandy_mD425F962956CBCE080D580B81D6C2336AE062663,
	ItemManager_set_CountCandy_mBBAF17D24BA57FA6F26F9AD94E14F7CFC58A5DBC,
	ItemManager_get_CountMilk_m7D0F9D93518F11AB606A0EA5BF212F696E6C7093,
	ItemManager_set_CountMilk_m9FD522B8470865B15F1E4EE1E36E1A22BA7AE70B,
	ItemManager_get_CountCola_mAD239036425F52D9BC9E9B69DF407ABC8A332078,
	ItemManager_set_CountCola_m1A7CA9AB8EB7F8B9829295A4D495045C91DFB4BB,
	ItemManager_get_CountMeat_mC4FF877C321FDB09033ADA52F147CF14157010E1,
	ItemManager_set_CountMeat_m0D6F51990969E8BDEA8EC4747C4B2F98283FA1B9,
	ItemManager_get_CountIceCream_mCE8B43152CF5DBAE44E478594A2ACF97A8530D9D,
	ItemManager_set_CountIceCream_mA4205FF84CED9EC7A7377633DDCD9B31875FDD3A,
	ItemManager_get_CountFrozenFood_m6E9A7BF0B2418409E3D3FBDE4BB49A2C2B61FCA3,
	ItemManager_set_CountFrozenFood_m9D615D288C7F81ED97985CD72AA197A71839FD42,
	ItemManager_get_CountVegetables_m210EF2EB4D68EF632248FFF648B1DB7A008A5F28,
	ItemManager_set_CountVegetables_m0E8C52DE66373314386315C34EDEC1571433656B,
	ItemManager_get_CountBattery_mA3DE2B0E9ADD96E9352B425D4B1D15B7C6E11C45,
	ItemManager_set_CountBattery_m2DAA7D323706C85D611C6D2DBC6ACE7C5D277496,
	ItemManager_get_CountWire_mC5C55E17672A312BA901B71D037140A940853AA1,
	ItemManager_set_CountWire_mEBF3FD4D022794B5BF80CFE94B9CB8687B91D8AC,
	ItemManager_get_CountLightBulb_mDC786E62738959A55E84E2DA7EB400EF8E2AA33E,
	ItemManager_set_CountLightBulb_mCACA46B4E35537CFA786D438855F8D2F6842CAAE,
	ItemManager_Awake_m38A9CB7617956210243C27F71C25FE547791586C,
	ItemManager_SetOnClick_mF5BA6710568D6BF2ECC35393452B3CC1D048B8C2,
	ItemManager_SetScoreEasyItem_m416865AD0DE87B15AF1992D191D8257706CAB274,
	ItemManager_SetScoreModerateItem_mEA9B5C6DE280D7DE3C53C69C949F72C4EFE88F2C,
	ItemManager_SetScoreHardItem_mD8230DD694D8C3A6BB32E806B294F3993FE2DF3B,
	ItemManager_CheckEndGame_mB2F4C0706F5DF997091B3AFBE6B6BDEFDC0FCC11,
	ItemManager_StartGame_m723F4FAEC2D99C000F6E81A4220D7052FE5D413B,
	ItemManager_StartCreateItemShelf_mA43BB29365DD92D1E17F6D5496361938DAA83EAD,
	ItemManager_CreateItemShelf_mF6C5DE07A96B6DAA0A01C57C51A61C5CFD0DBE3E,
	ItemManager_SetEasyItemList_m655E0AA5793D6317BF3A4382E60C6461D4E56B09,
	ItemManager_SetModerateItemList_m472A61DF6AA6256676030BF62286335F0315B798,
	ItemManager_SetHardItemList_m8B9EEA7AB916BA6C9819C8EE09984184A8A67C93,
	ItemManager_RandomEasyItem_m20A3EDEF413E3744DE4A21503ACC66D26C4CDE60,
	ItemManager_RandomHardItem_mF5E40F84ECA2AA4141E43FE5F694D6B22912AB50,
	ItemManager_RandomModerateItem_mB606CB27B956726637233C584EF98994F72C1F3B,
	ItemManager_CreateItemList_m1C073B93036ED928124E029857A2B1C79CECF002,
	ItemManager_InstantiateItemList_mC54CFA14082486311CE5CEB05152CE597C8FCB58,
	ItemManager__ctor_mB4E542587417D4CED86516E00930C40F85219460,
	U3CU3Ec__DisplayClass98_0__ctor_m0022B9B4E8B7B4E0682D50EF314DE224B1118869,
	U3CU3Ec__DisplayClass98_0_U3CSetOnClickU3Eb__0_m3C6E39B7B8703F405A7BA4BA7A4ECE5F135B1A4B,
	U3CU3Ec__DisplayClass98_1__ctor_m359E547BE5982C9FCAB089F836B0F99ACFA55E17,
	U3CU3Ec__DisplayClass98_1_U3CSetOnClickU3Eb__1_m291760F0355B86DC17F349B27E8EF4E3C34910B3,
	U3CU3Ec__DisplayClass98_2__ctor_mB2A461F19C95BDFB00780B3B1996C23C43E38BF2,
	U3CU3Ec__DisplayClass98_2_U3CSetOnClickU3Eb__2_m2EB947AAD4F6A393E800E6BE2A5B91226EA3CFE8,
	U3CU3Ec__DisplayClass98_3__ctor_m05ACC0B8E0E8AA5F256E6AB950021838051439FE,
	U3CU3Ec__DisplayClass98_3_U3CSetOnClickU3Eb__3_mF69BAE41A31F5D3FFF11DACAD81F500B7DE46182,
	U3CU3Ec__DisplayClass98_4__ctor_mADBC978537A18B54E4A98CEE25273564FD3F30D3,
	U3CU3Ec__DisplayClass98_4_U3CSetOnClickU3Eb__4_mCA0C55F6B2A788063D18547A131604C21C876FD6,
	U3CU3Ec__DisplayClass98_5__ctor_mCDE572D1F9BB5930F97E5980D4BEE4F025E3D350,
	U3CU3Ec__DisplayClass98_5_U3CSetOnClickU3Eb__5_m9B677D36B7F7383886078590171750211288F291,
	U3CU3Ec__DisplayClass98_6__ctor_m5F480B615C08FB7D46C3EE45064AF8748E5357AD,
	U3CU3Ec__DisplayClass98_6_U3CSetOnClickU3Eb__6_mF47435C5165F5A0ED519BAA49EEAB2DEF3117D43,
	U3CU3Ec__DisplayClass98_7__ctor_m6C803F8D1AF4AFA5B1EE6F8EFAB6F181783A1610,
	U3CU3Ec__DisplayClass98_7_U3CSetOnClickU3Eb__7_m3A817EB00BA0D0BB47D52076A1B97476D23095D3,
	U3CU3Ec__DisplayClass98_8__ctor_m21FA20DB4ADEE856FE089F20E91BD8B91B8BDC1E,
	U3CU3Ec__DisplayClass98_8_U3CSetOnClickU3Eb__8_m8BFD6F2202D20F29F0A5CBFB7692E33FCE8FCB58,
	U3CU3Ec__DisplayClass98_9__ctor_mD86D8431B928711476742ED99CF82037FB2C9A85,
	U3CU3Ec__DisplayClass98_9_U3CSetOnClickU3Eb__9_m8EE539E7CC807233A51F72DAD421AF28CC31EDB2,
	U3CU3Ec__DisplayClass98_10__ctor_mE25CFBD6FF076C21F38432A2F81B92B90323ECE5,
	U3CU3Ec__DisplayClass98_10_U3CSetOnClickU3Eb__10_m4B6F28A348EEA3EE165B24303F5525E8A5DF7267,
	U3CU3Ec__DisplayClass98_11__ctor_m5CAD8112C745653D5D9AB4E147F5DD5242EDAA44,
	U3CU3Ec__DisplayClass98_11_U3CSetOnClickU3Eb__11_m9AFA6A21DC4236954888EB7573504213AF59FBE7,
	ScoreManager_Awake_mB6C30C958421EED1082D2D5B24532F2548DB4575,
	ScoreManager_StartGame_mA0E1A60BD40B6F9B1636A574AD66891466FB232F,
	ScoreManager_Update_m77B6F3EB450BA678FBB01E50C4A6C6A611FB20CD,
	ScoreManager_GetScore_m4A0725CB7BD2000328375DCD5E4099B2B8A3E654,
	ScoreManager_SetScore_m86322F173E436E8E067EC97641031911830FD662,
	ScoreManager_SetMaxScore_mFA4CF7AB6743F612C045332691C0704E902B68E3,
	ScoreManager__ctor_m638A240D34643E8AB9D17553622C1C9354348354,
	PlayerController_Awake_m23059564DCFDD324EA9DB966473251896647CD23,
	PlayerController_Start_m9531F30EC892BDD1758A2EEC724E86EFBDA150A3,
	PlayerController_Update_mB31159CAD7DD2329859472554BC9154A83D8E794,
	PlayerController_Move_mB16F20D8A54197A9CB7648A10810E68038A49E5D,
	PlayerController_Open_m1954927782A81DB40FA112A00DA15928774CF006,
	PlayerController_OnAttack_m67BDF8BFE7DE03AA8E2925AB95F06099FDFF84D4,
	PlayerController__ctor_mF30385729DAFDFCB895C4939F6051DCE6C0327FB,
	U3COnAttackU3Ed__19__ctor_m8ED2C6F0F09070E28C887E3B3674DF0BFDA1DD47,
	U3COnAttackU3Ed__19_System_IDisposable_Dispose_m0ABC51B8B54F7C477F529A12297FED807D68756C,
	U3COnAttackU3Ed__19_MoveNext_mDF80D118A79DFD563D58515BD0568F7F239397EE,
	U3COnAttackU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9D07FF142DD9D1EE9D2D8A040A0D46E42E5B0DAE,
	U3COnAttackU3Ed__19_System_Collections_IEnumerator_Reset_mE9CBC89C7391490A6C0B004C604716171876D216,
	U3COnAttackU3Ed__19_System_Collections_IEnumerator_get_Current_mC6D03D6C21954931D3A3E5DF6931C21BCC0BECE3,
	ShelfCollision_OnTriggerStay_m78A12615A5C69B5DBD1ED87E9AC829476EB56788,
	ShelfCollision_OnTriggerExit_m55E7459D975F1A85EF5343F4209F2AE5FF2132C6,
	ShelfCollision__ctor_mCB8533299C1F9109D6917E3214F268781F00EC29,
	ShelfEight_OnTriggerStay_mECE58133E12EB1BC4228D4409056E58BFF451E54,
	ShelfEight__ctor_mAF2A42998B20B1C1C2608E8B457C3A43A398DFA8,
	ShelfEleven_OnTriggerStay_mF8876DF0CD0B2B4D49E07347C9186E42B68BDFAC,
	ShelfEleven__ctor_m939C02922DD535E95EA39A31851A4A93C699E014,
	ShelfFifteen_OnTriggerStay_m24663A23DE88C6F022E5D62DFB7D8BE82EFFC4D1,
	ShelfFifteen__ctor_mB6BC29B810F5421513D9F04949E98D3268A3BB82,
	ShelfFive_OnTriggerStay_m6C174C8F93C21DBCF376C1D9357A7F656260305B,
	ShelfFive__ctor_m8A353DB3C2C4BC2BA9D169304D7D2B36906ECF43,
	ShelfFour_OnTriggerStay_m3C6271ED40BD4926D25F9A6376DB061C84E79F7D,
	ShelfFour__ctor_m801C833742C75C73BFA6649B986D65691CE5CA16,
	ShelfFourteen_OnTriggerStay_m746E7A940DAA719A9FBB82D81A946A5BA3EBD6D9,
	ShelfFourteen__ctor_m629092A3BFE51102F20642759D5197EE6BCC00F3,
	ShelfNine_OnTriggerStay_mCD7EF24FBB89A1C9A598BEC69F5B698D7C480B18,
	ShelfNine__ctor_m49FC3343D03C95F2E7B3110790B16A1995214F18,
	ShelfOne_OnTriggerStay_m520B72EB4E8B4D365B5E1FB0C51D2AE30203299B,
	ShelfOne__ctor_m3468CBB39EF1EEE8A4D4FE81B9BFE04E607BF5A2,
	ShelfSeven_OnTriggerStay_m44C3320A171A6532DE7FC3FCB21B9E6C84E9A21C,
	ShelfSeven__ctor_m97C3EE5ECBD14EA8F2BE498F761CD31CDDA85CCE,
	ShelfSix_OnTriggerStay_mCCA0979BB5D75EE98813C838AF7D087D118E4363,
	ShelfSix__ctor_mA7DE59CA49E75FDD322919660ACD2855C8DD485F,
	ShelfSixteen_OnTriggerStay_m4D20310B8B1359B7B9E8F1845A1F6F630E44DE55,
	ShelfSixteen__ctor_mD6439E0C52E3CFEDE0E61C39924BF3B3FA1DE556,
	ShelfTen_OnTriggerStay_m1CE2DC557CE85F89C18A28A131B7DBCCC27F522C,
	ShelfTen__ctor_mFE5ECE1226FD2522C4A9F5BA64B351F699ABEB7E,
	ShelfThirteen_OnTriggerStay_mC3EE6864EA995CD18E58ADA6C3BE1407806D2DCC,
	ShelfThirteen__ctor_mD339173C52DDF0162B8DC96A06F1BD45778465A0,
	ShelfThree_OnTriggerStay_mCBB6E2745A8AFE90DC58AE53EE4BF1A5D2469408,
	ShelfThree__ctor_mF5A9652CD493CA681F9D36C7EC69B869E16B89CC,
	ShelfTwelve_OnTriggerStay_m735B4F25F3BBC3085456CD73AB1B77F828C7D9E3,
	ShelfTwelve__ctor_mFA5C038A0C9DD7C57B165A18D259E9A8718C63A1,
	ShelfTwo_OnTriggerStay_mEAB94B74D5BA8FB368B74F40910F64EFA4ADDF0F,
	ShelfTwo__ctor_m310164FAE1A602D696F9B6A1706F738A696F4284,
	ShelfZero_OnTriggerStay_mECAAF4F6F628635E7FECE04E26EB02ED79EAE405,
	ShelfZero__ctor_mD820D97114B157CAA84A2635CF155712AE95FD1F,
	HpUI_Init_m08B42491F8B27279FCEA2A636BEA05CD48135067,
	HpUI__ctor_m33CD6BA5039F9375343832DCF11553B5525888EF,
	UISceneEndGame__ctor_mC3DCFD5EBCF5F3F0165D91F3E926DCBBA37841B9,
	UISceneStartMenu_Awake_m05259D09056D15889E8D668550AEED9314683541,
	UISceneStartMenu_OnClickPlayButton_m6BC46660D332476CF54C8EC190D06D7C69613811,
	UISceneStartMenu_OnClickExitButton_m256DB69BB17641DD593B64E1F2BB5D4D3CF04936,
	UISceneStartMenu_OnClickJennieCharacterButton_m6278D0FA878EDD5375AF916AEFC78F8DDAC46289,
	UISceneStartMenu_OnClickJohnCharacterButton_m627720D6F19747DF7DEE512DE165A748BC50F487,
	UISceneStartMenu_LoadSceneGamePlay_m7FBA42BDC8A6A8CF5294F4B3106560AAAA1FCE41,
	UISceneStartMenu_ShowUI_m035094A6F0F42CB9ED9E721DBBC774B4785CB50D,
	UISceneStartMenu_HideUI_m68681BE6AC613864BC5BFA36CE036137D3D4E769,
	UISceneStartMenu_ShowButton_mCF9E727C083262420BD4E2A4C97A18F59CEBAAC4,
	UISceneStartMenu_HideButton_mF0F0817222D46FCDF1CD4DB9586502F18FE114B6,
	UISceneStartMenu__ctor_m6A6C15088E9B43997B66E1BE5C00D0988478D371,
	InputActions_get_asset_m0B72E38B19A55A117FBA6E926016FBB514C749C2,
	InputActions__ctor_mC7EBC76D7EB1E115FBA01B9CAF581AD999CC6EA7,
	InputActions_Dispose_mEA7CA175846E55B94546ECF1CCCADB433193E80A,
	InputActions_get_bindingMask_m73324E13801283558A623C2A7F7F02B734CB3F92,
	InputActions_set_bindingMask_mC25EBFCD84B0FC543E72F4F18B64512BB76EA090,
	InputActions_get_devices_mD16EE90C2E482B274DCAD86506CF7483BEAAC09A,
	InputActions_set_devices_m8B7F8D4F7165368CEDB339EC95F0641C8505A304,
	InputActions_get_controlSchemes_m41E467EEFEF963C20562A459B0072D0D665BB7E6,
	InputActions_Contains_m13DF17674AB027337D6F8DA0EBAC030A6AECCD93,
	InputActions_GetEnumerator_m270A505B580FCD59C89CB8B136954B18F23AADF6,
	InputActions_System_Collections_IEnumerable_GetEnumerator_mD6E8ECA793894FE233B2BEB69D4F96E3EACE355B,
	InputActions_Enable_mE34A744896308FEB2E057243D5C8BE5696F05A43,
	InputActions_Disable_mB8B2080BAE165B5A9C189F330EB306F19572C01F,
	InputActions_get_Player_mBC8DE2D352789433F5300F1C9B9F99BFE3903488,
	PlayerActions__ctor_m930E091485F40060E44C70D57942B72C8846D851,
	PlayerActions_get_Move_mA26DAF3D380CEAA2C8E380BFF6189FC1B9644889,
	PlayerActions_get_Attack_mD3B982834EECD17F87FF0B3ABD57015297CAF078,
	PlayerActions_get_Look_m37DE82164F8D5796D0EC565A956819BE4B93F707,
	PlayerActions_get_Open_m16D8BFDB470D6DDFC00A68881E15F23B44039D35,
	PlayerActions_Get_mCFD389B69ACE65D5F30FC407BD927CC40428584A,
	PlayerActions_Enable_mA1D8C3F578776B79B01FFFC610FBAF4A1354714D,
	PlayerActions_Disable_m960BE16B364ED50D91824CF19C69C92CB5C71F89,
	PlayerActions_get_enabled_m08BF48AFE9EC8BC74F261D2FF1610B5D5B13AA27,
	PlayerActions_op_Implicit_m6E473469D8A6C3BE74FE2CE5685F8567154D7D2F,
	PlayerActions_SetCallbacks_mF731117D76FCFD8E4B59E2C402524B39759E295B,
	NULL,
	NULL,
	NULL,
	NULL,
	SafeAreaSetter_Start_m83780A11588277507500E26788A2EA69E09FF392,
	SafeAreaSetter_ApplySafeArea_m4F4F38103E2C3689B92117BF87F0E7559B48CB25,
	SafeAreaSetter_Update_mF64C75E3889BDD924F5886A3AFF934BC395A5E01,
	SafeAreaSetter__ctor_mA93E55DED3B5E17188E4D3CC093573F5E1BC696B,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	TestScen_Update_m9A37DFD1020B565707A634E6E5E19412746BE78D,
	TestScen_T_m0C9D744C5CCFF74F6A82384E9D8628EDDB796C2C,
	TestScen__ctor_m6B34D87455D8955B1405782313F317332DB5B933,
	ChatController_OnEnable_mBC3BFC05A1D47069DFA58A4F0E39CFF8FAD44FF2,
	ChatController_OnDisable_m268A7488A3FDEB850FC3BC91A0BBDA767D42876B,
	ChatController_AddToChatOutput_m43856EEA133E04C24701A2616E7F10D7FFA68371,
	ChatController__ctor_m3B66A5F749B457D865E8BDA1DE481C8CF1158026,
	DropdownSample_OnButtonClick_m97641451A2EAC4FC6682897A948A37FDD6BF9EA6,
	DropdownSample__ctor_m87664E3DC75A1A9AA19FEE6ED6B993AB1E50E2B9,
	EnvMapAnimator_Awake_mFFC04BC5320F83CA8C45FF36A11BF43AC17C6B93,
	EnvMapAnimator_Start_mC0D348CAB0F0DC920EF3D3A008688B533F66D1BE,
	EnvMapAnimator__ctor_mC151D673A394E2E7CEA8774C4004985028C5C3EC,
	U3CStartU3Ed__4__ctor_m4D65F4FC2207AE4B6BE963AF9B5EDC55C7E29B23,
	U3CStartU3Ed__4_System_IDisposable_Dispose_mFEE2ACED70A3D825988E28CC61FEF8DCD7660A5B,
	U3CStartU3Ed__4_MoveNext_m2F1A8053A32AD86DA80F86391EC32EDC1C396AEE,
	U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF03E2A57659B8F598AC450183F55D43F903C0A1E,
	U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m268BA538CF6812C56EB281C0CE29D5AA2E9A2CAB,
	U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_mFE9CA9F52F9AFA1E4C9DBADA2064F854D6931CFF,
	TMP_DigitValidator_Validate_m5D303EB8CD6E9E7D526D633BA1021884051FE226,
	TMP_DigitValidator__ctor_m1E838567EF38170662F1BAF52A847CC2C258333E,
	TMP_PhoneNumberValidator_Validate_mF0E90A277E9E91BC213DD02DC60088D03C9436B1,
	TMP_PhoneNumberValidator__ctor_mB3C36CAAE3B52554C44A1D19194F0176B5A8EED3,
	TMP_TextEventHandler_get_onCharacterSelection_m90C39320C726E8E542D91F4BBD690697349F5385,
	TMP_TextEventHandler_set_onCharacterSelection_m5ED7658EB101C6740A921FA150DE18C443BDA0C4,
	TMP_TextEventHandler_get_onSpriteSelection_m0E645AE1DFE19B011A3319474D0CF0DA612C8B7B,
	TMP_TextEventHandler_set_onSpriteSelection_m4AFC6772C3357218956A5D33B3CD19F3AAF39788,
	TMP_TextEventHandler_get_onWordSelection_mA42B89A37810FB659FCFA8539339A3BB8037203A,
	TMP_TextEventHandler_set_onWordSelection_m4A839FAFFABAFECD073B82BA8826E1CD033C0076,
	TMP_TextEventHandler_get_onLineSelection_mB701B6C713AD4EFC61E1B30A564EE54ADE31F58D,
	TMP_TextEventHandler_set_onLineSelection_m0B2337598350E51D0A17B8FCB3AAA533F312F3DA,
	TMP_TextEventHandler_get_onLinkSelection_mF5C3875D661F5B1E3712566FE16D332EA37D15BB,
	TMP_TextEventHandler_set_onLinkSelection_m200566EDEB2C9299647F3EEAC588B51818D360A5,
	TMP_TextEventHandler_Awake_m43EB03A4A6776A624F79457EC49E78E7B5BA1C70,
	TMP_TextEventHandler_LateUpdate_mE1D989C40DA8E54E116E3C60217DFCAADD6FDE11,
	TMP_TextEventHandler_OnPointerEnter_m8FC88F25858B24CE68BE80C727A3F0227A8EE5AC,
	TMP_TextEventHandler_OnPointerExit_mBB2A74D55741F631A678A5D6997D40247FE75D44,
	TMP_TextEventHandler_SendOnCharacterSelection_m78983D3590F1B0C242BEAB0A11FDBDABDD1814EC,
	TMP_TextEventHandler_SendOnSpriteSelection_m10C257A74F121B95E7077F7E488FBC52380A6C53,
	TMP_TextEventHandler_SendOnWordSelection_m95A4E5A00E339E5B8BA9AA63B98DB3E81C8B8F66,
	TMP_TextEventHandler_SendOnLineSelection_mE85BA6ECE1188B666FD36839B8970C3E0130BC43,
	TMP_TextEventHandler_SendOnLinkSelection_m299E6620DFD835C8258A3F48B4EA076C304E9B77,
	TMP_TextEventHandler__ctor_m6345DC1CEA2E4209E928AD1E61C3ACA4227DD9B8,
	CharacterSelectionEvent__ctor_m06BC183AF31BA4A2055A44514BC3FF0539DD04C7,
	SpriteSelectionEvent__ctor_m0F760052E9A5AF44A7AF7AC006CB4B24809590F2,
	WordSelectionEvent__ctor_m106CDEB17C520C9D20CE7120DE6BBBDEDB48886C,
	LineSelectionEvent__ctor_m5D735FDA9B71B9147C6F791B331498F145D75018,
	LinkSelectionEvent__ctor_m7AB7977D0D0F8883C5A98DD6BB2D390BC3CAB8E0,
	Benchmark01_Start_mE7E5146B0D8D926CC410CAA48F3B617A0A7D955C,
	Benchmark01__ctor_mB92568AA7A9E13B92315B6270FCA23584A7D0F7F,
	U3CStartU3Ed__10__ctor_mBE5D8B4B98C372BD6DC3936437999DF3048DE3AB,
	U3CStartU3Ed__10_System_IDisposable_Dispose_m1F4180A7FDAE9AC5F11F30B67F813B3D7CD56D73,
	U3CStartU3Ed__10_MoveNext_mF3B1D38CD37FD5187C3192141DB380747C66AD3C,
	U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m22B2975D42326E3DE437174BBE4F0E8790CB6591,
	U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m514DA545ECA10AA62BFC239BB582FF0017B91D2B,
	U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_m6904A738F04118BA24E473F1809B3D69E711627E,
	Benchmark01_UGUI_Start_m2F8F9E1798943DA8086A7C7E73BA9D7C67482BD4,
	Benchmark01_UGUI__ctor_mACFE8D997EAB50FDBD7F671C1A25A892D9F78376,
	U3CStartU3Ed__10__ctor_m653B757B49A674E239C804FFF4EDEF325B5DB651,
	U3CStartU3Ed__10_System_IDisposable_Dispose_mAD0BC985E1E01C8369A7A955A60D3E545B0B3561,
	U3CStartU3Ed__10_MoveNext_mF9B2C8D290035BC9A79C4347772B5B7B9D404DE1,
	U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF438D42D7C5A811FF9A283F8780F015FF55826CD,
	U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m65545EB5B6DDDDAB5ACF92600137C1C754B322BA,
	U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_mD4523BE1D1014AAF1C0709EB767C0B0F47D739D2,
	Benchmark02_Start_m315C0DF3A9AC66B4F5802FDAED056E7E5F3D2046,
	Benchmark02__ctor_m526B38D3B7E75905208E4E57B168D8AC1900F4E5,
	Benchmark03_Awake_mD23429C1EE428EEF4ED9A753385BF194BAA41A77,
	Benchmark03_Start_m19E638DED29CB7F96362F3C346DC47217C4AF516,
	Benchmark03__ctor_m89F9102259BE1694EC418E9023DC563F3999B0BE,
	Benchmark04_Start_mEABD467C12547066E33359FDC433E8B5BAD43DB9,
	Benchmark04__ctor_m5015375E0C8D19060CB5DE14CBF4AC02DDD70E7C,
	CameraController_Awake_m420393892377B9703EC97764B34E32890FC5283E,
	CameraController_Start_m32D90EE7232BE6DE08D224F824F8EB9571655610,
	CameraController_LateUpdate_m6D81DEBA4E8B443CF2AD8288F0E76E3A6B3B5373,
	CameraController_GetPlayerInput_m6695FE20CFC691585A6AC279EDB338EC9DD13FE3,
	CameraController__ctor_m09187FB27B590118043D4DC7B89B93164124C124,
	ObjectSpin_Awake_mC1EB9630B6D3BAE645D3DD79C264F71F7B18A1AA,
	ObjectSpin_Update_mD39DCBA0789DC0116037C442F0BA1EE6752E36D3,
	ObjectSpin__ctor_m1827B9648659746252026432DFED907AEC6007FC,
	ShaderPropAnimator_Awake_mE04C66A41CA53AB73733E7D2CCD21B30A360C5A8,
	ShaderPropAnimator_Start_mC5AC59C59AF2F71E0CF65F11ACD787488140EDD2,
	ShaderPropAnimator_AnimateProperties_mAF49CD157AD41377CE00AA10F0C06C8BF5AA0469,
	ShaderPropAnimator__ctor_mAA626BC8AEEB00C5AE362FE8690D3F2200CE6E64,
	U3CAnimatePropertiesU3Ed__6__ctor_mFAC0F8A7368D9D35AD2780C118E13414DA79B56A,
	U3CAnimatePropertiesU3Ed__6_System_IDisposable_Dispose_m628EECBFCBC49087298185F17BC2AE7A73FC8A7B,
	U3CAnimatePropertiesU3Ed__6_MoveNext_m8A58CCFDAE59F55AB1BB2C103800886E62C807CF,
	U3CAnimatePropertiesU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0742A0FA62C518EBD119ED5FEBF849F918E12390,
	U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_Reset_mC076F281DD17668D6CE42EB04EA974DE1FFB3F6B,
	U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_get_Current_mA585E786DA86D2589EBCA029AD9E64E8B5135362,
	SimpleScript_Start_m75CD9CEDCAFA9A991753478D7C28407C7329FB8F,
	SimpleScript_Update_mE8C3930DCC1767C2DF59B622FABD188E6FB91BE5,
	SimpleScript__ctor_mEB370A69544227EF04C48C604A28502ADAA73697,
	SkewTextExample_Awake_m6FBA7E7DC9AFDD3099F0D0B9CDE91574551105DB,
	SkewTextExample_Start_m536F82F3A5D229332694688C59F396E07F88153F,
	SkewTextExample_CopyAnimationCurve_m555177255F5828DBC7E677ED06F7EFFC052886B3,
	SkewTextExample_WarpText_m0E0C46988600673F0E5DFA3133534DC6CA5950D3,
	SkewTextExample__ctor_m945059906734DD38CF860CD32B3E07635D0E1F86,
	U3CWarpTextU3Ed__7__ctor_mEAD3C39209B75514446A44B6C2FA76F8097EBD6F,
	U3CWarpTextU3Ed__7_System_IDisposable_Dispose_m32AA7120BE15547799BDC515FA3486488952BDCF,
	U3CWarpTextU3Ed__7_MoveNext_mB832E4A3DFDDFECC460A510BBC664F218B3D7FCF,
	U3CWarpTextU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3CF506B740C16FEDE58B6E1BE4556CAC2C6AEAD1,
	U3CWarpTextU3Ed__7_System_Collections_IEnumerator_Reset_mDDCDBB794F21DF70178A75747D8D8398F38A9C2F,
	U3CWarpTextU3Ed__7_System_Collections_IEnumerator_get_Current_m6D89CF1910F76577E23C924B535E93FD1B01CF0A,
	TMP_ExampleScript_01_Awake_m381EF5B50E1D012B8CA1883DEFF604EEAE6D95F4,
	TMP_ExampleScript_01_Update_m31611F22A608784F164A24444195B33B714567FB,
	TMP_ExampleScript_01__ctor_m3641F2E0D25B6666CE77773A4A493C4C4D3D3A12,
	TMP_FrameRateCounter_Awake_m958B668086DB4A38D9C56E3F8C2DCCB6FF11FAA3,
	TMP_FrameRateCounter_Start_mC642CA714D0FB8482D2AC6192D976DA179EE3720,
	TMP_FrameRateCounter_Update_m5E41B55573D86C3D345BFE11C489B00229BCEE21,
	TMP_FrameRateCounter_Set_FrameCounter_Position_mA72ECDE464E3290E4F533491FC8113B8D4068BE1,
	TMP_FrameRateCounter__ctor_mC79D5BF3FAE2BB7D22D9955A75E3483725BA619C,
	TMP_TextEventCheck_OnEnable_mE6D5125EEE0720E6F414978A496066E7CF1592DF,
	TMP_TextEventCheck_OnDisable_m94F087C259890C48D4F5464ABA4CD1D0E5863A9A,
	TMP_TextEventCheck_OnCharacterSelection_mDA044F0808D61A99CDA374075943CEB1C92C253D,
	TMP_TextEventCheck_OnSpriteSelection_mE8FFA550F38F5447CA37205698A30A41ADE901E0,
	TMP_TextEventCheck_OnWordSelection_m6037166D18A938678A2B06F28A5DCB3E09FAC61B,
	TMP_TextEventCheck_OnLineSelection_mD2BAA6C8ABD61F0442A40C5448FA7774D782C363,
	TMP_TextEventCheck_OnLinkSelection_m184B11D5E35AC4EA7CDCE3340AB9FEE3C14BF41C,
	TMP_TextEventCheck__ctor_mA5E82EE7CE8F8836FC6CF3823CBC7356005B898B,
	TMP_TextInfoDebugTool__ctor_mF6AA30660FBD4CE708B6147833853498993CB9EE,
	TMP_TextSelector_A_Awake_mD7252A5075E30E3BF9BEF4353F7AA2A9203FC943,
	TMP_TextSelector_A_LateUpdate_mDBBC09726332EDDEAF7C30AB6C08FB33261F79FD,
	TMP_TextSelector_A_OnPointerEnter_mC19B85FB5B4E6EB47832C39F0115A513B85060D0,
	TMP_TextSelector_A_OnPointerExit_mE51C850F54B18A5C96E3BE015DFBB0F079E5AE66,
	TMP_TextSelector_A__ctor_mEB83D3952B32CEE9A871EC60E3AE9B79048302BF,
	TMP_TextSelector_B_Awake_m7E413A43C54DF9E0FE70C4E69FC682391B47205A,
	TMP_TextSelector_B_OnEnable_m0828D13E2D407B90038442228D54FB0D7B3D29FB,
	TMP_TextSelector_B_OnDisable_m2B559A85B52C8CAFC7350CC7B4F8E5BC773EF781,
	TMP_TextSelector_B_ON_TEXT_CHANGED_m1597DBE7C7EBE7CFA4DC395897A4779387B59910,
	TMP_TextSelector_B_LateUpdate_m8BA10E368C7F3483E9EC05589BBE45D7EFC75691,
	TMP_TextSelector_B_OnPointerEnter_m0A0064632E4C0E0ADCD4358AD5BC168BFB74AC4D,
	TMP_TextSelector_B_OnPointerExit_m667659102B5B17FBAF56593B7034E5EC1C48D43F,
	TMP_TextSelector_B_OnPointerClick_m8DB2D22EE2F4D3965115791C81F985D82021469F,
	TMP_TextSelector_B_OnPointerUp_m303500BE309B56F3ADCE8B461CEC50C8D5ED70BC,
	TMP_TextSelector_B_RestoreCachedVertexAttributes_m3D637CF2C6CA663922EBE56FFD672BE582E9F1F2,
	TMP_TextSelector_B__ctor_mE654F9F1570570C4BACDA79640B8DB3033D91C33,
	TMP_UiFrameRateCounter_Awake_m83DBE22B6CC551BE5E385048B92067679716179E,
	TMP_UiFrameRateCounter_Start_m59DA342A492C9EF8AD5C4512753211BF3796C944,
	TMP_UiFrameRateCounter_Update_m28FC233C475AA15A3BE399BF9345977089997749,
	TMP_UiFrameRateCounter_Set_FrameCounter_Position_m2025933902F312C0EC4B6A864196A8BA8D545647,
	TMP_UiFrameRateCounter__ctor_m5774BF4B9389770FC34991095557B24417600F84,
	TMPro_InstructionOverlay_Awake_m2444EA8749D72BCEA4DC30A328E3745AB19062EC,
	TMPro_InstructionOverlay_Set_FrameCounter_Position_m62B897E4DB2D6B1936833EC54D9C246D68D50EEB,
	TMPro_InstructionOverlay__ctor_m7AB5B851B4BFB07547E460D6B7B9D969DEB4A7CF,
	TeleType_Awake_m099FCB202F2A8299B133DC56ECB9D01A16C08DFE,
	TeleType_Start_m72DE9DE597F4FA0B1CA02115CBC1E74EB53F63F7,
	TeleType__ctor_m12A79B34F66CBFDCA8F582F0689D1731586B90AF,
	U3CStartU3Ed__4__ctor_mF3575DBEBF4F153F6A899AF3362940298C11B629,
	U3CStartU3Ed__4_System_IDisposable_Dispose_m6DABFDBC2A313BF6DD90AA1C43B4EF9D6249CBE9,
	U3CStartU3Ed__4_MoveNext_mEF7A3215376BDFB52C3DA9D26FF0459074E89715,
	U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m66DC06D52865D30DAA78DBD70B6554D13E2EA70B,
	U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_mE854A2CECA0E01F3C2D90DA4F720CF0F387994F8,
	U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_m77D3936B94684C817EE9F6D2C903238A457D5261,
	TextConsoleSimulator_Awake_mA51EE182A528CCA5CAEA8DBE8AD30E43FDFAE7B0,
	TextConsoleSimulator_Start_m429701BE4C9A52AA6B257172A4D85D58E091E7DA,
	TextConsoleSimulator_OnEnable_m3397FBCDA8D9DA264D2149288BE4DCF63368AB50,
	TextConsoleSimulator_OnDisable_m43DF869E864789E215873192ECFCDC51ABA79712,
	TextConsoleSimulator_ON_TEXT_CHANGED_m51F06EE5DD9B32FEFB529743F209C6E6C41BE3B8,
	TextConsoleSimulator_RevealCharacters_mE8E415644F7BD2056D0809F7BB0FA9A2F9FE8534,
	TextConsoleSimulator_RevealWords_mFDBC863D30BC63ADCF2860F303AF252E27D0F4F4,
	TextConsoleSimulator__ctor_m4719FB9D4D89F37234757D93876DF0193E8E2848,
	U3CRevealCharactersU3Ed__7__ctor_mD45A85F5F50909F70C80AC8CE460F4FD261CDE9D,
	U3CRevealCharactersU3Ed__7_System_IDisposable_Dispose_m85F270FDC11A4D79E9CF47AADC9FA1FBF032F86C,
	U3CRevealCharactersU3Ed__7_MoveNext_mA9A6555E50889A7AA73F20749F25989165023DE3,
	U3CRevealCharactersU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5BBAE6868EB7F1C11BE5DF001641E18C9D625F83,
	U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_Reset_mA9F4893381AB24E01EAEBFC38A88AF363A9A0691,
	U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_get_Current_mFD0B7538B1A650FB389FFE9296B0E51AEA5B6B6F,
	U3CRevealWordsU3Ed__8__ctor_m22FF9E770988107A928C5D1EA639F60239BFFEF0,
	U3CRevealWordsU3Ed__8_System_IDisposable_Dispose_m3EAF6EF4A8C99A71FEED251BF3F28A26BF6AD7F8,
	U3CRevealWordsU3Ed__8_MoveNext_m3811753E2384D4CBBAD6BD712EBA4FAF00D73210,
	U3CRevealWordsU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA3F93E4AA532F15D52D68B7121805C014AB2D7FB,
	U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_Reset_mCDC8982948ED5F7743567569CA1D4A354218714F,
	U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_get_Current_mF39E492D576810F58DB9031556CFD6806FD32E27,
	TextMeshProFloatingText_Awake_m679E597FF18E192C1FBD263D0E5ECEA392412046,
	TextMeshProFloatingText_Start_m68E511DEEDA883FE0F0999B329451F3A8A7269B1,
	TextMeshProFloatingText_DisplayTextMeshProFloatingText_m3C381B8A53C58CF001D7A9212B8BDA6F368CC2C7,
	TextMeshProFloatingText_DisplayTextMeshFloatingText_m22691E6EA41B7FCF782B03954865D8E3B890E4DF,
	TextMeshProFloatingText__ctor_m968E2691E21A93C010CF8205BC3666ADF712457E,
	TextMeshProFloatingText__cctor_m5FF3949FBDB0FD13908FB7349F64DDB3012512B8,
	U3CDisplayTextMeshProFloatingTextU3Ed__15__ctor_m3F7F9561735FF8403090A93E708E781B3997B81B,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_System_IDisposable_Dispose_m6F24E1DC5777D4682375D8742BC9272EE6D4EA2C,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_MoveNext_m04B1BB9074CBA76BA7F63849721F4CAEA1045A28,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m14A7C9DEBD0FF0C7A6979AFEC78B0BEE9E4C3963,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_IEnumerator_Reset_mC0D5548A92F57550F60EE76E17B58125B57B94A8,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_IEnumerator_get_Current_mD3CBFD6F8558EF09F2B4A3D8D1B76BD7FDB3A98B,
	U3CDisplayTextMeshFloatingTextU3Ed__16__ctor_m0466586E44A01852547C6CECCA91C14673B98C20,
	U3CDisplayTextMeshFloatingTextU3Ed__16_System_IDisposable_Dispose_m8730A3A2EFBF759A51F0E5330C3BBF8EFC1736A3,
	U3CDisplayTextMeshFloatingTextU3Ed__16_MoveNext_m93E19CF96C4EBC57CC4E03E2D3D0B56B3E92A36A,
	U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA4076BE73343A473EFE46E73ABA5DE66CC797598,
	U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_IEnumerator_Reset_mC33B355BB392C5A52DCA0AAF2588E1E78AF6D3A5,
	U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_IEnumerator_get_Current_m28CCE2E51295AD5F368C504BADDDFF008C2CC0A6,
	TextMeshSpawner_Awake_mDF8CCC9C6B7380D6FA027263CDC3BC00EF75AEFB,
	TextMeshSpawner_Start_m7CC21883CA786A846A44921D2E37C201C25439BA,
	TextMeshSpawner__ctor_m1255777673BE591F62B4FC55EB999DBD7A7CB5A1,
	VertexColorCycler_Awake_m84B4548078500DA811F3ADFF66186373BE8EDABC,
	VertexColorCycler_Start_mC3FF90808EDD6A02F08375E909254778D5268B66,
	VertexColorCycler_AnimateVertexColors_m6960F777E4876DFCA726BCAE7A8163850D58FA42,
	VertexColorCycler__ctor_m09990A8066C8A957A96CDBEBDA980399283B45E9,
	U3CAnimateVertexColorsU3Ed__3__ctor_m0038B1054BCC928D35F8C0021ED7D2E1C533E35F,
	U3CAnimateVertexColorsU3Ed__3_System_IDisposable_Dispose_m5BAD394A0B09B3E0FF19E91521E02C2B3ADD6007,
	U3CAnimateVertexColorsU3Ed__3_MoveNext_m2842AF5B12AFC17112D1AE75E46AB1B12776D2A6,
	U3CAnimateVertexColorsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAE810D7968957A09C88E61C29DAAEC68E4AF1E51,
	U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_Reset_m72ABAC50E9E4D972FB44CAFF387F3E23FEC5D932,
	U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_get_Current_m8DE595A1D01F3A507A356F8BCE020D0851412B52,
	VertexJitter_Awake_m9C5586A35BD9C928455D6479C93C1CE095447D8F,
	VertexJitter_OnEnable_m97ED60DBD350C72D1436ADFB8009A6F33A78C825,
	VertexJitter_OnDisable_mF8D32D6E02E41A73C3E958FB6EE5D1D659D2A846,
	VertexJitter_Start_m8A0FED7ED16F90DBF287E09BFCBD7B26E07DBF97,
	VertexJitter_ON_TEXT_CHANGED_mC7AB0113F6823D4FF415A096314B55D9C1BE9549,
	VertexJitter_AnimateVertexColors_mECAE037FC0CBA52CAC71C0B61E88829FF18BCC16,
	VertexJitter__ctor_m550C9169D6FCD6F60D6AABCB8B4955DF58A12DCE,
	U3CAnimateVertexColorsU3Ed__11__ctor_m0222C3457F5ACA497FE3A8EC829DE4AD11A169F8,
	U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m717E79A39A8161ADDA9E62F7CDFB67B8F2D65099,
	U3CAnimateVertexColorsU3Ed__11_MoveNext_m169A75C7E2147372CB933520B670AF77907C1C6B,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m920EBA43D59A1A88E27FED92CF0AC0DF90179479,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_m6C045A6DD0B60F1512457448E615877EAB86D75D,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m017345FE58B497EAFC9D0CB1FB733F76EB3449AF,
	VertexShakeA_Awake_m005C6F9EE8A8FD816CD3A736D6AF199CD2B615A2,
	VertexShakeA_OnEnable_m9F60F3951A7D0DF4201A0409F0ADC05243D324EA,
	VertexShakeA_OnDisable_m59B86895C03B278B2E634A7C68CC942344A8D2D2,
	VertexShakeA_Start_m3E623C28F873F54F4AC7579433C7C50B2A3319DE,
	VertexShakeA_ON_TEXT_CHANGED_m7AD31F3239351E0916E4D02148F46A80DC148D7E,
	VertexShakeA_AnimateVertexColors_mC451E2732A4E3E90E2553811AD75903EEF974599,
	VertexShakeA__ctor_m7E6FD1700BD08616532AF22B3B489A18B88DFB62,
	U3CAnimateVertexColorsU3Ed__11__ctor_m92612416BEC0EBF5E9849FB603629C0F2F95FEF2,
	U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m77D966994D4717EAFD8EFE169F3E8A4EE8B05B81,
	U3CAnimateVertexColorsU3Ed__11_MoveNext_mCEEDE03667D329386BB4AE7B8252B7A9B54F443F,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDCCD3645ACF9B18D760B341C863F853996FA9BCE,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_mB9444B5E58B4E97105C447F547C0F74C51BCFBFA,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m5CC464185D5C0251C6206E20AFFA681BA0525A7E,
	VertexShakeB_Awake_m99C9A0474DBFE462DC88C898F958C1805376F9D5,
	VertexShakeB_OnEnable_m64299258797D745CDFE7F3CBEC4708BDBC7C3971,
	VertexShakeB_OnDisable_m07D520A8D7BCD8D188CE9F5CC7845F47D5AD6EF4,
	VertexShakeB_Start_m9642281210FA5F701A324B78850331E4638B2DD1,
	VertexShakeB_ON_TEXT_CHANGED_m5650D69D258D0EEF35AF390D6D5086B327B308A5,
	VertexShakeB_AnimateVertexColors_mDF7A7E7028361D288DF588D9301541E4FA1EFA87,
	VertexShakeB__ctor_m605A778C36B506B5763A1AE17B01F8DCCBCD51EC,
	U3CAnimateVertexColorsU3Ed__10__ctor_m3E3B1D286DEBED2BC028AD490308568B930C3760,
	U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_m0F9D4B2A6ED0500C2120DBA29932CC279E8908DC,
	U3CAnimateVertexColorsU3Ed__10_MoveNext_m0534E436514E663BF024364E524EE5716FF15C8E,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF2A8BAFC0261ACBBE8EEA74DE4B498D30C68AE3D,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_m5F12D7C6F2523BEB6326FE49AE972116D6157CBB,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_mB9C12786011A3B78517AEFAE8D5A78B95A4219AD,
	VertexZoom_Awake_m1B5D386B98CF2EB05A8155B238D6F6E8275D181C,
	VertexZoom_OnEnable_m7F980FC038FC2534C428A5FD33E3E13AEAEB4EEC,
	VertexZoom_OnDisable_m880C38B62B4BB05AF49F12F75B83FFA2F0519884,
	VertexZoom_Start_m10A182DCEF8D430DADAFBFFA3F04171F8E60C84C,
	VertexZoom_ON_TEXT_CHANGED_mB696E443C61D2212AC93A7615AA58106DD25250F,
	VertexZoom_AnimateVertexColors_mEFBA6C940DFECB485236C247358A32011A7963ED,
	VertexZoom__ctor_mE7B36F2D3EC8EF397AB0AD7A19E988C06927A3B2,
	U3CU3Ec__DisplayClass10_0__ctor_m4EC5F8042B5AC645EA7D0913E50FD22144DBD348,
	U3CU3Ec__DisplayClass10_0_U3CAnimateVertexColorsU3Eb__0_m959A7E1D6162B5AAF28B953AFB04D7943BCAB107,
	U3CAnimateVertexColorsU3Ed__10__ctor_m537CF0A5ADF5BBC2DF784BF526E3F32EB528E1B2,
	U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_mF0CD944C393771101D2718A660E1DFF22385819F,
	U3CAnimateVertexColorsU3Ed__10_MoveNext_m8340EFEB2B2CF6EA1545CFB6967968CA171FEE66,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m093E922A5A046910B2A7EE826D804CA3064A7BD9,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_mDED63FD52741D58D8D5A3E53415F91B6560F680C,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_m933EBF7C4300C7C1E3EE60E4741110025877CA5F,
	WarpTextExample_Awake_mEDB072A485F3F37270FC736E1A201624D696B4B0,
	WarpTextExample_Start_m36448AEA494658D7C129C3B71BF3A64CC4DFEDC4,
	WarpTextExample_CopyAnimationCurve_m744026E638662DA48AC81ED21E0589E3E4E05FAB,
	WarpTextExample_WarpText_mA29C98CF3B92F253C2DD2BADFC76618F8324BEF2,
	WarpTextExample__ctor_m62299DFDB704027267321007E16DB87D93D0F099,
	U3CWarpTextU3Ed__8__ctor_m9FADE04C27A0034C5A276232FCA187AECDC6BF49,
	U3CWarpTextU3Ed__8_System_IDisposable_Dispose_m5AB0DCF4B3DE6C290487F3DBBC7BAE931130DE60,
	U3CWarpTextU3Ed__8_MoveNext_m9C83C8455FF8ADFBAA8D05958C3048612A96EB84,
	U3CWarpTextU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9D5776AF80C4227401ADD7E13D98F2530CB9E7A1,
	U3CWarpTextU3Ed__8_System_Collections_IEnumerator_Reset_m4F2FBBE7B375E6FC837736B4275A7B601A01F535,
	U3CWarpTextU3Ed__8_System_Collections_IEnumerator_get_Current_m01FA935D93D737C9DDD88A051F7DFC393DD7BD25,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	CharacterManager_Awake_m64BD9F18F150A8AF7824482418781BA5BFC05E1B,
	CharacterManager_StartGame_m207BD9A33CBFB94D3453A8B938485ECD96B40AAC,
	CharacterManager_SpawnJennieCharacter_m8D235ECBF5D7110B7C710CE441803DC66CC27756,
	CharacterManager_SpawnJohnCharacter_m4A0F0D0FF185839AA170C9F6AF567837BEF9903B,
	CharacterManager__ctor_m0EE2629BA31A11062677C9269765AED363DC3E86,
	UISceneGameplayManager_Awake_m08663A590601B8083C7500056FA11FEF0ED62D35,
	UISceneGameplayManager_StartGame_mCA6A43C2B79AFFD0FF17FA82E07A07316DAFAFED,
	UISceneGameplayManager_StartHpUI_mEB93FD04C48F01C6F85154B93C3C3B340E176085,
	UISceneGameplayManager_SetHpUI_m01F9FED0B054D3951D2D51520D92DF91DDC50A93,
	UISceneGameplayManager_SetTimeText_mD5EF64A13B41119541B94CB5DA690F256E323844,
	UISceneGameplayManager_SetScoreText_m1366CCE87678404C47A3EB403DC985F3DD994A4E,
	UISceneGameplayManager_SetCountBulletText_mACD756A45D7537DDFF7F4F55D9C41AC1B6D482FA,
	UISceneGameplayManager_ShowShelfZero_mB4B13060C15515ED626E29F9EFB1651B7934C723,
	UISceneGameplayManager_ShowShelfOne_m3DF518BBA1E2DA4BF8C999A57C1B63C14769C286,
	UISceneGameplayManager_ShowShelfTwo_mAB82ADA4F3BCCD52F3452AEA727FE2B8437C5BF3,
	UISceneGameplayManager_ShowShelfThree_m15CAD734C7D07428F87BC35CC919C4578D4912F7,
	UISceneGameplayManager_ShowShelfFour_m01A5C18CD4061313DA250E583BB51DC46B4CAA3C,
	UISceneGameplayManager_ShowShelfFive_m52268C0639EFAE04C1167AB392B1AE245C3267AB,
	UISceneGameplayManager_ShowShelfSix_m35E984986BC0530E82BE5C2617CD6F5CA4D0B0A6,
	UISceneGameplayManager_ShowShelfSeven_m65767C0894E6CB66DF8F1FB8C0BAC8EA25AEE72E,
	UISceneGameplayManager_ShowShelfEight_m148BB7456D3EBBED18387ECB455CC528E68AAFFF,
	UISceneGameplayManager_ShowShelfNine_m055AC838B88ED5C7814B459665FC68D510742454,
	UISceneGameplayManager_ShowShelfTen_m823DE8C2DEE44C0341F49089744DAC671F34EF7E,
	UISceneGameplayManager_ShowShelfEleven_m648290A30763B084D6BC4F26A41437BEEDD17EAF,
	UISceneGameplayManager_ShowShelfTwelve_m04EDDD419A63FE9C807AA893270F4FAE6C4532EB,
	UISceneGameplayManager_ShowShelfThirteen_m9AB41B40BB4860BCBFA2D4A3A9D3B7C7F8EF772B,
	UISceneGameplayManager_ShowShelfFourteen_m87610210A8BA39D2E665D96A19E4756466876941,
	UISceneGameplayManager_ShowShelfFifteen_mB06A49394038CAB11FE23FD084BE6E69E56DE5FA,
	UISceneGameplayManager_ShowShelfSixteen_m832B62B6FB087BD4298523FA727F42D7FEC98758,
	UISceneGameplayManager_ShowShelfPanel_m73EC5F6F86314DF1C1E59602010558C0D5C96DD4,
	UISceneGameplayManager_CloseShelfPanel_m35AC101B318800D1012FA2A4916247E0AE60C91D,
	UISceneGameplayManager_CloseOpenUI_m44F962B5CB3E8555D9A33187B86CBA846FC2DCB5,
	UISceneGameplayManager_ShowOpenUI_mFA2F75EB604003B66C437088E734828D375EC9DA,
	UISceneGameplayManager_CloseShelfUI_m611E31A59DAADC8FB3BEA6585D18D499EFA7C4DE,
	UISceneGameplayManager_ShowUI_m99F453028414EB91FB7FB84ED902E73155D7868D,
	UISceneGameplayManager_HideUI_m3EE1D5E43CC7D4507A1571FE8BF9AFB4E3B1D630,
	UISceneGameplayManager_ShowButton_m17FF4242F06641FBA15470DA6D388E44997EF578,
	UISceneGameplayManager_HideButton_m323DF5AD8C78D66761AAF6DCE70E99FDCCA0C6D9,
	UISceneGameplayManager__ctor_mF73463E65694FA081C2283F70AA7463C4688C443,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	BaseCharacter_get_Hp_m84512611F7685F0534B1446CFB16236CB55BECF1,
	BaseCharacter_set_Hp_mB638FB310531F8AC2FF4BC65E145964985C2EC3C,
	BaseCharacter_get_Speed_m0F6167443D284CE95AFA64907A6ACE9B886C0B2F,
	BaseCharacter_set_Speed_m6AD36AA734E58013D4842F9F14C3DBA5811DC049,
	BaseCharacter_get_Bullet_m4DB1BD77232B3C1DF7F2FC4F29472F68ED015BFA,
	BaseCharacter_set_Bullet_mB106B7A92BA4CAD0423A2A9601A378256301ADAC,
	BaseCharacter_Init_mBFEA03793CA90C41299282F9BA02A725FACF83E2,
	NULL,
	NULL,
	NULL,
	BaseCharacter__ctor_m578E57F40104D0E260E0D02460B0949B238109E7,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	PromiseCancelledException__ctor_mDA55F2F9C6DF87917C0A40BC149A437C750C66FD,
	PromiseCancelledException__ctor_m0733E2CEAEE71A6A1A22E96FFF62D85627437BF1,
	PredicateWait__ctor_mCD6942E554ED82AF36B3F07828C4E5BCA7F0D318,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	PromiseTimer_WaitFor_m99B243125D5686DC36844FA69D796651E12C9B76,
	PromiseTimer_WaitWhile_m8EF7C405289D3A2AE7F7185AE8E4E901BF504EC9,
	PromiseTimer_WaitUntil_m7AC7AE2AFC6CB5E43A789DE5A6E683D62D23FBFC,
	PromiseTimer_Cancel_m87F8524626DDC28FB1ECA1968F73A2D18540704E,
	PromiseTimer_FindInWaiting_mA1660E092E5065B151FB000488B2C87FF4C6C8F1,
	PromiseTimer_Update_m9C84E7E7B30B14A7C13510CC22AB6032C7C0874F,
	PromiseTimer_RemoveNode_mF32CC3AE04AE1AFBCEF4B8DA7DFD9E2AF02A6E89,
	PromiseTimer__ctor_m3F1298B21CD5644C3AE646911AD13B6FCCD19BE6,
	U3CU3Ec__DisplayClass3_0__ctor_mC51BCEA4436030F27897F20585D501ABF744C78E,
	U3CU3Ec__DisplayClass3_0_U3CWaitForU3Eb__0_m7EEE8458FA739B7F87A8EF705E05039C9EFB483D,
	U3CU3Ec__DisplayClass4_0__ctor_m8884F549B84F64B7E7D4B61E036A398416091313,
	U3CU3Ec__DisplayClass4_0_U3CWaitWhileU3Eb__0_m8DF4AC7205D6CD8FF4E8AE640F92F6AB4A9B59F7,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ExceptionEventArgs__ctor_m7023DA6E99C0B23D96D0AAB0500F16B19345A30C,
	ExceptionEventArgs_get_Exception_m79958EAB2CAA0E6C91E10902A3ABCC242012745A,
	ExceptionEventArgs_set_Exception_mBEFA8B8A6990A708691413D5E188FD73D6036A88,
	Promise_add_UnhandledException_m501ED8710D326733D0A4B095516E79AD93771A17,
	Promise_remove_UnhandledException_m0A7255398B93C672C3F4AD9AE8CC1260E6BDC493,
	Promise_GetPendingPromises_mCD78AA3FD69C8809D95E0FBBC6DC24DB648CD1E4,
	Promise_get_Id_m2A159B9C83B813983859B9F8497B92A636D26198,
	Promise_get_Name_m2681487A6A6188C86D6B1FEE6F4E42F0817F0AF1,
	Promise_set_Name_m04BD79E80089945F37642A501B2E0C621CFAADFF,
	Promise_get_CurState_mBE60E772D62CB2C8449BF67F3A2106C0E0FA720D,
	Promise_set_CurState_mC9014B9835DB4BDFE7B3E1AFC0B61A249DB0B148,
	Promise__ctor_m72891E8C449A8F0285F2BBEE6596EEE91D1B69D0,
	Promise__ctor_m0F170C315BB66058A0B8DB59D2FB2E269460DB4A,
	Promise_NextId_m91D574BF971140F5BCA824858A1F23785E625936,
	Promise_AddRejectHandler_m8A5C912F1C8D8FC752EAAFCC3397992F84DE2C83,
	Promise_AddResolveHandler_m39C28AC656C57C1865250657901C41569B49B87A,
	Promise_AddProgressHandler_mC3147E564182B45C04B4AD18F55200ADE09494A9,
	Promise_InvokeRejectHandler_mD002929AA239DA2F2D3CBA6B828BCC190E2FA010,
	Promise_InvokeResolveHandler_m8253411D16B16952735214A8D4840B69CBC19367,
	Promise_InvokeProgressHandler_m2767B570D7BE8E457C295823CFD48F4DF079AD1F,
	Promise_ClearHandlers_m77595F54867BFFB97E19A6354940D3C574E8AF30,
	Promise_InvokeRejectHandlers_m78C4A77BB7F1728BCF63373111A992940FD73FBA,
	Promise_InvokeResolveHandlers_m071F1B5884447CEE1F7D19154AB430522BE6C208,
	Promise_InvokeProgressHandlers_m33BBE047F8B32BDC15B943203CF4D653C10F5F3D,
	Promise_Reject_mA334C811DFA609A9294A15B8C4FE3D06CFE59E4B,
	Promise_Resolve_mB395CBF764AD56F64C953F240F5525C4F24AA176,
	Promise_ReportProgress_m7496AD24A305E551C317FAF83A339D50555EC2F0,
	Promise_Done_mBF4D40ECA5734EFE1213BD2AD11124841BCA6C63,
	Promise_Done_mA20B58D299DFC458330AB673679F52760076ED6D,
	Promise_Done_mB94F090BD0EDC421C98291DF2437788619416322,
	Promise_WithName_m9A8610E5066D79F98133FBDE0C427032A9EF04DB,
	Promise_Catch_m34A1A6F483209E344CEE20CAECA131319E25A35F,
	NULL,
	Promise_Then_m660D492D7CB977D2658FDC71CA16C5F1C7C80BFB,
	Promise_Then_mE35145837C7EBB840D64C24A92FFEC013AA4E52F,
	NULL,
	Promise_Then_m9C5987E6126BE35855402986BE5878DDDA5DDD2F,
	Promise_Then_m1F2DA120D93DB9D1801A4410BACE242992BFDE5D,
	NULL,
	Promise_Then_mE03C61503CE58EA2981EF96852E6F252B6F29736,
	Promise_Then_m6693CA7F4467B53237CE92F7F44C36CD44FABE0D,
	Promise_ActionHandlers_m226D1CB18140648C8EC32E3E2FA7DF0730478F26,
	Promise_ProgressHandlers_m21C8B94D38E0FBF1F14B24FCFFFEC214D9D80912,
	Promise_ThenAll_m444DBB4B6489921A44CDCF98AB8D3F2708372D66,
	NULL,
	Promise_All_m4021CBAF737AB75139BE836A21E02B28B238AD21,
	Promise_All_mD80850322247A01D346893F8962617690B9D2606,
	Promise_ThenSequence_mE42D6CD848060EB59C10345B5A14B16724D35F9B,
	Promise_Sequence_m9F9C078AA9DB0404DAB83831A3EF769379E58906,
	Promise_Sequence_m646E63C1583234FE0B9EC2F642D5B1EB7A830E52,
	Promise_ThenRace_mF04FB2EDFB9FF1589EF714F409994945E123EE7D,
	NULL,
	Promise_Race_m7A61FE78FC907244940AE23BE119699E657860BB,
	Promise_Race_m422B8D03C310B70EC818A1A5F7A8E12C836FD877,
	Promise_Resolved_m27D7349EFBB518D89093ADB573BD7D7F836B24DE,
	Promise_Rejected_mF6CA3689BE449406F16A5D145B1DD4FD13C41890,
	Promise_Finally_m762D2CD8F1F48788A1B94FB46CB338D69083230B,
	Promise_ContinueWith_m1A53BD142DC421621617AE8E0BE6DA2053D96D8A,
	NULL,
	Promise_Progress_m53E64BDA8764C5E88057601FAB54BF75FD0B368A,
	Promise_PropagateUnhandledException_m30B4923F2598FA6B4F7CD5964E00760B5D9F10B7,
	Promise__cctor_mB03C9B64D5DF3D552E96C4FCFA942B128173413D,
	Promise_U3CInvokeResolveHandlersU3Eb__35_0_mE4D9BB0CB11B33C2E7F12BFE44103CC546FD2E2A,
	Promise_U3CDoneU3Eb__40_0_m03D038B6097EEEF2B736CCE4A1D9154518BE3E01,
	Promise_U3CDoneU3Eb__41_0_mDE5A60CCBCC7F17ADD74A395C92F553EAAA52817,
	Promise_U3CDoneU3Eb__42_0_m10D5F4FC16A8A05494ED431EA28AA288DB13AE3B,
	U3CU3Ec__DisplayClass34_0__ctor_mF81EF54B39A864EB5E463DA04D72B5F8A0CF4DFC,
	U3CU3Ec__DisplayClass34_0_U3CInvokeRejectHandlersU3Eb__0_mC2CB7940F03A8D527C6C9BA7793BA6C6A07E13FC,
	U3CU3Ec__DisplayClass36_0__ctor_m18834E008AB1CCA3258C2388F1158B37AB6AFAC1,
	U3CU3Ec__DisplayClass36_0_U3CInvokeProgressHandlersU3Eb__0_mB466D8A8500579A62E0EF36D3766DB1A59E89F41,
	U3CU3Ec__DisplayClass44_0__ctor_mCB6B49A87238E9E939A3FB884973D5B8085A1805,
	U3CU3Ec__DisplayClass44_0_U3CCatchU3Eb__0_m59D9BB020E9BAA19BF3A55E1A66B4BBF0115A4F3,
	U3CU3Ec__DisplayClass44_0_U3CCatchU3Eb__1_mC2F86C6B2CC1463D07838F8B6E71CF95A00D6B1C,
	U3CU3Ec__DisplayClass44_0_U3CCatchU3Eb__2_m1C82A7D09BAFC515977C21BBAD3E8EE6E6C8B665,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	U3CU3Ec__DisplayClass52_0__ctor_mB894C314363743106885723C9A014E58D79394DE,
	U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__0_mFAB16B8D05F5B6D1B0820FEAECBB318B28D6F95F,
	U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__2_m68317E06CF2A6CB4E45914984E5E7B1C8E250B3A,
	U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__3_m1DD830DC1CA6AEAB53B32028949E3349E37146EE,
	U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__4_m6F52D5A374717AC944B68D544EF57498A6CF0C12,
	U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__1_mE79481476F323A2E8C64CF6853F48919B454B238,
	U3CU3Ec__DisplayClass53_0__ctor_m8AD8D6610D93664501D710EB55F2D75DE59813DC,
	U3CU3Ec__DisplayClass53_0_U3CThenU3Eb__0_m089AF19F35A75DF89A101BC920FA8F3CBF856F03,
	U3CU3Ec__DisplayClass53_0_U3CThenU3Eb__1_m6BF2C7A1D0A0D3699E5339E92DFF00CA7B60BBE2,
	U3CU3Ec__DisplayClass56_0__ctor_mA16A09395360FDB88DAEB794DAF2A6BAF4CAC8AE,
	U3CU3Ec__DisplayClass56_0_U3CThenAllU3Eb__0_m76809680DE80AD5F5596FA06F41BA163AC6D1C30,
	NULL,
	NULL,
	U3CU3Ec__DisplayClass59_0__ctor_mBA66AC86289688F1C468B96C50BE5358F31A082F,
	U3CU3Ec__DisplayClass59_0_U3CAllU3Eb__0_mA4CF65F285BDA1A6D803D917E9FD265051D6BEB9,
	U3CU3Ec__DisplayClass59_0_U3CAllU3Eb__3_m8253EC136EA78D646B69616B15FA235271A992AA,
	U3CU3Ec__DisplayClass59_1__ctor_m526DF9DF8BBEBFA28914FB4F54E3A1AF03B414BB,
	U3CU3Ec__DisplayClass59_1_U3CAllU3Eb__1_m593233FA219F817C7E2E48E4B57F1967B608EDAE,
	U3CU3Ec__DisplayClass59_1_U3CAllU3Eb__2_m5AC61534D5AE0A52E1F555A299AD9A673E2DFD45,
	U3CU3Ec__DisplayClass60_0__ctor_m99C72D8D0D1E5F3744F901AF2CE1D01B10074CD3,
	U3CU3Ec__DisplayClass60_0_U3CThenSequenceU3Eb__0_m6FC48A06C04839828FBCC417B074F35B1849CBF1,
	U3CU3Ec__DisplayClass62_0__ctor_m4E600EF746E7384F9FF81A106144CCC5CA295EB6,
	U3CU3Ec__DisplayClass62_0_U3CSequenceU3Eb__0_m3BD1BDD0E7EEF8F00E0C7DE8CD8AA502555164A8,
	U3CU3Ec__DisplayClass62_0_U3CSequenceU3Eb__1_m030831A862679BA67E0AFFF2A793684303FDC197,
	U3CU3Ec__DisplayClass62_1__ctor_m4E3A09413376A37B8429009826FB5E58FB5918D7,
	U3CU3Ec__DisplayClass62_1_U3CSequenceU3Eb__2_m38F09C7AB72B5AAAEE2B1F47D8B8E46A8B244D76,
	U3CU3Ec__DisplayClass62_1_U3CSequenceU3Eb__3_m45E00529CA4F236640E37AD28227E06C03260F75,
	U3CU3Ec__DisplayClass63_0__ctor_mA3AE9325599E7C269CF28464DFD338E2FB36D87B,
	U3CU3Ec__DisplayClass63_0_U3CThenRaceU3Eb__0_m5B8C751B1FAD053AE58DE02DF3FEEA12AE43EC44,
	NULL,
	NULL,
	U3CU3Ec__DisplayClass66_0__ctor_m20042739F9915BDA4DCECD38A6FAEC9A32FC9FD4,
	U3CU3Ec__DisplayClass66_0_U3CRaceU3Eb__0_m3C1812F10C134DFFDE20266483FB8CEE43E00D28,
	U3CU3Ec__DisplayClass66_0_U3CRaceU3Eb__2_mD8A7C646663CD58D24E0B9E5AD99A223042BCC7B,
	U3CU3Ec__DisplayClass66_0_U3CRaceU3Eb__3_m5925431199B068785646FFD433C27ACF0DA6C7D2,
	U3CU3Ec__DisplayClass66_1__ctor_m864691E480DAEE7AEEAD46F8FB882828DB249E77,
	U3CU3Ec__DisplayClass66_1_U3CRaceU3Eb__1_mAA09AC32AAC0FB42ED3E102028E9F62480131D48,
	U3CU3Ec__DisplayClass69_0__ctor_m1076FE9DB847E9C246A4B77B4A9AD68DA316DAFC,
	U3CU3Ec__DisplayClass69_0_U3CFinallyU3Eb__0_m05B68401734C79A8332FD12FBF32C5C1DB2FCF61,
	U3CU3Ec__DisplayClass69_0_U3CFinallyU3Eb__1_m805874267E687B76A8017DEFD8A11A96DA9FA8AD,
	U3CU3Ec__DisplayClass70_0__ctor_mD191E17C3CB17FB74E6636B739DFFE3332CF62FD,
	U3CU3Ec__DisplayClass70_0_U3CContinueWithU3Eb__0_m251D75318366BEDFA9CC05A71A3B108CBEBDE4D6,
	U3CU3Ec__DisplayClass70_0_U3CContinueWithU3Eb__1_m8EF480C7A95C97BAE71BB581C32C4B1A14B98178,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Tuple__ctor_mB90BC7455CD38661E7B174497948846C3717B84A,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	PromiseException__ctor_mC1C353F7C09B485B8EFCF3B176412385745A37B2,
	PromiseException__ctor_m25FEF871F4EADD0813603091A36151F62E072F34,
	PromiseException__ctor_m378D7284D56C1662B14AFD057C60ACBBABCF8895,
	PromiseStateException__ctor_m0841D9ED95CB98A821651EB934050C7F99325539,
	PromiseStateException__ctor_mB10D86B4B0D696B724F6A344C9DA8BA582785E8A,
	PromiseStateException__ctor_m09D7E625AC5B60FBAA33F53C52F2C5B9784C3AA9,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	HttpBase_CreateRequestAndRetry_m6DC921EB7E83F0FB058D9C9889B15D489092D059,
	HttpBase_CreateRequest_m395D82BBC11CA3C9D8FA5DCA55BC4E5AFE6AC451,
	HttpBase_CreateException_m4F00611C1B083E175A53008F96FCDAEED39E094E,
	HttpBase_DebugLog_mB403310A43B2C0F73923BE8135DBA43E5FECF8C3,
	HttpBase_DefaultUnityWebRequest_mA2CA028D057F027E7D8283E63643F0AF2974B069,
	NULL,
	NULL,
	U3CCreateRequestAndRetryU3Ed__0__ctor_mA038A5FC9BE11F7D821C2EB89C903BD81DD6673F,
	U3CCreateRequestAndRetryU3Ed__0_System_IDisposable_Dispose_m0F0654EB7C2275B8DDFC1B070E8B145B2C5FBCAC,
	U3CCreateRequestAndRetryU3Ed__0_MoveNext_mC05783571C265F2A6348BF44253BDF379AD516DD,
	U3CCreateRequestAndRetryU3Ed__0_U3CU3Em__Finally1_m65EAC36497FC4D9964288E99CD0E428ABC806C46,
	U3CCreateRequestAndRetryU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m585D7434321501EABFADD21A314DCBB231EC3252,
	U3CCreateRequestAndRetryU3Ed__0_System_Collections_IEnumerator_Reset_m09C357E4FCCD4430591B46B1960B5DC7D4BFE376,
	U3CCreateRequestAndRetryU3Ed__0_System_Collections_IEnumerator_get_Current_m4D5CD62D3F82FDDE9C67A9FD2E2894857D7746A6,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	RequestException_get_IsHttpError_mF1D87818843F7F4BFA1BB1B1F1EAD6925497CF30,
	RequestException_set_IsHttpError_mF311DBD704CCC61C23A270F8BCD1E7E8E5F17469,
	RequestException_get_IsNetworkError_mF6C31F24DCF7B5F649A4CABF455EBB03CB53434C,
	RequestException_set_IsNetworkError_mB4D9148437DD8A59B2969764E067E35E085ABB15,
	RequestException_get_StatusCode_m786C170A63F36EF40CF6F4347F5EACC85925E1F1,
	RequestException_set_StatusCode_mFD7034224DB7D4C27984CE0DA365D02173432309,
	RequestException_get_ServerMessage_m14FEBDED7437643B057376759428572897C03597,
	RequestException_set_ServerMessage_m3DE596FD331A4F694B415FA3C8886FDC5F861DE9,
	RequestException_get_Response_m4FE8480DACD2F2347FDE3BC8F0843885E10912E5,
	RequestException_set_Response_mEF4EB83001EB93F345CD9FB6214237F3888EDA25,
	RequestException__ctor_m4255A573E554424A1B5C9F6E2D58590C50877C85,
	RequestException__ctor_m2A12EC5D24756FA04C4D080FFD4DB9264B01753B,
	RequestException__ctor_m230AC0532C60E770A87DB74A57630F5464269D23,
	RequestException__ctor_mD2293F4AA83F9A3A3EA0E48219894E91DF485FCC,
	RequestHelper_get_Uri_m579870F497E54040498F01E0F03D8E385B75682B,
	RequestHelper_set_Uri_mED832E7E6B02422B23573A3BE154D41312A32BE3,
	RequestHelper_get_Method_m3CAF08A8E99D378C7693940ED9BBC576130C5558,
	RequestHelper_set_Method_m93E8A5F66E37E3459E8F1689E272C9EA2303EEA2,
	RequestHelper_get_Body_mE96291B55EA0C31805FDF261E1A6E0E0A04B9B4D,
	RequestHelper_set_Body_m6C34BB52EE69E8A7D5B2FC14CE8478A4CCCB223A,
	RequestHelper_get_BodyString_m583C6B16055049B23992E90B9BE9240D2494C872,
	RequestHelper_set_BodyString_m264B491596B1E5D59DEBEDFA6D2C07EC37F6AF25,
	RequestHelper_get_BodyRaw_m0567B940B2FD769D548A63A3937EDAB8AF8BEB07,
	RequestHelper_set_BodyRaw_m4BB1B19C3109ED8297827657F5855DCAF7EF315E,
	RequestHelper_get_Timeout_m5EB987EDD3D72C0AFC56600A506DED4393B31D03,
	RequestHelper_set_Timeout_m8769C12B49481D0B8BC11B3227F802FDF049707F,
	RequestHelper_get_ContentType_mF1D15920E2C975B8788B61B57B6A0EC713C5EB97,
	RequestHelper_set_ContentType_m53EBFC2111ABCD757D3B613E943FED0F2A916B1F,
	RequestHelper_get_Retries_mFE6F51F6EE991FEE0982128A8305F013668B7D5B,
	RequestHelper_set_Retries_m8E91AB08F2D651BE88B57973D472A87CFE403AE1,
	RequestHelper_get_RetrySecondsDelay_mF461806AF11B3F89D745861D3EE818A9548705F5,
	RequestHelper_set_RetrySecondsDelay_mF554D9A11BAD76DF31F07A5FA92B67E34FF7D6F8,
	RequestHelper_get_RetryCallback_m8487D96D1889FAC85CC99D59CE9AAD81AF975858,
	RequestHelper_set_RetryCallback_m8B02AFF7AB55609093C48182D79501B34C2F2646,
	RequestHelper_get_EnableDebug_m031DA45E2494D29141E793FBFE2BEF047DCF7472,
	RequestHelper_set_EnableDebug_m7DEAE27887D92A686E341EB14010DE5A4B3E4DF9,
	RequestHelper_get_UseHttpContinue_m64713A0BA2260B82D1587517ADF0671DB60B8D66,
	RequestHelper_set_UseHttpContinue_m454427FB941A608ABD370EC1E64F0C5166183E48,
	RequestHelper_get_RedirectLimit_m7628CFD271A1A06DBF264C289268CE52DA66DB82,
	RequestHelper_set_RedirectLimit_m32E15A1AE611D405855B008D58576C8839929DC3,
	RequestHelper_get_IgnoreHttpException_m0BA6B17AACCF9C74FEFD730806A64B67E4505FEA,
	RequestHelper_set_IgnoreHttpException_m6653B079C6C70E478895D1EF4BFAC9FE5D5D2209,
	RequestHelper_get_FormData_m1CE699C5BC879C7AEFBB1084CF0A1928CD02B931,
	RequestHelper_set_FormData_mD33B2EEF5749F7898DB81BD2ACEB0F7EA9E97EC4,
	RequestHelper_get_SimpleForm_mE158581E8CAC46C434A072324A6290CBE4878C78,
	RequestHelper_set_SimpleForm_m885368EFE7D8FF37C0E9CBC36A8B551562042165,
	RequestHelper_get_FormSections_m4157D4255B827B864F882669136AB30BA93316F0,
	RequestHelper_set_FormSections_m955F901BFE0A3D2FFE42F223BB2005BA2028337E,
	RequestHelper_get_CertificateHandler_mBF95069255782425F21F491C5A51F3BBCEE956B1,
	RequestHelper_set_CertificateHandler_m1B5296D19D58F16EF358706D8AC3817F0ED0B1C5,
	RequestHelper_get_UploadHandler_m5C72A7EF49F678C8CB02FA0620C0B41D1CF8AE78,
	RequestHelper_set_UploadHandler_mDA3C9BCAE240C2A5185B88FE2FE7BF198B6467ED,
	RequestHelper_get_DownloadHandler_m3CC95DFC6C81ABD8508947C1808BC8FB7095BDA3,
	RequestHelper_set_DownloadHandler_mE7FC2C6C38C5418A915D4007DCFF2A9CF5C6DC74,
	RequestHelper_get_Headers_m09842B9228E6B35E7254DFAC41A5D2C98DBF0BE9,
	RequestHelper_set_Headers_mE4B41FA1E24D02AD057068E2D0A697C129921A29,
	RequestHelper_get_Params_mDB495A9CB92B43BB1BD3526C99880EFBDC40E033,
	RequestHelper_set_Params_m887AAEE89701BE57B2875DD482A584C58EF7C400,
	RequestHelper_get_ParseResponseBody_m8352D39E0EBACC6FB532FBE05B5D3EC709EA43BD,
	RequestHelper_set_ParseResponseBody_m1149B5FC0628F6BCDAFA95D21516A9CAE3F4031D,
	RequestHelper_get_Request_mB3DD0BDA5D264948DE92BA5E060545D8D3CCBD63,
	RequestHelper_set_Request_m4C49567052095FC092A24AD821EF7957CCB6F4BF,
	RequestHelper_get_UploadProgress_m4F7D8BCA66AE1D13F1C59D91EE10E656D48DE91B,
	RequestHelper_get_UploadedBytes_m7D3445EE67E794E91D82A28171224EED03460533,
	RequestHelper_get_DownloadProgress_m0ADF466E7C45A9D043506B099C6B3E7A08C5BE33,
	RequestHelper_get_DownloadedBytes_m5201AEFCAF699F4C054CBA3C63004950C1E32ED7,
	RequestHelper_GetHeader_m0EAA6AEED4B885553B4CD97B2C4CBF81A32EF468,
	RequestHelper_get_IsAborted_mED2B0C2E2FA7DA65574ED4141818116D5407825B,
	RequestHelper_set_IsAborted_mF37D6A6B93B70D7B4AC2B579FDA59345D0D51DDF,
	RequestHelper_get_DefaultContentType_mF52E8AD322DC2F3A0A0444FA1A60D2C62B42113F,
	RequestHelper_set_DefaultContentType_m0098EE480F8E776673979AFB751C76CD61AB5365,
	RequestHelper_Abort_m7CE069D799170B7D0336E7DB358C78B1B00DA8F3,
	RequestHelper__ctor_m76D5CE4B07BAD0EFE344E9A8C4D1BFE895922C13,
	ResponseHelper_get_Request_mE2DF5B066E83E4216167EEE1A28A79A09A372843,
	ResponseHelper_set_Request_m9743967B9D789F748E8205E910F339114CB7D522,
	ResponseHelper__ctor_m0F45F428ADF231F62EC7C6E5621058DA806A1850,
	ResponseHelper_get_StatusCode_mAC67167172472DC6804430CA8FDEDDBF1FED2D4F,
	ResponseHelper_get_Data_mDFB63D2691666575C126F59C928580F359646E44,
	ResponseHelper_get_Text_m4C6CE813DC4FA5B0659BF20EC4B5EE2504C618AF,
	ResponseHelper_get_Error_mAF99FD6F00AF225E4D2EF620266C6A428199F6E7,
	ResponseHelper_get_Headers_m2990A8F894F71B2503103388796B0717EEF9D9CB,
	ResponseHelper_GetHeader_m9E39EF8000022EA04DF66B43FCCB31AA01AB0D52,
	ResponseHelper_ToString_m3DD8554605B5BDE7A6412218369E317C804B0572,
	StaticCoroutine_get_Runner_mC13BCB3C4C6F548812E35E5555D3455AEB2700DF,
	StaticCoroutine_StartCoroutine_m46187D9942A6387EAD6EEE4771C5C54027547C58,
	CoroutineHolder__ctor_mC1424305AB5B1F2E631B7121BBB184B1B8BBBCF6,
	RestClient_get_Version_m5701043CF8E7F883ACF65A25CADCE6AB4AFC8EB0,
	RestClient_get_DefaultRequestParams_mE23F55E6B8003CFEE7FF60C7483232FE7E247A3D,
	RestClient_set_DefaultRequestParams_mA826F5ADE53A384196CC73D37D26E72E5B7CAA9B,
	RestClient_ClearDefaultParams_mCBFF0C9C7DA720E8F0F9D54D0BDDE3D1C421CA71,
	RestClient_get_DefaultRequestHeaders_m7CFC695866D2FFFAE6C2C79B38564917EFFE37AA,
	RestClient_set_DefaultRequestHeaders_m8ED47C4D90B6F05DB8ED523A5A49C2B0B1B7C2B5,
	RestClient_ClearDefaultHeaders_mFADDD96FA3D43A3CFC44B9A6A641B666F000C09F,
	RestClient_Request_m6D0EEC74B2073F8006977A737FEB5A26FF8B2BE4,
	NULL,
	RestClient_Get_mD3CD57C7B7809173BB0841F05883D4EAB667C4E4,
	RestClient_Get_m62DDD6AA1F3E2D93DC5AF9378090C97D67EE7D54,
	NULL,
	NULL,
	NULL,
	NULL,
	RestClient_Post_m4B07264A4B85D23B2F3013C227AB599B04F18E7A,
	RestClient_Post_mAD4114F33CCE7E913400D3AA42D2494C1C71D27A,
	RestClient_Post_mB43880A95D1AD7E76C1878CB288DC4E6B3453651,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	RestClient_Put_m955E298F1437E5DDB4391BE3BF2A58B02232AA4E,
	RestClient_Put_m8AF7486C0FD795F8D4CCEBE663A56980E9397A02,
	RestClient_Put_m08D2C2C9C8B6F73A7531BF8BD2AC15C9C5E13680,
	NULL,
	NULL,
	NULL,
	RestClient_Delete_m7701A6040B30FF21656087D1FFBE7BCAFA782E16,
	RestClient_Delete_m1C6AA27C4494A101CED7B1B6BFE9A3B38247FB60,
	RestClient_Head_m01F7D5AEF77C54D5AE2948C1F579C1FD856AB0A8,
	RestClient_Head_mFEFB7B87CFC87642E95B09D1A4AB76F63ABA3E14,
	RestClient_Request_mA130CF629F30CB634AA89655F61C5BD37F729409,
	NULL,
	RestClient_Get_m7E5DEB235C456E5FAFCA828BF688ECFD630E86F7,
	RestClient_Get_mDEA2483857BD9E64941E12CB42063D74EEF50FE6,
	NULL,
	NULL,
	NULL,
	NULL,
	RestClient_Post_m4763A3817990302191F3A5B069A64B02419523BC,
	RestClient_Post_mD59FE057DB71BF372315AB100BD5E4339FFFFE07,
	RestClient_Post_m27A12B1AAD73F990A53E62B578D01BF4EA933509,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	RestClient_Put_mB84F593B9E21B27814D3EBAA2920B67D1699FD26,
	RestClient_Put_m3E06B35E9CFAE4AF81DCF12A8F30A178E1CC5ABA,
	RestClient_Put_m5E7F007136807CD127CA4FD4D6A986B3979391F4,
	NULL,
	NULL,
	NULL,
	RestClient_Delete_m7CACD86724C07540430FDA0E5D759F6E57BBF75C,
	RestClient_Delete_m33B84121232197C62DB7D3BAFB4E5F39CEF4C474,
	RestClient_Head_m6CA11D53CD3CD3724C345EA2DCEBCF665E31D101,
	RestClient_Head_mFE1399A7241D6D868C0AB0B65C774812167D9D43,
	NULL,
	NULL,
	Common_GetFormSectionsContentType_m19E34A4A4613B617EB59187AE573639031CD6B93,
	Common_ConfigureWebRequestWithOptions_mF5668E71E221566BFD09C871A2B0667206901F78,
	Common_SendWebRequestWithOptions_mA6DA8F8469D1F337E3CB043E57F2191D8D95EE1B,
	U3CSendWebRequestWithOptionsU3Ed__4__ctor_m47FB1CC913CF34CE244286B13D6600F97F2C7006,
	U3CSendWebRequestWithOptionsU3Ed__4_System_IDisposable_Dispose_m5F26553216BF51BE2953AFD9118938F28D511A34,
	U3CSendWebRequestWithOptionsU3Ed__4_MoveNext_mB5E4CFE2AAE6A9BCE0D76C72C378282AFCB56C68,
	U3CSendWebRequestWithOptionsU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF49002192678DCE4CE3D8D78E1E922C730FED67C,
	U3CSendWebRequestWithOptionsU3Ed__4_System_Collections_IEnumerator_Reset_m82497AA2724FEA6918E2C0900364DA889677B45E,
	U3CSendWebRequestWithOptionsU3Ed__4_System_Collections_IEnumerator_get_Current_mD15322133CA601A9884E3FCBDE8BD76FFFB1558D,
	Extensions_CreateWebResponse_m85A8BFBB1916DE1247943658A0758B372807A7F1,
	Extensions_IsValidRequest_m95295E8977C94FB722EE97C5047C3975DE5EA65A,
	Extensions_EscapeURL_mAAD51E7CBC83CB8CE79715D8A6086CB153919CF7,
	Extensions_BuildUrl_mE2D5E2B005BCD677EED820AE36D5F28F01E57CA0,
	U3CU3Ec__DisplayClass3_0__ctor_mDEE443CFFB4553720586AE36F8109D07F7E08B87,
	U3CU3Ec__DisplayClass3_0_U3CBuildUrlU3Eb__0_mDFB7EBC5B80A07C2899A8B10EE18130CC2AE4157,
	U3CU3Ec__cctor_m51C966458A7B400E929B365351E9FA572508174F,
	U3CU3Ec__ctor_m3AF7EC6F7B8017905D17E0078A8E8219711B52B7,
	U3CU3Ec_U3CBuildUrlU3Eb__3_1_m3F16B42B55E86F33694E97ABD64E3ADB7B6EDD03,
	Photo_ToString_mC7549402494D8D3A4192988B7A75C3F43BB81C7D,
	Photo__ctor_mFB7B355DA04B8883B308CC03347EF777898F6846,
	Post_ToString_m4AD8D8586CF507C7C435254089148F3AD32A855A,
	Post__ctor_mAD9D421E1716FEB8D8B297565CE8F10FFB42CC79,
	Todo_ToString_mAD1AD0938A238807A2D0EA2888893C11CBE105CF,
	Todo__ctor_m3DBCDF59C636313C811C93C83ED5ABC292A19AA4,
	User_ToString_mD57533E879F6F6B77BD1A38883A0DAED53D2FAF2,
	User__ctor_m179E4458F95BA48DD9D0F7E1E4DF8AC26DB05C0F,
};
extern void PlayerActions__ctor_m930E091485F40060E44C70D57942B72C8846D851_AdjustorThunk (void);
extern void PlayerActions_get_Move_mA26DAF3D380CEAA2C8E380BFF6189FC1B9644889_AdjustorThunk (void);
extern void PlayerActions_get_Attack_mD3B982834EECD17F87FF0B3ABD57015297CAF078_AdjustorThunk (void);
extern void PlayerActions_get_Look_m37DE82164F8D5796D0EC565A956819BE4B93F707_AdjustorThunk (void);
extern void PlayerActions_get_Open_m16D8BFDB470D6DDFC00A68881E15F23B44039D35_AdjustorThunk (void);
extern void PlayerActions_Get_mCFD389B69ACE65D5F30FC407BD927CC40428584A_AdjustorThunk (void);
extern void PlayerActions_Enable_mA1D8C3F578776B79B01FFFC610FBAF4A1354714D_AdjustorThunk (void);
extern void PlayerActions_Disable_m960BE16B364ED50D91824CF19C69C92CB5C71F89_AdjustorThunk (void);
extern void PlayerActions_get_enabled_m08BF48AFE9EC8BC74F261D2FF1610B5D5B13AA27_AdjustorThunk (void);
extern void PlayerActions_SetCallbacks_mF731117D76FCFD8E4B59E2C402524B39759E295B_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[10] = 
{
	{ 0x0600010C, PlayerActions__ctor_m930E091485F40060E44C70D57942B72C8846D851_AdjustorThunk },
	{ 0x0600010D, PlayerActions_get_Move_mA26DAF3D380CEAA2C8E380BFF6189FC1B9644889_AdjustorThunk },
	{ 0x0600010E, PlayerActions_get_Attack_mD3B982834EECD17F87FF0B3ABD57015297CAF078_AdjustorThunk },
	{ 0x0600010F, PlayerActions_get_Look_m37DE82164F8D5796D0EC565A956819BE4B93F707_AdjustorThunk },
	{ 0x06000110, PlayerActions_get_Open_m16D8BFDB470D6DDFC00A68881E15F23B44039D35_AdjustorThunk },
	{ 0x06000111, PlayerActions_Get_mCFD389B69ACE65D5F30FC407BD927CC40428584A_AdjustorThunk },
	{ 0x06000112, PlayerActions_Enable_mA1D8C3F578776B79B01FFFC610FBAF4A1354714D_AdjustorThunk },
	{ 0x06000113, PlayerActions_Disable_m960BE16B364ED50D91824CF19C69C92CB5C71F89_AdjustorThunk },
	{ 0x06000114, PlayerActions_get_enabled_m08BF48AFE9EC8BC74F261D2FF1610B5D5B13AA27_AdjustorThunk },
	{ 0x06000116, PlayerActions_SetCallbacks_mF731117D76FCFD8E4B59E2C402524B39759E295B_AdjustorThunk },
};
static const int32_t s_InvokerIndices[1210] = 
{
	1354,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2290,
	2290,
	782,
	1354,
	2290,
	2290,
	2844,
	1777,
	1777,
	1777,
	2290,
	2290,
	4440,
	2844,
	1348,
	2844,
	2290,
	2290,
	2290,
	2844,
	2844,
	2844,
	2844,
	751,
	2844,
	2844,
	2844,
	2272,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	751,
	2844,
	2844,
	2844,
	2272,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2841,
	2844,
	2844,
	2844,
	2844,
	2272,
	2290,
	2787,
	2844,
	2272,
	2844,
	2814,
	2787,
	2844,
	2787,
	2844,
	779,
	2844,
	1354,
	2844,
	2844,
	2844,
	2787,
	2844,
	2844,
	2272,
	2844,
	2814,
	2787,
	2844,
	2787,
	2768,
	2272,
	2768,
	2272,
	2768,
	2272,
	2768,
	2272,
	2768,
	2272,
	2768,
	2272,
	2768,
	2272,
	2768,
	2272,
	2768,
	2272,
	2768,
	2272,
	2768,
	2272,
	2768,
	2272,
	2768,
	2272,
	2844,
	2844,
	2290,
	2290,
	2290,
	2272,
	2844,
	2844,
	203,
	2844,
	2844,
	2844,
	2272,
	2272,
	2272,
	2844,
	779,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2272,
	2272,
	2272,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	1778,
	2844,
	2272,
	2844,
	2814,
	2787,
	2844,
	2787,
	2290,
	2290,
	2844,
	2290,
	2844,
	2290,
	2844,
	2290,
	2844,
	2290,
	2844,
	2290,
	2844,
	2290,
	2844,
	2290,
	2844,
	2290,
	2844,
	2290,
	2844,
	2290,
	2844,
	2290,
	2844,
	2290,
	2844,
	2290,
	2844,
	2290,
	2844,
	2290,
	2844,
	2290,
	2844,
	2290,
	2844,
	2290,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2290,
	2290,
	2290,
	2290,
	2844,
	2787,
	2844,
	2844,
	2672,
	2191,
	2670,
	2189,
	2681,
	1966,
	2787,
	2787,
	2844,
	2844,
	2884,
	2290,
	2787,
	2787,
	2787,
	2787,
	2787,
	2844,
	2844,
	2814,
	4244,
	2290,
	2367,
	2367,
	2367,
	2367,
	2844,
	2844,
	2844,
	2844,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	2844,
	2844,
	2844,
	2844,
	2844,
	2290,
	2844,
	2844,
	2844,
	2844,
	2787,
	2844,
	2272,
	2844,
	2814,
	2787,
	2844,
	2787,
	537,
	2844,
	537,
	2844,
	2787,
	2290,
	2787,
	2290,
	2787,
	2290,
	2787,
	2290,
	2787,
	2290,
	2844,
	2844,
	2290,
	2290,
	1130,
	1130,
	770,
	770,
	779,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2787,
	2844,
	2272,
	2844,
	2814,
	2787,
	2844,
	2787,
	2787,
	2844,
	2272,
	2844,
	2814,
	2787,
	2844,
	2787,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2787,
	2844,
	2272,
	2844,
	2814,
	2787,
	2844,
	2787,
	2844,
	2844,
	2844,
	2844,
	2844,
	1777,
	2787,
	2844,
	2272,
	2844,
	2814,
	2787,
	2844,
	2787,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2272,
	2844,
	2844,
	2844,
	1130,
	1130,
	770,
	770,
	779,
	2844,
	2844,
	2844,
	2844,
	2290,
	2290,
	2844,
	2844,
	2844,
	2844,
	2290,
	2844,
	2290,
	2290,
	2290,
	2290,
	2272,
	2844,
	2844,
	2844,
	2844,
	2272,
	2844,
	2844,
	2272,
	2844,
	2844,
	2787,
	2844,
	2272,
	2844,
	2814,
	2787,
	2844,
	2787,
	2844,
	2844,
	2844,
	2844,
	2290,
	1777,
	1777,
	2844,
	2272,
	2844,
	2814,
	2787,
	2844,
	2787,
	2272,
	2844,
	2814,
	2787,
	2844,
	2787,
	2844,
	2844,
	2787,
	2787,
	2844,
	4440,
	2272,
	2844,
	2814,
	2787,
	2844,
	2787,
	2272,
	2844,
	2814,
	2787,
	2844,
	2787,
	2844,
	2844,
	2844,
	2844,
	2844,
	2787,
	2844,
	2272,
	2844,
	2814,
	2787,
	2844,
	2787,
	2844,
	2844,
	2844,
	2844,
	2290,
	2787,
	2844,
	2272,
	2844,
	2814,
	2787,
	2844,
	2787,
	2844,
	2844,
	2844,
	2844,
	2290,
	2787,
	2844,
	2272,
	2844,
	2814,
	2787,
	2844,
	2787,
	2844,
	2844,
	2844,
	2844,
	2290,
	2787,
	2844,
	2272,
	2844,
	2814,
	2787,
	2844,
	2787,
	2844,
	2844,
	2844,
	2844,
	2290,
	2787,
	2844,
	2844,
	861,
	2272,
	2844,
	2814,
	2787,
	2844,
	2787,
	2844,
	2844,
	1777,
	2787,
	2844,
	2272,
	2844,
	2814,
	2787,
	2844,
	2787,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2272,
	2272,
	2272,
	2272,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2290,
	2290,
	2290,
	2290,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2844,
	2768,
	2272,
	2816,
	2314,
	2787,
	2290,
	470,
	2844,
	2844,
	2844,
	2844,
	2272,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	2290,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	2844,
	2290,
	2844,
	1780,
	1777,
	1777,
	2314,
	1966,
	1780,
	1777,
	1777,
	1966,
	1777,
	2314,
	1777,
	2844,
	2844,
	2007,
	2844,
	2007,
	2768,
	1777,
	1354,
	2290,
	2844,
	1777,
	-1,
	1777,
	1777,
	-1,
	949,
	949,
	-1,
	599,
	599,
	1777,
	-1,
	1777,
	1777,
	-1,
	1777,
	1777,
	-1,
	1777,
	2768,
	2844,
	2314,
	2768,
	2787,
	2290,
	2787,
	2290,
	4337,
	4337,
	4413,
	2768,
	2787,
	2290,
	2768,
	2272,
	2844,
	2290,
	4406,
	1354,
	1354,
	1354,
	782,
	1354,
	785,
	2844,
	2290,
	2844,
	2314,
	2290,
	2844,
	2314,
	1354,
	2290,
	2844,
	1777,
	1777,
	-1,
	1777,
	1777,
	-1,
	949,
	949,
	-1,
	599,
	599,
	782,
	1354,
	1777,
	-1,
	4235,
	4235,
	1777,
	4235,
	4235,
	1777,
	-1,
	4235,
	4235,
	4413,
	4235,
	1777,
	1777,
	-1,
	1777,
	4023,
	4440,
	2395,
	2290,
	2290,
	2290,
	2844,
	2308,
	2844,
	2300,
	2844,
	2844,
	2290,
	2314,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	2844,
	2844,
	2314,
	2844,
	2290,
	2290,
	2844,
	2844,
	2290,
	2844,
	2787,
	-1,
	-1,
	2844,
	1348,
	2290,
	2844,
	2314,
	2844,
	2844,
	2787,
	2844,
	949,
	2844,
	2844,
	2787,
	2314,
	2844,
	2787,
	-1,
	-1,
	2844,
	1348,
	2290,
	2844,
	2844,
	2314,
	2844,
	2844,
	2290,
	2844,
	2844,
	2290,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	2844,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	2844,
	2290,
	1354,
	2844,
	2290,
	1354,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	3818,
	4235,
	3818,
	3681,
	3818,
	-1,
	-1,
	2272,
	2844,
	2814,
	2844,
	2787,
	2844,
	2787,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	2814,
	2312,
	2814,
	2312,
	2769,
	2273,
	2787,
	2290,
	2787,
	2290,
	2844,
	2290,
	1354,
	212,
	2787,
	2290,
	2787,
	2290,
	2787,
	2290,
	2787,
	2290,
	2787,
	2290,
	2675,
	2192,
	2787,
	2290,
	2768,
	2272,
	2816,
	2314,
	2787,
	2290,
	2814,
	2312,
	2671,
	2190,
	2675,
	2192,
	2814,
	2312,
	2787,
	2290,
	2787,
	2290,
	2787,
	2290,
	2787,
	2290,
	2787,
	2290,
	2787,
	2290,
	2787,
	2290,
	2787,
	2290,
	2814,
	2312,
	2787,
	2290,
	2816,
	2769,
	2816,
	2769,
	1777,
	2814,
	2312,
	2814,
	2312,
	2844,
	2844,
	2787,
	2290,
	2290,
	2769,
	2787,
	2787,
	2787,
	2787,
	1777,
	2787,
	4413,
	4235,
	2844,
	4413,
	4413,
	4337,
	4440,
	4413,
	4337,
	4440,
	4023,
	-1,
	4023,
	4023,
	-1,
	-1,
	-1,
	-1,
	3668,
	3668,
	4023,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	3668,
	3668,
	4023,
	-1,
	-1,
	-1,
	4023,
	4023,
	4023,
	4023,
	4235,
	-1,
	4235,
	4235,
	-1,
	-1,
	-1,
	-1,
	3818,
	3818,
	4235,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	3818,
	3818,
	4235,
	-1,
	-1,
	-1,
	4235,
	4235,
	4235,
	4235,
	-1,
	-1,
	3798,
	3427,
	3818,
	2272,
	2844,
	2814,
	2787,
	2844,
	2787,
	4235,
	3911,
	4235,
	3818,
	2844,
	1849,
	4440,
	2844,
	1761,
	2787,
	2844,
	2787,
	2844,
	2787,
	2844,
	2787,
	2844,
};
static const Il2CppTokenRangePair s_rgctxIndices[91] = 
{
	{ 0x0200003B, { 0, 5 } },
	{ 0x0200003C, { 5, 4 } },
	{ 0x0200007E, { 9, 4 } },
	{ 0x02000099, { 13, 101 } },
	{ 0x0200009A, { 147, 1 } },
	{ 0x0200009B, { 148, 1 } },
	{ 0x0200009D, { 149, 4 } },
	{ 0x0200009E, { 153, 13 } },
	{ 0x0200009F, { 166, 4 } },
	{ 0x020000A0, { 170, 1 } },
	{ 0x020000A1, { 171, 3 } },
	{ 0x020000A2, { 174, 3 } },
	{ 0x020000A3, { 177, 1 } },
	{ 0x020000A4, { 178, 10 } },
	{ 0x020000A5, { 188, 3 } },
	{ 0x020000A6, { 191, 3 } },
	{ 0x020000A7, { 194, 1 } },
	{ 0x020000A8, { 195, 11 } },
	{ 0x020000A9, { 206, 2 } },
	{ 0x020000AA, { 208, 2 } },
	{ 0x020000AE, { 239, 3 } },
	{ 0x020000AF, { 242, 8 } },
	{ 0x020000B0, { 250, 10 } },
	{ 0x020000C3, { 286, 13 } },
	{ 0x020000C7, { 299, 3 } },
	{ 0x020000CE, { 302, 3 } },
	{ 0x020000D5, { 311, 2 } },
	{ 0x020000D6, { 313, 3 } },
	{ 0x020000D7, { 316, 4 } },
	{ 0x020000DB, { 328, 4 } },
	{ 0x020000DE, { 338, 2 } },
	{ 0x020000DF, { 340, 2 } },
	{ 0x060002A4, { 114, 1 } },
	{ 0x060002B2, { 115, 1 } },
	{ 0x060002B5, { 116, 1 } },
	{ 0x060002B8, { 117, 7 } },
	{ 0x060002BB, { 124, 6 } },
	{ 0x060002BE, { 130, 6 } },
	{ 0x060002C2, { 136, 6 } },
	{ 0x060002CA, { 142, 5 } },
	{ 0x0600030C, { 210, 14 } },
	{ 0x0600030D, { 224, 7 } },
	{ 0x0600030E, { 231, 8 } },
	{ 0x0600036B, { 260, 1 } },
	{ 0x0600036E, { 261, 1 } },
	{ 0x06000371, { 262, 7 } },
	{ 0x06000377, { 269, 6 } },
	{ 0x0600037E, { 275, 6 } },
	{ 0x06000385, { 281, 5 } },
	{ 0x060003CB, { 305, 2 } },
	{ 0x060003CC, { 307, 2 } },
	{ 0x060003CD, { 309, 2 } },
	{ 0x060003EA, { 320, 3 } },
	{ 0x060003EB, { 323, 3 } },
	{ 0x060003EC, { 326, 2 } },
	{ 0x060003FA, { 332, 3 } },
	{ 0x060003FB, { 335, 3 } },
	{ 0x06000407, { 342, 1 } },
	{ 0x06000408, { 343, 1 } },
	{ 0x06000409, { 344, 2 } },
	{ 0x0600040A, { 346, 2 } },
	{ 0x0600046A, { 348, 1 } },
	{ 0x0600046D, { 349, 1 } },
	{ 0x0600046E, { 350, 1 } },
	{ 0x0600046F, { 351, 1 } },
	{ 0x06000470, { 352, 1 } },
	{ 0x06000474, { 353, 1 } },
	{ 0x06000475, { 354, 1 } },
	{ 0x06000476, { 355, 1 } },
	{ 0x06000477, { 356, 1 } },
	{ 0x06000478, { 357, 1 } },
	{ 0x06000479, { 358, 1 } },
	{ 0x0600047D, { 359, 1 } },
	{ 0x0600047E, { 360, 1 } },
	{ 0x0600047F, { 361, 1 } },
	{ 0x06000485, { 362, 6 } },
	{ 0x06000488, { 368, 1 } },
	{ 0x06000489, { 369, 6 } },
	{ 0x0600048A, { 375, 1 } },
	{ 0x0600048B, { 376, 6 } },
	{ 0x0600048F, { 382, 1 } },
	{ 0x06000490, { 383, 1 } },
	{ 0x06000491, { 384, 6 } },
	{ 0x06000492, { 390, 1 } },
	{ 0x06000493, { 391, 1 } },
	{ 0x06000494, { 392, 6 } },
	{ 0x06000498, { 398, 1 } },
	{ 0x06000499, { 399, 1 } },
	{ 0x0600049A, { 400, 6 } },
	{ 0x0600049F, { 406, 2 } },
	{ 0x060004A0, { 408, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[409] = 
{
	{ (Il2CppRGCTXDataType)2, 3068 },
	{ (Il2CppRGCTXDataType)2, 565 },
	{ (Il2CppRGCTXDataType)1, 565 },
	{ (Il2CppRGCTXDataType)2, 3830 },
	{ (Il2CppRGCTXDataType)3, 18851 },
	{ (Il2CppRGCTXDataType)2, 3066 },
	{ (Il2CppRGCTXDataType)3, 14644 },
	{ (Il2CppRGCTXDataType)2, 563 },
	{ (Il2CppRGCTXDataType)3, 14645 },
	{ (Il2CppRGCTXDataType)2, 2853 },
	{ (Il2CppRGCTXDataType)2, 537 },
	{ (Il2CppRGCTXDataType)3, 19322 },
	{ (Il2CppRGCTXDataType)3, 18850 },
	{ (Il2CppRGCTXDataType)3, 14205 },
	{ (Il2CppRGCTXDataType)3, 14196 },
	{ (Il2CppRGCTXDataType)2, 908 },
	{ (Il2CppRGCTXDataType)3, 342 },
	{ (Il2CppRGCTXDataType)3, 14195 },
	{ (Il2CppRGCTXDataType)3, 441 },
	{ (Il2CppRGCTXDataType)2, 2548 },
	{ (Il2CppRGCTXDataType)3, 9491 },
	{ (Il2CppRGCTXDataType)3, 9492 },
	{ (Il2CppRGCTXDataType)2, 860 },
	{ (Il2CppRGCTXDataType)3, 39 },
	{ (Il2CppRGCTXDataType)3, 40 },
	{ (Il2CppRGCTXDataType)3, 14189 },
	{ (Il2CppRGCTXDataType)3, 9493 },
	{ (Il2CppRGCTXDataType)3, 9494 },
	{ (Il2CppRGCTXDataType)3, 14171 },
	{ (Il2CppRGCTXDataType)2, 861 },
	{ (Il2CppRGCTXDataType)3, 43 },
	{ (Il2CppRGCTXDataType)3, 44 },
	{ (Il2CppRGCTXDataType)3, 14203 },
	{ (Il2CppRGCTXDataType)3, 14191 },
	{ (Il2CppRGCTXDataType)3, 14192 },
	{ (Il2CppRGCTXDataType)3, 14190 },
	{ (Il2CppRGCTXDataType)3, 14199 },
	{ (Il2CppRGCTXDataType)3, 14180 },
	{ (Il2CppRGCTXDataType)3, 14197 },
	{ (Il2CppRGCTXDataType)3, 14181 },
	{ (Il2CppRGCTXDataType)3, 14182 },
	{ (Il2CppRGCTXDataType)3, 14188 },
	{ (Il2CppRGCTXDataType)3, 14206 },
	{ (Il2CppRGCTXDataType)2, 866 },
	{ (Il2CppRGCTXDataType)3, 59 },
	{ (Il2CppRGCTXDataType)3, 14204 },
	{ (Il2CppRGCTXDataType)3, 60 },
	{ (Il2CppRGCTXDataType)3, 61 },
	{ (Il2CppRGCTXDataType)3, 14183 },
	{ (Il2CppRGCTXDataType)3, 62 },
	{ (Il2CppRGCTXDataType)3, 14193 },
	{ (Il2CppRGCTXDataType)2, 867 },
	{ (Il2CppRGCTXDataType)3, 67 },
	{ (Il2CppRGCTXDataType)2, 2982 },
	{ (Il2CppRGCTXDataType)3, 14179 },
	{ (Il2CppRGCTXDataType)3, 14202 },
	{ (Il2CppRGCTXDataType)3, 68 },
	{ (Il2CppRGCTXDataType)3, 69 },
	{ (Il2CppRGCTXDataType)3, 70 },
	{ (Il2CppRGCTXDataType)3, 14201 },
	{ (Il2CppRGCTXDataType)3, 14200 },
	{ (Il2CppRGCTXDataType)2, 869 },
	{ (Il2CppRGCTXDataType)3, 91 },
	{ (Il2CppRGCTXDataType)3, 92 },
	{ (Il2CppRGCTXDataType)3, 93 },
	{ (Il2CppRGCTXDataType)2, 870 },
	{ (Il2CppRGCTXDataType)3, 103 },
	{ (Il2CppRGCTXDataType)3, 104 },
	{ (Il2CppRGCTXDataType)3, 105 },
	{ (Il2CppRGCTXDataType)3, 14172 },
	{ (Il2CppRGCTXDataType)3, 14186 },
	{ (Il2CppRGCTXDataType)3, 14185 },
	{ (Il2CppRGCTXDataType)3, 14184 },
	{ (Il2CppRGCTXDataType)2, 873 },
	{ (Il2CppRGCTXDataType)3, 117 },
	{ (Il2CppRGCTXDataType)3, 118 },
	{ (Il2CppRGCTXDataType)2, 1519 },
	{ (Il2CppRGCTXDataType)3, 6104 },
	{ (Il2CppRGCTXDataType)3, 14198 },
	{ (Il2CppRGCTXDataType)3, 14187 },
	{ (Il2CppRGCTXDataType)2, 2982 },
	{ (Il2CppRGCTXDataType)2, 874 },
	{ (Il2CppRGCTXDataType)3, 121 },
	{ (Il2CppRGCTXDataType)3, 18775 },
	{ (Il2CppRGCTXDataType)3, 18734 },
	{ (Il2CppRGCTXDataType)3, 14231 },
	{ (Il2CppRGCTXDataType)2, 2995 },
	{ (Il2CppRGCTXDataType)2, 3824 },
	{ (Il2CppRGCTXDataType)2, 2995 },
	{ (Il2CppRGCTXDataType)3, 14230 },
	{ (Il2CppRGCTXDataType)3, 14232 },
	{ (Il2CppRGCTXDataType)3, 122 },
	{ (Il2CppRGCTXDataType)2, 986 },
	{ (Il2CppRGCTXDataType)3, 442 },
	{ (Il2CppRGCTXDataType)3, 18799 },
	{ (Il2CppRGCTXDataType)2, 879 },
	{ (Il2CppRGCTXDataType)3, 153 },
	{ (Il2CppRGCTXDataType)3, 154 },
	{ (Il2CppRGCTXDataType)3, 14194 },
	{ (Il2CppRGCTXDataType)2, 880 },
	{ (Il2CppRGCTXDataType)3, 157 },
	{ (Il2CppRGCTXDataType)3, 158 },
	{ (Il2CppRGCTXDataType)2, 884 },
	{ (Il2CppRGCTXDataType)3, 173 },
	{ (Il2CppRGCTXDataType)3, 174 },
	{ (Il2CppRGCTXDataType)3, 175 },
	{ (Il2CppRGCTXDataType)3, 176 },
	{ (Il2CppRGCTXDataType)2, 1509 },
	{ (Il2CppRGCTXDataType)3, 6100 },
	{ (Il2CppRGCTXDataType)3, 14175 },
	{ (Il2CppRGCTXDataType)2, 885 },
	{ (Il2CppRGCTXDataType)3, 181 },
	{ (Il2CppRGCTXDataType)3, 182 },
	{ (Il2CppRGCTXDataType)3, 183 },
	{ (Il2CppRGCTXDataType)3, 339 },
	{ (Il2CppRGCTXDataType)3, 14177 },
	{ (Il2CppRGCTXDataType)3, 14178 },
	{ (Il2CppRGCTXDataType)2, 868 },
	{ (Il2CppRGCTXDataType)3, 75 },
	{ (Il2CppRGCTXDataType)2, 2975 },
	{ (Il2CppRGCTXDataType)3, 14163 },
	{ (Il2CppRGCTXDataType)3, 14164 },
	{ (Il2CppRGCTXDataType)3, 76 },
	{ (Il2CppRGCTXDataType)3, 77 },
	{ (Il2CppRGCTXDataType)2, 871 },
	{ (Il2CppRGCTXDataType)3, 109 },
	{ (Il2CppRGCTXDataType)3, 110 },
	{ (Il2CppRGCTXDataType)2, 1513 },
	{ (Il2CppRGCTXDataType)3, 6101 },
	{ (Il2CppRGCTXDataType)3, 14173 },
	{ (Il2CppRGCTXDataType)2, 872 },
	{ (Il2CppRGCTXDataType)3, 113 },
	{ (Il2CppRGCTXDataType)3, 114 },
	{ (Il2CppRGCTXDataType)2, 1518 },
	{ (Il2CppRGCTXDataType)3, 6103 },
	{ (Il2CppRGCTXDataType)3, 14176 },
	{ (Il2CppRGCTXDataType)2, 878 },
	{ (Il2CppRGCTXDataType)3, 149 },
	{ (Il2CppRGCTXDataType)3, 150 },
	{ (Il2CppRGCTXDataType)2, 1517 },
	{ (Il2CppRGCTXDataType)3, 6102 },
	{ (Il2CppRGCTXDataType)3, 14174 },
	{ (Il2CppRGCTXDataType)2, 887 },
	{ (Il2CppRGCTXDataType)3, 191 },
	{ (Il2CppRGCTXDataType)3, 192 },
	{ (Il2CppRGCTXDataType)3, 193 },
	{ (Il2CppRGCTXDataType)3, 19426 },
	{ (Il2CppRGCTXDataType)3, 14212 },
	{ (Il2CppRGCTXDataType)3, 14213 },
	{ (Il2CppRGCTXDataType)3, 14216 },
	{ (Il2CppRGCTXDataType)3, 6163 },
	{ (Il2CppRGCTXDataType)3, 14214 },
	{ (Il2CppRGCTXDataType)3, 14215 },
	{ (Il2CppRGCTXDataType)3, 6116 },
	{ (Il2CppRGCTXDataType)3, 78 },
	{ (Il2CppRGCTXDataType)2, 2237 },
	{ (Il2CppRGCTXDataType)3, 79 },
	{ (Il2CppRGCTXDataType)2, 916 },
	{ (Il2CppRGCTXDataType)3, 348 },
	{ (Il2CppRGCTXDataType)3, 80 },
	{ (Il2CppRGCTXDataType)3, 14225 },
	{ (Il2CppRGCTXDataType)3, 14226 },
	{ (Il2CppRGCTXDataType)3, 14224 },
	{ (Il2CppRGCTXDataType)3, 6165 },
	{ (Il2CppRGCTXDataType)3, 81 },
	{ (Il2CppRGCTXDataType)3, 82 },
	{ (Il2CppRGCTXDataType)3, 6117 },
	{ (Il2CppRGCTXDataType)3, 94 },
	{ (Il2CppRGCTXDataType)3, 95 },
	{ (Il2CppRGCTXDataType)3, 96 },
	{ (Il2CppRGCTXDataType)3, 344 },
	{ (Il2CppRGCTXDataType)3, 6118 },
	{ (Il2CppRGCTXDataType)3, 14227 },
	{ (Il2CppRGCTXDataType)2, 2992 },
	{ (Il2CppRGCTXDataType)3, 6119 },
	{ (Il2CppRGCTXDataType)3, 14228 },
	{ (Il2CppRGCTXDataType)2, 2993 },
	{ (Il2CppRGCTXDataType)3, 6120 },
	{ (Il2CppRGCTXDataType)2, 877 },
	{ (Il2CppRGCTXDataType)3, 143 },
	{ (Il2CppRGCTXDataType)3, 144 },
	{ (Il2CppRGCTXDataType)2, 2232 },
	{ (Il2CppRGCTXDataType)3, 145 },
	{ (Il2CppRGCTXDataType)2, 912 },
	{ (Il2CppRGCTXDataType)3, 345 },
	{ (Il2CppRGCTXDataType)3, 123 },
	{ (Il2CppRGCTXDataType)3, 14234 },
	{ (Il2CppRGCTXDataType)3, 14233 },
	{ (Il2CppRGCTXDataType)3, 14237 },
	{ (Il2CppRGCTXDataType)3, 14235 },
	{ (Il2CppRGCTXDataType)3, 14236 },
	{ (Il2CppRGCTXDataType)3, 6121 },
	{ (Il2CppRGCTXDataType)3, 14229 },
	{ (Il2CppRGCTXDataType)2, 2994 },
	{ (Il2CppRGCTXDataType)3, 6122 },
	{ (Il2CppRGCTXDataType)2, 882 },
	{ (Il2CppRGCTXDataType)3, 165 },
	{ (Il2CppRGCTXDataType)3, 166 },
	{ (Il2CppRGCTXDataType)2, 2233 },
	{ (Il2CppRGCTXDataType)3, 159 },
	{ (Il2CppRGCTXDataType)2, 913 },
	{ (Il2CppRGCTXDataType)3, 346 },
	{ (Il2CppRGCTXDataType)3, 160 },
	{ (Il2CppRGCTXDataType)3, 14219 },
	{ (Il2CppRGCTXDataType)3, 14218 },
	{ (Il2CppRGCTXDataType)3, 14217 },
	{ (Il2CppRGCTXDataType)3, 14221 },
	{ (Il2CppRGCTXDataType)3, 14220 },
	{ (Il2CppRGCTXDataType)3, 14223 },
	{ (Il2CppRGCTXDataType)3, 14222 },
	{ (Il2CppRGCTXDataType)2, 859 },
	{ (Il2CppRGCTXDataType)3, 29 },
	{ (Il2CppRGCTXDataType)2, 2997 },
	{ (Il2CppRGCTXDataType)3, 14238 },
	{ (Il2CppRGCTXDataType)3, 30 },
	{ (Il2CppRGCTXDataType)2, 905 },
	{ (Il2CppRGCTXDataType)3, 340 },
	{ (Il2CppRGCTXDataType)2, 2224 },
	{ (Il2CppRGCTXDataType)3, 31 },
	{ (Il2CppRGCTXDataType)3, 32 },
	{ (Il2CppRGCTXDataType)2, 915 },
	{ (Il2CppRGCTXDataType)3, 347 },
	{ (Il2CppRGCTXDataType)2, 2234 },
	{ (Il2CppRGCTXDataType)3, 33 },
	{ (Il2CppRGCTXDataType)3, 19435 },
	{ (Il2CppRGCTXDataType)3, 19438 },
	{ (Il2CppRGCTXDataType)2, 853 },
	{ (Il2CppRGCTXDataType)3, 19 },
	{ (Il2CppRGCTXDataType)2, 1567 },
	{ (Il2CppRGCTXDataType)3, 6152 },
	{ (Il2CppRGCTXDataType)3, 6765 },
	{ (Il2CppRGCTXDataType)3, 19436 },
	{ (Il2CppRGCTXDataType)3, 19437 },
	{ (Il2CppRGCTXDataType)3, 19439 },
	{ (Il2CppRGCTXDataType)2, 856 },
	{ (Il2CppRGCTXDataType)3, 24 },
	{ (Il2CppRGCTXDataType)2, 1568 },
	{ (Il2CppRGCTXDataType)3, 6153 },
	{ (Il2CppRGCTXDataType)3, 6766 },
	{ (Il2CppRGCTXDataType)3, 19694 },
	{ (Il2CppRGCTXDataType)3, 14240 },
	{ (Il2CppRGCTXDataType)3, 14239 },
	{ (Il2CppRGCTXDataType)2, 854 },
	{ (Il2CppRGCTXDataType)3, 20 },
	{ (Il2CppRGCTXDataType)2, 854 },
	{ (Il2CppRGCTXDataType)3, 15010 },
	{ (Il2CppRGCTXDataType)3, 15004 },
	{ (Il2CppRGCTXDataType)3, 15005 },
	{ (Il2CppRGCTXDataType)3, 15011 },
	{ (Il2CppRGCTXDataType)3, 19696 },
	{ (Il2CppRGCTXDataType)2, 857 },
	{ (Il2CppRGCTXDataType)3, 25 },
	{ (Il2CppRGCTXDataType)2, 857 },
	{ (Il2CppRGCTXDataType)3, 15012 },
	{ (Il2CppRGCTXDataType)3, 15006 },
	{ (Il2CppRGCTXDataType)3, 15007 },
	{ (Il2CppRGCTXDataType)3, 15013 },
	{ (Il2CppRGCTXDataType)3, 15008 },
	{ (Il2CppRGCTXDataType)3, 15009 },
	{ (Il2CppRGCTXDataType)3, 19698 },
	{ (Il2CppRGCTXDataType)3, 19430 },
	{ (Il2CppRGCTXDataType)3, 19431 },
	{ (Il2CppRGCTXDataType)2, 876 },
	{ (Il2CppRGCTXDataType)3, 127 },
	{ (Il2CppRGCTXDataType)2, 2974 },
	{ (Il2CppRGCTXDataType)3, 14161 },
	{ (Il2CppRGCTXDataType)3, 14162 },
	{ (Il2CppRGCTXDataType)3, 128 },
	{ (Il2CppRGCTXDataType)3, 129 },
	{ (Il2CppRGCTXDataType)2, 883 },
	{ (Il2CppRGCTXDataType)3, 169 },
	{ (Il2CppRGCTXDataType)3, 170 },
	{ (Il2CppRGCTXDataType)2, 1466 },
	{ (Il2CppRGCTXDataType)3, 6064 },
	{ (Il2CppRGCTXDataType)3, 19427 },
	{ (Il2CppRGCTXDataType)2, 888 },
	{ (Il2CppRGCTXDataType)3, 197 },
	{ (Il2CppRGCTXDataType)3, 198 },
	{ (Il2CppRGCTXDataType)2, 1463 },
	{ (Il2CppRGCTXDataType)3, 6062 },
	{ (Il2CppRGCTXDataType)3, 19425 },
	{ (Il2CppRGCTXDataType)2, 891 },
	{ (Il2CppRGCTXDataType)3, 215 },
	{ (Il2CppRGCTXDataType)3, 216 },
	{ (Il2CppRGCTXDataType)3, 217 },
	{ (Il2CppRGCTXDataType)3, 19424 },
	{ (Il2CppRGCTXDataType)3, 6063 },
	{ (Il2CppRGCTXDataType)3, 130 },
	{ (Il2CppRGCTXDataType)2, 2229 },
	{ (Il2CppRGCTXDataType)3, 131 },
	{ (Il2CppRGCTXDataType)2, 909 },
	{ (Il2CppRGCTXDataType)3, 343 },
	{ (Il2CppRGCTXDataType)3, 132 },
	{ (Il2CppRGCTXDataType)3, 14208 },
	{ (Il2CppRGCTXDataType)3, 14209 },
	{ (Il2CppRGCTXDataType)3, 14207 },
	{ (Il2CppRGCTXDataType)3, 6164 },
	{ (Il2CppRGCTXDataType)3, 133 },
	{ (Il2CppRGCTXDataType)3, 134 },
	{ (Il2CppRGCTXDataType)3, 6059 },
	{ (Il2CppRGCTXDataType)3, 14210 },
	{ (Il2CppRGCTXDataType)2, 2984 },
	{ (Il2CppRGCTXDataType)3, 6060 },
	{ (Il2CppRGCTXDataType)3, 14211 },
	{ (Il2CppRGCTXDataType)2, 2985 },
	{ (Il2CppRGCTXDataType)2, 3157 },
	{ (Il2CppRGCTXDataType)3, 15001 },
	{ (Il2CppRGCTXDataType)2, 3175 },
	{ (Il2CppRGCTXDataType)3, 15048 },
	{ (Il2CppRGCTXDataType)2, 3180 },
	{ (Il2CppRGCTXDataType)3, 15075 },
	{ (Il2CppRGCTXDataType)3, 15002 },
	{ (Il2CppRGCTXDataType)3, 15003 },
	{ (Il2CppRGCTXDataType)3, 15049 },
	{ (Il2CppRGCTXDataType)3, 15050 },
	{ (Il2CppRGCTXDataType)3, 15051 },
	{ (Il2CppRGCTXDataType)3, 15076 },
	{ (Il2CppRGCTXDataType)3, 15077 },
	{ (Il2CppRGCTXDataType)3, 15078 },
	{ (Il2CppRGCTXDataType)3, 15079 },
	{ (Il2CppRGCTXDataType)2, 1804 },
	{ (Il2CppRGCTXDataType)2, 1935 },
	{ (Il2CppRGCTXDataType)3, 338 },
	{ (Il2CppRGCTXDataType)2, 1805 },
	{ (Il2CppRGCTXDataType)2, 1936 },
	{ (Il2CppRGCTXDataType)3, 438 },
	{ (Il2CppRGCTXDataType)2, 895 },
	{ (Il2CppRGCTXDataType)3, 291 },
	{ (Il2CppRGCTXDataType)2, 631 },
	{ (Il2CppRGCTXDataType)2, 896 },
	{ (Il2CppRGCTXDataType)3, 292 },
	{ (Il2CppRGCTXDataType)3, 293 },
	{ (Il2CppRGCTXDataType)2, 886 },
	{ (Il2CppRGCTXDataType)3, 187 },
	{ (Il2CppRGCTXDataType)3, 188 },
	{ (Il2CppRGCTXDataType)2, 890 },
	{ (Il2CppRGCTXDataType)3, 203 },
	{ (Il2CppRGCTXDataType)3, 204 },
	{ (Il2CppRGCTXDataType)3, 19222 },
	{ (Il2CppRGCTXDataType)3, 525 },
	{ (Il2CppRGCTXDataType)3, 19213 },
	{ (Il2CppRGCTXDataType)3, 528 },
	{ (Il2CppRGCTXDataType)3, 19223 },
	{ (Il2CppRGCTXDataType)3, 19224 },
	{ (Il2CppRGCTXDataType)2, 3270 },
	{ (Il2CppRGCTXDataType)3, 15852 },
	{ (Il2CppRGCTXDataType)2, 3271 },
	{ (Il2CppRGCTXDataType)3, 15853 },
	{ (Il2CppRGCTXDataType)3, 18969 },
	{ (Il2CppRGCTXDataType)3, 19467 },
	{ (Il2CppRGCTXDataType)3, 19528 },
	{ (Il2CppRGCTXDataType)3, 19478 },
	{ (Il2CppRGCTXDataType)3, 18967 },
	{ (Il2CppRGCTXDataType)3, 19486 },
	{ (Il2CppRGCTXDataType)3, 19487 },
	{ (Il2CppRGCTXDataType)3, 19529 },
	{ (Il2CppRGCTXDataType)3, 19497 },
	{ (Il2CppRGCTXDataType)3, 19498 },
	{ (Il2CppRGCTXDataType)3, 18968 },
	{ (Il2CppRGCTXDataType)3, 19518 },
	{ (Il2CppRGCTXDataType)3, 19519 },
	{ (Il2CppRGCTXDataType)3, 19530 },
	{ (Il2CppRGCTXDataType)2, 2979 },
	{ (Il2CppRGCTXDataType)3, 14168 },
	{ (Il2CppRGCTXDataType)3, 19510 },
	{ (Il2CppRGCTXDataType)2, 1025 },
	{ (Il2CppRGCTXDataType)3, 524 },
	{ (Il2CppRGCTXDataType)3, 19527 },
	{ (Il2CppRGCTXDataType)3, 19463 },
	{ (Il2CppRGCTXDataType)2, 2976 },
	{ (Il2CppRGCTXDataType)3, 14165 },
	{ (Il2CppRGCTXDataType)3, 19507 },
	{ (Il2CppRGCTXDataType)2, 1022 },
	{ (Il2CppRGCTXDataType)3, 521 },
	{ (Il2CppRGCTXDataType)3, 19466 },
	{ (Il2CppRGCTXDataType)3, 19470 },
	{ (Il2CppRGCTXDataType)2, 2999 },
	{ (Il2CppRGCTXDataType)3, 14241 },
	{ (Il2CppRGCTXDataType)3, 19511 },
	{ (Il2CppRGCTXDataType)2, 1037 },
	{ (Il2CppRGCTXDataType)3, 526 },
	{ (Il2CppRGCTXDataType)3, 19477 },
	{ (Il2CppRGCTXDataType)3, 19481 },
	{ (Il2CppRGCTXDataType)3, 19482 },
	{ (Il2CppRGCTXDataType)2, 2977 },
	{ (Il2CppRGCTXDataType)3, 14166 },
	{ (Il2CppRGCTXDataType)3, 19508 },
	{ (Il2CppRGCTXDataType)2, 1023 },
	{ (Il2CppRGCTXDataType)3, 522 },
	{ (Il2CppRGCTXDataType)3, 19485 },
	{ (Il2CppRGCTXDataType)3, 19493 },
	{ (Il2CppRGCTXDataType)3, 19494 },
	{ (Il2CppRGCTXDataType)2, 3000 },
	{ (Il2CppRGCTXDataType)3, 14242 },
	{ (Il2CppRGCTXDataType)3, 19512 },
	{ (Il2CppRGCTXDataType)2, 1038 },
	{ (Il2CppRGCTXDataType)3, 527 },
	{ (Il2CppRGCTXDataType)3, 19496 },
	{ (Il2CppRGCTXDataType)3, 19514 },
	{ (Il2CppRGCTXDataType)3, 19515 },
	{ (Il2CppRGCTXDataType)2, 2978 },
	{ (Il2CppRGCTXDataType)3, 14167 },
	{ (Il2CppRGCTXDataType)3, 19509 },
	{ (Il2CppRGCTXDataType)2, 1024 },
	{ (Il2CppRGCTXDataType)3, 523 },
	{ (Il2CppRGCTXDataType)3, 19517 },
	{ (Il2CppRGCTXDataType)3, 14169 },
	{ (Il2CppRGCTXDataType)3, 14170 },
	{ (Il2CppRGCTXDataType)3, 19504 },
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	1210,
	s_methodPointers,
	10,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	91,
	s_rgctxIndices,
	409,
	s_rgctxValues,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
